# Add more folders to ship with the application, here
folder_01.source = qml/android-desktop
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += src/main.cpp \
    src/core/teamseasondata.cpp \
    src/core/teamdata.cpp \
    src/core/seasondata.cpp \
    src/core/lapdata.cpp \
    src/core/jsondataparser.cpp \
    src/core/eventdata.cpp \
    src/core/driverseasondata.cpp \
    src/core/driverdata.cpp \
    src/core/datamanager.cpp \
    src/core/connectionapimanager.cpp \
    src/core/circuitmanager.cpp \
    src/core/circuitdata.cpp \
    src/items/nextraceitem.cpp \
    src/models/raceschedulemodel.cpp \
    src/models/raceresultsmodel.cpp \
    src/models/qualiresultsmodel.cpp \
    src/models/laptimesmodel.cpp \
    src/models/driverstandingsmodel.cpp \
    src/models/driverresultsmodel.cpp \
    src/models/constructorstandingsmodel.cpp \
    src/models/constructorresultsmodel.cpp \
    src/items/eventinfoitem.cpp \
    src/items/circuitinfoitem.cpp \
    src/items/driverraceinfoitem.cpp \
    src/items/driverseasoninfoitem.cpp \
    src/items/constructorseasoninfoitem.cpp \
    src/models/constructorraceresultsmodel.cpp \
    src/items/seasoninfoitem.cpp \
    src/core/weatherforecastdata.cpp \
    src/models/weatherforecastmodel.cpp \
    src/items/settingsitem.cpp
	
HEADERS += \
    src/core/teamseasondata.h \
    src/core/teamdata.h \
    src/core/seasondata.h \
    src/core/lapdata.h \
    src/core/jsondataparser.h \
    src/core/eventdata.h \
    src/core/driverseasondata.h \
    src/core/driverdata.h \
    src/core/datamanager.h \
    src/core/connectionapimanager.h \
    src/core/circuitmanager.h \
    src/core/circuitdata.h \
    src/items/nextraceitem.h \
    src/models/raceschedulemodel.h \
    src/models/raceresultsmodel.h \
    src/models/qualiresultsmodel.h \
    src/models/laptimesmodel.h \
    src/models/driverstandingsmodel.h \
    src/models/driverresultsmodel.h \
    src/models/constructorstandingsmodel.h \
    src/models/constructorresultsmodel.h \
    src/items/eventinfoitem.h \
    src/items/circuitinfoitem.h \
    src/items/driverraceinfoitem.h \
    src/items/driverseasoninfoitem.h \
    src/items/constructorseasoninfoitem.h \
    src/models/constructorraceresultsmodel.h \
    src/items/seasoninfoitem.h \
    src/core/weatherforecastdata.h \
    src/models/weatherforecastmodel.h \
    src/items/settingsitem.h


OBJECTS_DIR = obj2/
MOC_DIR = moc2/

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2controlsapplicationviewer/qtquick2controlsapplicationviewer.pri)
qtcAddDeployment()

RESOURCES += \
    icons/icons.qrc \
    qmlpages.qrc \
    icons/weather_color_icons.qrc

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += \
    android/AndroidManifest.xml
