/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../items"

ListItem {

    signal roundSelected;

    Row {
        id: row
        width: parent.width-40
        height: parent.height
        spacing: 10

        Label {
            text: round
            width: row.width * 0.1
            height: parent.height
            font.pixelSize: appSettings.fontSizeExtraLarge
            color: appSettings.fontColorPrimary
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }

        Column {
            id: col
            width: row.width * 0.6
            anchors.verticalCenter: parent.verticalCenter

            Label {
                text: name
                width: parent.width
                verticalAlignment: Text.AlignBottom
                color: appSettings.fontColorPrimary
                font.pixelSize: appSettings.fontSizeMedium
            }
            Label {
                text: team
                width: parent.width
                font.pixelSize: appSettings.fontSizeSmall
                color: appSettings.fontColorSecondary
                verticalAlignment: Text.AlignTop
            }
        }

        Column {
            width: row.width * 0.2
            anchors.verticalCenter: parent.verticalCenter

            Label {
                text: position + " (" + grid + ")"
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom
                font.pixelSize: appSettings.fontSizeMedium
                color: appSettings.fontColorPrimary
            }

            Label {
                text: status
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: appSettings.fontSizeSmall
                color: appSettings.fontColorSecondary
                verticalAlignment: Text.AlignTop
            }
        }
        Label {
            text: points + "P"
            width: row.width * 0.1
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: appSettings.fontSizeMedium
            color: appSettings.fontColorPrimary
        }
    }

    onClicked: {
        roundSelected(round)
    }
}
