/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../items"

ListItem {

    signal qualiResultClicked

    Row {
        id: row
        width: parent.width-60
        height: parent.height
        spacing: 10

        Label {
            text: position
            width: row.width * 0.1
            font.pixelSize: appSettings.fontSizeExtraLarge
            color: appSettings.fontColorPrimary
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }

        Column {
            anchors.verticalCenter: parent.verticalCenter
            width: row.width * 0.36

            Label {
                text: name
                color: appSettings.fontColorPrimary
                verticalAlignment: Text.AlignBottom
                font.pixelSize: appSettings.fontSizeMedium
            }
            Label {
                text: team_name
                font.pixelSize: appSettings.fontSizeSmall
                color: appSettings.fontColorSecondary
                verticalAlignment: Text.AlignTop
            }
        }
        Label {
            text: q1
            width: (season > 2005) ? row.width * 0.18 : row.width * 0.54
            font.pixelSize: appSettings.fontSizeSmall
            color: appSettings.fontColorPrimary
            height: parent.height
            horizontalAlignment: (season > 2005) ? Text.AlignHCenter : Text.AlignRight
            verticalAlignment: (season > 2005) ? Text.AlignBottom : Text.AlignVCenter
        }
        Label {
            text: q2
            visible: (season > 2005)
            width: row.width * 0.18
            font.pixelSize: appSettings.fontSizeSmall
            color: appSettings.fontColorPrimary
            height: parent.height
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }
        Label {
            text: q3
            visible: (season > 2005)
            width: row.width * 0.18
            font.pixelSize: appSettings.fontSizeSmall
            color: appSettings.fontColorPrimary
            height: parent.height
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
        }
    }

    onClicked: {
        qualiResultClicked(driver_id, round)
    }
}
