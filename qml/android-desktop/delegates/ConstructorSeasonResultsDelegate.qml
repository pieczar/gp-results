/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import GPComponents 1.0
import "../items"

ListItem {

    id: item
    signal roundSelected;

    property int baseHeight

    Row {
        id: row
        width: parent.width-20
        height: parent.height
        spacing: 10

        Label {
            text: round
            width: row.width * 0.1
            height: parent.height
            font.pixelSize: appSettings.fontSizeExtraLarge
            color: appSettings.fontColorPrimary
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter

            onTextChanged: {
                crModel.update(constructorSeasonResultsModel.getSeasonData(), constructor_id, round)
            }
        }

        Column {
            width: parent.width
            anchors.verticalCenter: parent.verticalCenter

            Label {
                text: name
                width: row.width * 0.9
                color: appSettings.fontColorPrimary
                font.pixelSize: appSettings.fontSizeMedium
            }

            ListView {
                clip: true
                interactive: false
                id: lv

                width: row.width * 0.9
                height: item.baseHeight

                delegate:

                    Row {
                    width: row.width * 0.9
                    height: item.baseHeight

                    Label {
                        text: driver_name
                        font.pixelSize: appSettings.fontSizeSmall
                        width: parent.width * 0.5
                        color: appSettings.fontColorSecondary
                        verticalAlignment: Text.AlignVCenter

                        onTextChanged: {
                            item.updateHeights(crModel.rowCount())
                        }
                    }

                    Label {
                        text: driver_position + " (" + driver_grid + ")"
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorSecondary
                        width: parent.width * 0.15
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                    Label {
                        text: driver_status
                        font.pixelSize: appSettings.fontSizeSmall
                        width: parent.width * 0.25
                        color: appSettings.fontColorSecondary
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }

                    Label {
                        text: driver_points + "P"
                        width: parent.width * 0.1
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorPrimary
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                    }
                }

                model: ConstructorRaceResultsModel {
                    id: crModel
                }
            }
        }
    }
    onClicked: {
        roundSelected(round)
    }

    function updateHeights(rows)
    {
        lv.height = (item.baseHeight) * rows;
        item.height = (item.baseHeight) * (rows + 1)
        row.height = (item.baseHeight) * (rows + 1);
    }
}
