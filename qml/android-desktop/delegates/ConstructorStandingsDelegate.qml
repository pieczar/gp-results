/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../items"

ListItem {

    id: item
    signal constructorClicked

    Row {
        id: row
        width: parent.width-40
        height: parent.height
        spacing: 10

        Label {
            text: position
            width: row.width * 0.1
            height: parent.height
            horizontalAlignment: Text.AlignRight
            color: appSettings.fontColorPrimary
            font.pixelSize: appSettings.fontSizeMedium
        }
        Label {
            text: name
            width: row.width * 0.5
            height: parent.height
            color: appSettings.fontColorPrimary
            font.pixelSize: appSettings.fontSizeMedium
        }
        Label {
            text: points
            width: row.width * 0.3
            height: parent.height
            horizontalAlignment: Text.AlignRight
            color: appSettings.fontColorPrimary
            font.pixelSize: appSettings.fontSizeMedium
        }
        Label {
            text: wins
            width: row.width * 0.1
            height: parent.height
            horizontalAlignment: Text.AlignRight
            color: appSettings.fontColorPrimary
            font.pixelSize: appSettings.fontSizeMedium
        }
    }

    onClicked: {
        item.constructorClicked(constructor_id)
    }
}
