/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property string constructorId;
    property int season;

    Component {
        id: constructorSeasonResultsDelegate
        ConstructorSeasonResultsDelegate {
            width: page.width
            baseHeight: appSettings.fontSizeMedium * 1.5
            height: appSettings.fontSizeMedium * 1.5

            onRoundSelected: {
                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/RaceResultsPage.qml"), { season: page.season, round: round, pageStack: pageStack });
            }
        }
    }

    TitleToolBar {
        id: toolbar
        title: page.season + " season results"
    }

    Column {
        width: page.width
        id: headerContainer

        ConstructorSeasonInfoItem {
            id: constructorSeasonInfo;
            width: page.width
            height: column.height

            Column {

                id: column
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 5

                ClickableLabel {
                    text: constructorSeasonInfo.position + ". "+ constructorSeasonInfo.name
                    font.underline: true
                    font.pixelSize: appSettings.fontSizeLarge

                    onClicked: {
                        if (appSettings.isAndroid)
                            Qt.openUrlExternally(constructorSeasonInfo.url);
                        else
                            pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: constructorSeasonInfo.url, pageStack: pageStack} )

                    }

                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Row {
                    width: parent.width

                    Label {
                        text: "Points: " + constructorSeasonInfo.points
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }

                    Label {
                        text: "Wins: " + constructorSeasonInfo.wins
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }
                }

                Row {
                    width: parent.width

                    Label {
                        text: "Retirements: " + constructorSeasonInfo.retirements
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }

                    Label {
                        text: "Podiums: " + constructorSeasonInfo.podiums
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }
                }
                Separator {}
            }
        }
    }

    Item {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        anchors.topMargin: appSettings.fontSizeMedium

        ScrollView {
            anchors.fill: parent
            id: listView
            opacity: 0
            ListView {
                clip: true

                header: Item {
                    id: header
                    width: headerContainer.width
                    height: headerContainer.height
                    Component.onCompleted: headerContainer.parent = header
                }

                anchors.fill: parent

                delegate: constructorSeasonResultsDelegate
                model: constructorSeasonResultsModel

            }
        }
        ViewPlaceholder {
            anchors.fill: parent
            opacity: 1 - listView.opacity
        }
    }

    Connections {
        target: dataManager
        onConstructorResultsObtained: {
            constructorSeasonResultsModel.update(sd, constructorId)
            constructorSeasonInfo.updateData(sd, constructorId)
            listView.opacity  = 1
        }
        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    Component.onCompleted: {
        dataManager.getConstructorResults(page.constructorId, page.season)
    }

    NotificationArea {
        id: notification
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            pageStack.pop()
            event.accepted = true
        }
    }
}
