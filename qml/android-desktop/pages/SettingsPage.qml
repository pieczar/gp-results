/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../delegates"
import "../items"

Page {
    id: page

    TitleToolBar {
        id: toolbar
        title: "Settings & About"
    }

    Flickable {
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        width: page.width
        clip: true
        contentHeight: col.height


    Column {                
        id: col
        anchors.margins: 10
        spacing: 30

        Column {
            width: page.width
            spacing: 5
            Label {
                text: "Settings"
                font.italic: true
                font.pixelSize: appSettings.fontSizeSmall
                color: appSettings.fontColorSecondary
                anchors.left: parent.left
                anchors.leftMargin: 10
            }
            Label {
                text: "Font size:"
                font.pixelSize: appSettings.fontSizeMedium
                color: appSettings.fontColorPrimary
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Slider {
                maximumValue: 30
                minimumValue: 12
                width: page.width / 3
                anchors.horizontalCenter: parent.horizontalCenter

                onValueChanged: {
                    if (pressed)
                        appSettings.fontSizeMedium = value
                }

                Component.onCompleted: {
                    value = appSettings.fontSizeMedium
                }
            }
        }
        Column {
            width: parent.width
            spacing: 20

            Column {
                width: parent.width
                spacing: 5
                Separator {

                }
                Label {
                    text: "About"
                    font.italic: true
                    font.pixelSize: appSettings.fontSizeSmall
                    color: appSettings.fontColorSecondary
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                }
            }

            Image {
                source: "qrc:/icons/icon2.png"
                fillMode: Image.PreserveAspectFit
                width: parent.width / 3

                anchors.horizontalCenter: parent.horizontalCenter

                horizontalAlignment: Image.AlignHCenter
            }
            Rectangle {
                width: parent.width
                height: appSettings.fontSizeMedium
                color: "#00000000"
            }

            Label {
                width: parent.width
                text: "GP Results " + dataManager.appVersion();
                font.bold: true
                color: appSettings.fontColorPrimary
                font.pixelSize: appSettings.fontSizeExtraLarge
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                width: parent.width
                text: "by Mariusz Pilarek"
                color: appSettings.fontColorPrimary
                font.pixelSize: appSettings.fontSizeLarge
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                width: parent.width
                font.italic: true
                text: "pieczaro@gmail.com"
                color: appSettings.fontColorPrimary
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: appSettings.fontSizeMedium

                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally("mailto:pieczaro@gmail.com")
                }
            }

            Label {
                width: parent.width
                text: "Data source: http://ergast.com/mrd/"
                wrapMode: "WordWrap"
                horizontalAlignment: Text.AlignHCenter
                color: appSettings.fontColorSecondary
                font.pixelSize: appSettings.fontSizeMedium
                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally("http://ergast.com/mrd/")
                }
            }

            Label {
                width: parent.width
                text: "Weather data: http://openweathermap.org"
                wrapMode: "WordWrap"
                horizontalAlignment: Text.AlignHCenter
                color: appSettings.fontColorSecondary
                font.pixelSize: appSettings.fontSizeMedium
                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally("http://openweathermap.org")
                }
            }

            Button {
                text: "License"
                anchors.horizontalCenter: parent.horizontalCenter

                onClicked: {
    //                pageStack.push(Qt.resolvedUrl("LicensePage.qml"), {pageStack: pageStack});
                    pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/LicensePage.qml"), { pageStack: pageStack });
                }
            }
        }
    }
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            pageStack.pop()
            event.accepted = true
        }
    }
}
