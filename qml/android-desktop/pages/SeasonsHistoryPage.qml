/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../items"

Page {
    id: page

    MainToolBar {
        id: toolbar
        anchors.top: page.top
        width: page.width
        selectedToolIcon: "history"
    }

    ScrollView {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom

        GridView {
            id: gridView
            clip: true

            anchors.fill: parent

            cellWidth: gridView.width / 4
            cellHeight: cellWidth / 3

            delegate: Rectangle {
                id: rectangle
                width: gridView.cellWidth
                height: gridView.cellHeight
                color: area.pressed ? "#22FFFFFF" : "#00000000"

                Label {
                    text: season
                    width: parent.width
                    height: parent.height
                    font.pixelSize: appSettings.fontSizeLarge
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: appSettings.fontColorPrimary
                }

                MouseArea {
                    id: area
                    anchors.fill: parent
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/SeasonPage.qml"), { season: season, pageStack: pageStack});
                    }
                }
            }
            model: listModel
        }
    }

    ListModel {
        id: listModel

        function update()
        {
            var d = new Date();
            var size = d.getFullYear() - 1 - 1950;

            for (var i = d.getFullYear() - 1; i >= 1950; --i)
                append({"season": i});
        }
    }

    Component.onCompleted: listModel.update()
}
