/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property string driverId;
    property int season;

    Component {
        id: driverSeasonResultsDelegate
        DriverSeasonResultsDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 3

            onRoundSelected: {
                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/RaceResultsPage.qml"), { season: page.season, round: round, pageStack: pageStack });
            }
        }
    }

    TitleToolBar {
        id: toolbar
        title: page.season + " season results"
    }

    Column {
        id: headerContainer
        width: page.width


        DriverSeasonInfoItem {
            id: driverSeasonInfo;
            width: page.width
            height: column.height

            Column {

                id: column
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 5

                ClickableLabel {
                    text: driverSeasonInfo.position + ". "+ driverSeasonInfo.name
                    font.underline: true
                    font.pixelSize: appSettings.fontSizeLarge

                    onClicked: {
                        if (appSettings.isAndroid)
                            Qt.openUrlExternally(driverSeasonInfo.url)
                        else
                            pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: driverSeasonInfo.url, pageStack: pageStack} )

                    }

                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Row {
                    width: parent.width

                    Label {
                        text: "Points: " + driverSeasonInfo.points
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }

                    Label {
                        text: "Wins: " + driverSeasonInfo.wins
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }
                }

                Row {
                    width: parent.width

                    Label {
                        text: "Retirements: " + driverSeasonInfo.retirements
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }

                    Label {
                        text: "Podiums: " + driverSeasonInfo.podiums
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }
                }

                Row {
                    width: parent.width

                    Label {
                        text: "Avg position: " + driverSeasonInfo.avgPosition
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }

                    Label {
                        text: "Avg grid: " + driverSeasonInfo.avgGrid
                        width: parent.width * 0.5
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                    }
                }
                Separator { }
            }
        }
    }

    Item {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        anchors.topMargin: appSettings.fontSizeMedium

        ScrollView {
            anchors.fill: parent
            id: dsView
            opacity: 0
            ListView {
                clip: true

                header: Item {
                    id: header
                    width: headerContainer.width
                    height: headerContainer.height
                    Component.onCompleted: headerContainer.parent = header
                }

                anchors.fill: parent

                delegate: driverSeasonResultsDelegate
                model: driverSeasonResultsModel
            }
        }
        ViewPlaceholder {
            anchors.fill: parent
            opacity: 1 - dsView.opacity
        }
    }

    Connections {
        target: dataManager
        onDriverResultsObtained: {
            driverSeasonResultsModel.update(sd, driverId)
            driverSeasonInfo.updateData(sd, driverId)
            dsView.opacity = 1
        }
        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    NotificationArea {
        id: notification
    }

    Component.onCompleted: {
        dataManager.getDriverResults(page.driverId, page.season)
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            pageStack.pop()
            event.accepted = true
        }
    }
}
