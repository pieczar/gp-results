/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import GPComponents 1.0
import "../delegates"
import "../items"

Page {

    id: page
    property string driverId;
    property int season;
    property int round;

    TitleToolBar {
        id: toolbar
        title: "Race details"
    }
    Column {
        width: page.width
        id: headerContainer


        DriverRaceInfoItem {
            id: driverRaceInfo
            width: page.width
            height: column.height

            Column {

                id: column
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 5

                ClickableLabel {
                    text: driverRaceInfo.position + ". "+ driverRaceInfo.name
                    font.underline: true
                    font.pixelSize: appSettings.fontSizeLarge

                    onClicked: {
                        if (appSettings.isAndroid)
                            Qt.openUrlExternally(driverRaceInfo.url)
                        else
                            pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: driverRaceInfo.url, pageStack: pageStack} )

                    }

                    anchors.horizontalCenter: parent.horizontalCenter
                }


                Row {
                    width: parent.width

                    Label {
                        width: parent.width * 0.3
                        text: "Grid: " + driverRaceInfo.grid
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorPrimary
                    }
                    Label {
                        visible: (page.season > 2010)
                        width: parent.width * 0.7
                        text: "Best lap: " + driverRaceInfo.fastestLap + " (L" + driverRaceInfo.fastestLapNumber +  ")"
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorPrimary
                        horizontalAlignment: Text.AlignRight
                    }
                }
                Row {
                    width: parent.width

                    Label {
                        width: parent.width * 0.5
                        text: "Status: " + driverRaceInfo.status
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorPrimary
                    }
                    Label {
                        visible: (page.season > 2010)
                        width: parent.width * 0.5
                        text: "Avg speed: " + driverRaceInfo.avgSpeed
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorPrimary
                        horizontalAlignment: Text.AlignRight
                    }
                }
                Separator {}
            }
        }
    }

    Component {
        id: lapTimesDelegate
        LapTimesDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 1.5
        }
    }

    Item {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        anchors.topMargin: appSettings.fontSizeMedium

        ScrollView {
            id: ltView
            anchors.fill: parent
            opacity: 0
            ListView {
                clip: true
                width: headerContainer.width
                header: Column {
                   Item {
                        id: header
                        width: headerContainer.width
                        height: headerContainer.height
                        Component.onCompleted: headerContainer.parent = header
                    }
                   Row {
                       id: row
                       width: parent.width-40
                       visible: (page.season > 2010)
                       spacing: 10

                       Label {
                           text: "Lap"
                           width: row.width * 0.1
                           font.pixelSize: appSettings.fontSizeSmall
                           horizontalAlignment: Text.AlignRight
                           color: appSettings.fontColorSecondary
                       }
                       Label {
                           text: "P"
                           width: row.width * 0.1
                           font.pixelSize: appSettings.fontSizeSmall
                           horizontalAlignment: Text.AlignRight
                           color: appSettings.fontColorSecondary
                       }
                       Label {
                           text: "Time"
                           width: row.width * 0.5
                           font.pixelSize: appSettings.fontSizeSmall
                           horizontalAlignment: Text.AlignHCenter
                           color: appSettings.fontColorSecondary
                       }
                       Label {
                           text: "Diff"
                           width: row.width * 0.3
                           font.pixelSize: appSettings.fontSizeSmall
                           horizontalAlignment: Text.AlignRight
                           color: appSettings.fontColorSecondary
                       }
                   }
                }

                anchors.fill: parent

                delegate: lapTimesDelegate
                model: lapTimesModel

            }
        }
        ViewPlaceholder {
            anchors.fill: parent
            opacity: 1 - ltView.opacity
        }
    }
    Connections {
        target: dataManager
        onLapTimesObtained: {
            driverRaceInfo.updateData(ed, driverId)
            lapTimesModel.update(ed, driverId)
            ltView.opacity = 1
        }

        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    Component.onCompleted: {
        dataManager.getLapTimes(page.driverId, page.season, page.round)
    }

    NotificationArea {
        id: notification
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            pageStack.pop()
            event.accepted = true
        }
    }
}
