/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page

    property int season: 2013;

    Component {
        id: raceScheduleDelegate
        RaceScheduleDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 3

            onRaceSelected: {

                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/RaceResultsPage.qml"), { season: page.season, round: round, pageStack: pageStack });
            }
        }
    }

    Component {
        id: driversStandingsDelegate
        DriverStandingsDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 3

            onDriverClicked: {
                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/DriverSeasonResultsPage.qml"), {season: page.season, driverId: driver_id, pageStack: pageStack} )
            }
        }
    }

    Component {
        id: constructorStandingsDelegate
        ConstructorStandingsDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 1.5

            onConstructorClicked: {
                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/ConstructorSeasonResultsPage.qml"), {season: page.season, constructorId: constructor_id, pageStack: pageStack} )
            }
        }
    }


    Column {
        id: headerContainer
        width: page.width

        spacing: 10

        SeasonInfoItem {
            id: seasonInfo
            width: parent.width
            height: col.height

            Column {

                id: col
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right

                ClickableLabel {
                    width: parent.width

                    text: seasonInfo.season + " season"
                    font.underline: true
                    font.pixelSize: appSettings.fontSizeLarge
                    horizontalAlignment: Text.AlignHCenter

                    onClicked: {
                        if (appSettings.isAndroid)
                            Qt.openUrlExternally(seasonInfo.url)
                        else
                            pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: seasonInfo.url, pageStack: pageStack } )

                    }
                }

                Label {
                    text: "Races: " + seasonInfo.rounds
                    width: parent.width
                    color: appSettings.fontColorPrimary
                    font.pixelSize: appSettings.fontSizeMedium
                }

                Row {
                    width: parent.width
                    height: chCol.height

                    Label {
                        text: (seasonInfo.season > 1957) ? "Champions:" : "Champion:"
                        width: parent.width * 0.35
                        color: appSettings.fontColorPrimary
                        font.pixelSize: appSettings.fontSizeMedium
                        height: chCol.height
                        verticalAlignment: Text.AlignVCenter
                    }
                    Column {
                        id: chCol
                        width: parent.width * 0.65
                        Label {
                            text: (seasonInfo.season > 1957) ? seasonInfo.driverChampion + "," : seasonInfo.driverChampion
                            font.pixelSize: appSettings.fontSizeMedium
                            width: parent.width
                            wrapMode: Text.WordWrap
                            color: appSettings.fontColorPrimary
                        }
                        Label {
                            visible: (seasonInfo.season > 1957)
                            font.pixelSize: appSettings.fontSizeMedium
                            text: seasonInfo.constructorChampion
                            width: parent.width
                            color: appSettings.fontColorPrimary
                        }

                    }

                }
            }
        }
        Separator { }

        Loader {
            width: 220
            sourceComponent: (seasonInfo.season > 1957) ?  constComboBox : noConstComboBox
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Component {
            id: constComboBox
            ComboBox {
                id: cb
                model: ListModel {
                    id: cbItems
                    ListElement { text: "Drivers standings"; }
                    ListElement { text: "Constructor standings"; }
                    ListElement { text: "Calendar"; }
                }

                onCurrentIndexChanged: {
                    switch (cb.currentIndex)
                    {
                        case 0: loader.sourceComponent = driversList; break;
                        case 1: loader.sourceComponent = constructorsList; break;
                        case 2: loader.sourceComponent = calList; break;
                    }
                }
            }
        }

        Component {
            id: noConstComboBox
            ComboBox {
                id: cb2
                model: ListModel {
                    id: cbItems2
                    ListElement { text: "Drivers standings"; }
                    ListElement { text: "Calendar"; }
                }

                onCurrentIndexChanged: {
                    switch (cb2.currentIndex)
                    {
                        case 0: loader.sourceComponent = driversList; break;
                        case 1: loader.sourceComponent = calList; break;
                    }
                }
            }
        }
    }

    TitleToolBar {
        id: toolbar
        title: "Season details"
    }

    Item {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        anchors.topMargin: appSettings.fontSizeMedium

        Loader {
            id: loader
            anchors.fill: parent
            sourceComponent: driversList
            opacity: 0
        }
        ViewPlaceholder {
            anchors.fill: parent
            opacity: 1 - loader.opacity
        }
    }


    Component {
        id: calList

        ScrollView {
            ListView {

                clip: true

                header: Item {
                    id: calHeaderItem
                    width: headerContainer.width
                    height: headerContainer.height
                    Component.onCompleted: headerContainer.parent = calHeaderItem
                }

                anchors.fill: parent

                delegate: raceScheduleDelegate
                model: raceScheduleModel
            }
        }
    }

    Component {
        id: driversList
        ScrollView {
            ListView {

                clip: true

                header: Column {
                    width: headerContainer.width
                    Item {
                        id: driversHeaderItem
                        width: headerContainer.width
                        height: headerContainer.height
                        Component.onCompleted: headerContainer.parent = driversHeaderItem
                    }
                    DriverStandingsHeader {
                        width: page.width
                        height: appSettings.fontSizeMedium * 1.5
                    }
                }

                anchors.fill: parent

                delegate: driversStandingsDelegate
                model: driverStandingsModel

            }
        }
    }

    Component {
        id: constructorsList

        ScrollView {
            ListView {

                clip: true

                header: Column {
                    width: headerContainer.width
                    Item {
                        id: constructorsHeaderItem
                        width: headerContainer.width
                        height: headerContainer.height
                        Component.onCompleted: headerContainer.parent = constructorsHeaderItem
                    }
                    DriverStandingsHeader {
                        width: page.width
                        height: appSettings.fontSizeMedium * 1.5
                    }
                }

                anchors.fill: parent

                delegate: constructorStandingsDelegate
                model: constructorStandingsModel

            }
        }
    }


    Connections {
        target: dataManager
        onDriverStandingsObtained: {
            driverStandingsModel.update(sd);
            seasonInfo.updateData(sd);
            loader.opacity = 1
        }
        onConstructorStandingsObtained: {
            constructorStandingsModel.update(sd);
            seasonInfo.updateData(sd);
        }
        onRaceScheduleObtained: {
            raceScheduleModel.update(sd);
            seasonInfo.updateData(sd);
        }

        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    Component.onCompleted: {
        dataManager.getRaceSchedule(page.season);
        dataManager.getDriverStandings(page.season);
        dataManager.getConstructorStandings(page.season);
    }

    NotificationArea {
        id: notification
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            pageStack.pop()
            event.accepted = true
        }
    }
}
