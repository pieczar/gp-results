/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property int season: 2014;
    property bool latestResults: false
    property int round: 1;
    property string race;

    TitleToolBar {
        id: toolbar
        title: "Round " + eventInfoItem.round;
    }

    property string activeView: "rList"

    Item {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        anchors.topMargin: appSettings.fontSizeMedium

        Loader {
            id: loader
            opacity: 0
            anchors.fill: parent
            sourceComponent: activeView == "rList" ? rrComponent : qrComponent
        }
        ViewPlaceholder {
            anchors.fill: parent
            opacity: 1 - loader.opacity
        }

    }

    Component {
        id: raceResultsDelegate
        RaceResultsDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 3
            onRaceResultClicked: {

                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/DriverRaceDataPage.qml"), {season: eventInfoItem.season, round: eventInfoItem.round, driverId: driver_id, pageStack: pageStack} )
            }
        }
    }

    Component {
        id: qualiResultsDelegate
        QualiResultsDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 3
            onQualiResultClicked: {

                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/DriverRaceDataPage.qml"), {season: eventInfoItem.season, round: eventInfoItem.round, driverId: driver_id, pageStack: pageStack} )
            }
        }
    }

    Column {        
        width: parent.width
        id: headerContainer
        spacing: 15

        EventInfoItem {
            id: eventInfoItem
            height: col.height
            width: page.width

            Column {
                id: col
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 5

                ClickableLabel {
                    width: parent.width
                    text: eventInfoItem.season + " " + eventInfoItem.raceName
                    font.pixelSize: appSettings.fontSizeLarge
                    font.underline: true
                    wrapMode: Text.WordWrap

                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter

                    onClicked: {
                        if (appSettings.isAndroid)
                            Qt.openUrlExternally(eventInfoItem.infoURL)
                        else
                            pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: eventInfoItem.infoURL, pageStack: pageStack } )

                    }

                }
                ClickableLabel {
                    id: nameLabel
                    text: eventInfoItem.circuit
                    font.pixelSize: appSettings.fontSizeMedium

                    onClicked: {
                        if (appSettings.isAndroid)
                            Qt.openUrlExternally(eventInfoItem.circuitURL)
                        else
                            pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: eventInfoItem.circuitURL, pageStack: pageStack } )

                    }
                }

                Row {
                    id: row
                    width: parent.width

                    Label {
                        id: countryLabel
                        width: row.width * 0.7
                        text: eventInfoItem.location + ", " + eventInfoItem.country
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorSecondary
                        verticalAlignment: Text.AlignVCenter
                    }
                    Label {
                        id: dateLabel
                        text: eventInfoItem.raceDate
                        width: row.width * 0.3
                        font.pixelSize: appSettings.fontSizeSmall
                        color: appSettings.fontColorPrimary
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                    }
                }

                Separator {}

                Label {
                    id: flLabel

                    //fastest laps available from 2004 season onwards
                    visible: (page.season > 2003) && eventInfoItem.raceCompleted
                    text: "Fastest lap: " + eventInfoItem.fastestLap + ", " + eventInfoItem.fastestLapDriver + " (L" + eventInfoItem.fastestLapNumber + ")"
                    width: parent.width
                    font.pixelSize: appSettings.fontSizeSmall - 2
                    color: appSettings.fontColorPrimary

                }
            }
        }        

        Row {
            id: row2
            anchors.margins: 5
            anchors.left: parent.left
            anchors.right: parent.right
            height: cb.height

            visible: eventInfoItem.qualiResultsPresent && eventInfoItem.raceCompleted

            Label {
                id: label
                color: appSettings.fontColorPrimary
                text: "Results: "
                font.pixelSize: appSettings.fontSizeMedium
                anchors.verticalCenter: parent.verticalCenter
            }

            ComboBox {
                width: 200
                id: cb
                model: ListModel {
                    id: cbItems
                    ListElement { text: "Race"; }
                    ListElement { text: "Qualifying"; }
                }
                onCurrentIndexChanged: {
                    if (currentIndex == 0)
                        activeView = "rList"
                    else
                        activeView = "qList"
                }
            }
        }
        EmptySpace {}
    }


    Component {
        id: rrComponent

        ScrollView {
            ListView {
                id: rrView
                clip: true

                header: Item {
                    id: header
                    width: headerContainer.width
                    height: headerContainer.height
                    Component.onCompleted: headerContainer.parent = header
                }

                anchors.fill: parent

                delegate: raceResultsDelegate
                model: raceResultsModel
            }
        }
    }

    Component {
        id: qrComponent

        ScrollView {
            ListView {
                id: qrView
                clip: true

                header: Item {
                    id: qHeader
                    width: headerContainer.width
                    height: headerContainer.height
                    Component.onCompleted: headerContainer.parent = qHeader
                }

                anchors.fill: parent

                delegate: qualiResultsDelegate
                model: qualiResultsModel

            }
        }
    }

    Component.onCompleted: {

        if (latestResults)
        {
            dataManager.getLatestRaceResults();
            dataManager.getLatestQualiResults();
        }
        else
        {
            dataManager.getRaceResults(page.season, page.round);
            dataManager.getQualiResults(page.season, page.round);
        }
    }

    Connections {
        target: dataManager
        onQualiResultsObtained: {
            qualiResultsModel.update(sd, round);
            eventInfoItem.updateData(sd, round);
        }
        onRaceResultsObtained: {
            raceResultsModel.update(sd, round);
            eventInfoItem.updateData(sd, round);
            loader.opacity = 1
        }
        onConnectionError: {
            notification.show("Server or connection error")
        }
    }
    NotificationArea {
        id: notification
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            pageStack.pop()
            event.accepted = true
        }
    }
}
