/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property string cityId
    property string city

    TitleToolBar {
        id: toolbar
        title: "Weather forecast"
    }

    Component {
        id: weatherDelegate
        WeatherForecastDelegate {
            width: page.width - 10
            height: appSettings.fontSizeMedium * 3
        }
    }

    ScrollView {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.topMargin: appSettings.fontSizeMedium
        anchors.bottom: page.bottom
        ListView {
            id: listView
            header: Column {
                width: page.width

                Button {
                    anchors.horizontalCenter: parent.horizontalCenter

                    text: "Full weather report for " + page.city
                    onClicked: {
                        Qt.openUrlExternally("http://openweathermap.org/city/" + page.cityId)
                    }
                }
                EmptySpace {}
            }
            spacing: 12

            anchors.fill: parent

            delegate: weatherDelegate
            model: weatherForecastModel
            clip: true
        }
    }


    Component.onCompleted: {
        dataManager.getWeatherForecast();
    }

    Connections  {
        target: dataManager
        onWeatherForecastObtained: {
            weatherForecastModel.update(wd);
            page.cityId = cityId
            page.city = city
        }
        onConnectionError: {
            notification.show("Server or connection error")
        }
    }
    NotificationArea {
        id: notification
    }

    focus: true
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            pageStack.pop()
            event.accepted = true
        }
    }
}
