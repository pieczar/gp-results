/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtWebKit 3.0
import QtQuick.Controls 1.0
import "../items"

Page {
    id: webViewPage

    property string url;


    PageToolBar {
        id: toolbar
        property string title
        Row {
            anchors.fill: parent
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            Image {
                id: button
                source: "qrc:/icons/back.png"
                height: toolbar.height * 0.6
                fillMode: Image.PreserveAspectFit

                anchors.verticalCenter: parent.verticalCenter

                scale: ma.pressed ? 0.8 : 1

                MouseArea {
                    id: ma
                    anchors.fill: parent
                    onClicked: {
                        pageStack.pop()
                    }
                }
            }

            Row {
                height: parent.height
                width: parent.width - button.width
                spacing: 10

                Item {
                    height: parent.height
                    width: parent.width - button.width - backImg.width - forwardImg.width - reloadImg.width
                }

                Image {
                    id: backImg
                    source: "qrc:/icons/browser_back.png"
                    height: toolbar.height * 0.6
                    fillMode: Image.PreserveAspectFit

                    anchors.verticalCenter: parent.verticalCenter

                    scale: (ma2.pressed && webView.canGoBack) ? 0.8 : 1

                    opacity: webView.canGoBack ? 1 : 0.5

                    MouseArea {
                        id: ma2
                        anchors.fill: parent
                        onClicked: {
                            webView.goBack()
                        }
                    }
                }
                Image {
                    id: reloadImg
                    source: "qrc:/icons/browser_reload.png"
                    height: toolbar.height * 0.6
                    fillMode: Image.PreserveAspectFit

                    anchors.verticalCenter: parent.verticalCenter

                    scale: ma3.pressed ? 0.8 : 1

                    MouseArea {
                        id: ma3
                        anchors.fill: parent
                        onClicked: {

                            if (!webView.loading)
                                webView.reload()
                            else
                                webView.stop()
                        }
                    }
                }

                Image {
                    id: forwardImg
                    source: "qrc:/icons/browser_forward.png"
                    height: toolbar.height * 0.6
                    fillMode: Image.PreserveAspectFit

                    anchors.verticalCenter: parent.verticalCenter

                    scale: (ma4.pressed && webView.canGoForward) ? 0.8 : 1

                    opacity: webView.canGoForward ? 1 : 0.5

                    MouseArea {
                        id: ma4
                        anchors.fill: parent
                        onClicked: {
                            webView.goForward()
                        }
                    }
                }
            }
        }
    }


    ScrollView {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: webViewPage.bottom

        WebView {

            anchors.fill: parent
            id: webView
            url: webViewPage.url

            //force usage of wiki mobile version
            Component.onCompleted: {
                var str = webView.url.toString();

                if (str.indexOf("m.wikipedia") == -1)
                    webView.url = str.replace("wikipedia", "m.wikipedia");
            }

            onLoadProgressChanged: {

                if (loadProgress == 100)
                {
                    reloadImg.opacity = 1
                    reloadImg.rotation = 0
                }
                else
                    reloadImg.opacity = 0.5

            }

            Timer {
                running: webView.loading
                interval: 20
                repeat: true

                onTriggered: {
                    reloadImg.rotation += 2
                }
            }

            onLoadingChanged : {
                if (webView.LoadStatus === WebView.LoadFailedStatus) {
                    notification.show("Server or connection error")
                }
            }
        }
    }
    NotificationArea {
        id: notification
    }
}
