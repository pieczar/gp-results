/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import GPComponents 1.0
import "../items"

Page {
    id: page

    BorderImage {
        anchors.fill: parent
        source: "qrc:/icons/bg.jpg"
    }

    MainToolBar {
        id: toolbar
        anchors.top: page.top
        width: page.width
        selectedToolIcon: "flags"
    }

    Flickable {
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        width: parent.width

        contentHeight: nrItem.height
        clip: true

    NextRaceItem {
        id: nrItem
        width: parent.width        
        height: Math.max(page.height - toolbar.height, col.height)
//        anchors.top: toolbar.bottom
//        anchors.bottom: page.bottom
        opacity: 0

        Column {
            id: col
            width: parent.width            

            anchors.centerIn: parent

            spacing: 15

            Image {
                source: "qrc:/icons/flags.png"
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                width: parent.width / 3

                anchors.horizontalCenter: parent.horizontalCenter

                horizontalAlignment: Image.AlignHCenter
            }

            Rectangle {
                width: parent.width
                height: appSettings.fontSizeMedium
                color: "#00000000"
            }

            Label {
                id: lab
                width: parent.width
                text: "Next race:"
                font.bold: true
                color: appSettings.fontColorPrimary
                font.pixelSize: appSettings.fontSizeExtraLarge
                horizontalAlignment: Text.AlignHCenter
            }

            ClickableLabel {
                id: tx
                width: parent.width
                text: nrItem.raceName
                wrapMode: Text.WordWrap
                font.pixelSize: appSettings.fontSizeLarge
                font.underline: true
                horizontalAlignment: Text.AlignHCenter

                onClicked: {

                    if (appSettings.isAndroid)
                        Qt.openUrlExternally(nrItem.url)
                    else
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: nrItem.url, pageStack: pageStack} )

                }
            }

            ClickableLabel {
                width: parent.width
                text: nrItem.circuitName
                font.underline: true
                font.pixelSize: appSettings.fontSizeSmall
                horizontalAlignment: Text.AlignHCenter

                onClicked: {
                    if (appSettings.isAndroid)
                        Qt.openUrlExternally(nrItem.circuitUrl)
                    else
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: nrItem.circuitUrl, pageStack: pageStack} )

                }
            }

            Label {
                width: parent.width
                text: nrItem.location
                color: appSettings.fontColorSecondary
                font.pixelSize: appSettings.fontSizeSmall
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                width: parent.width
                text: nrItem.raceDate
                color: appSettings.fontColorPrimary
                font.pixelSize: appSettings.fontSizeMedium
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                width: parent.width
                font.bold: true
                font.pixelSize: appSettings.fontSizeLarge
                color: appSettings.fontColorPrimary
                text: nrItem.days + "d  " + nrItem.hours + "h  " + nrItem.minutes + "m  " + nrItem.seconds + "s"
                horizontalAlignment: Text.AlignHCenter
            }

            Button {
                text: "Latest results"
                onClicked: {
                    var d = new Date();
                    pageStack.push(Qt.resolvedUrl("RaceResultsPage.qml"), { latestResults: true, pageStack: pageStack });
                }
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Button {
                text: "Weather forecast"
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/WeatherForecastPage.qml"), {pageStack: pageStack} )
                }
                anchors.horizontalCenter: parent.horizontalCenter

            }            

            Button {
                text: "Settings & About"
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/SettingsPage.qml"), { pageStack: pageStack });
                }
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Timer {
                id: timer
                interval: 1000
                repeat: true
                running: false
                onTriggered: nrItem.updateTime()
            }

            Component.onCompleted: {
                dataManager.getNextRaceData();
            }

            Connections {
                target: dataManager
                onNextRaceDataObtained: {
                    nrItem.setData(ed, cd)
                    nrItem.opacity = 1
                    timer.start()
                }
                onConnectionError: {
                    notification.show("Server or connection error")
                }
            }
        }
        }
    }

    NotificationArea {
        id: notification
    }

    ViewPlaceholder {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom

        opacity: 1 - nrItem.opacity
    }
}
