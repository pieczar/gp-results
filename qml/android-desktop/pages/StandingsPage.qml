/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../delegates"
import "../items"

Page {

    id: page
    property int season: 2014;

    MainToolBar {
        id: toolbar
        anchors.top: page.top
        width: page.width
        selectedToolIcon: "standings"
    }

    Column {
        id: headerContainer
        width: page.width

        Row {
            id: row2
            anchors.margins: 5
            anchors.left: parent.left
            anchors.right: parent.right
            height: cb.height

            Label {
                color: appSettings.fontColorPrimary
                text: "Standings: "
                font.pixelSize: appSettings.fontSizeMedium
                anchors.verticalCenter: parent.verticalCenter
            }

            ComboBox {
                id: cb
                width: 200
                model: ListModel {
                    id: cbItems
                    ListElement { text: "Drivers"; }
                    ListElement { text: "Constructors"; }
                }
                onCurrentIndexChanged: {
                    if (currentIndex == 0)
                        loader.sourceComponent = driversList
                    else
                        loader.sourceComponent = teamsList
                }
            }
        }
        EmptySpace { }

        DriverStandingsHeader {
            width: parent.width
            height: appSettings.fontSizeMedium * 1.5
        }
    }


    Item {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        anchors.topMargin: appSettings.fontSizeMedium

        Loader {
            id: loader
            opacity: 0
            anchors.fill: parent
            sourceComponent: driversList
        }

        ViewPlaceholder {
            anchors.fill: parent
            opacity: 1 - loader.opacity
        }

    }

    Component {
        id: driversStandingsDelegate
        DriverStandingsDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 3

            onDriverClicked: {
                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/DriverSeasonResultsPage.qml"), {season: page.season, driverId: driver_id} )
            }
        }
    }

    Component {
        id: constructorStandingsDelegate
        ConstructorStandingsDelegate {
            width: page.width
            height: appSettings.fontSizeMedium * 1.5

            onConstructorClicked: {
                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/ConstructorSeasonResultsPage.qml"), {season: page.season, constructorId: constructor_id} )
            }
        }
    }


    Component {
        id: driversList

        ScrollView {
            ListView {
                id: dList
                clip: true

                header: Item {
                  id: header
                  width: headerContainer.width
                  height: headerContainer.height
                  Component.onCompleted: headerContainer.parent = header
                }

                anchors.fill: parent

                delegate: driversStandingsDelegate
                model: driverStandingsModel
            }
        }
    }

    Component {
        id: teamsList

        ScrollView {
            ListView {
                id: tList
                clip: true

                header: Item {
                  id: theader
                  width: headerContainer.width
                  height: headerContainer.height
                  Component.onCompleted: headerContainer.parent = theader
                }

                anchors.fill: parent

                delegate: constructorStandingsDelegate
                model: constructorStandingsModel

            }
        }
    }

    Component {
        id: noStandingsView

        Item {
            id: standFlick
            anchors.fill: parent

            Label {
                anchors.fill: parent

                anchors.centerIn: parent.Center
                text: page.season + " standings not available yet"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                font.pixelSize: appSettings.fontSizeLarge
                color: appSettings.fontColorPrimary
                wrapMode: Text.WordWrap
            }
        }
    }

    Connections {
        target: dataManager
        onDriverStandingsObtained: {
            driverStandingsModel.update(sd);
            loader.opacity = 1
        }
        onConstructorStandingsObtained: {
            constructorStandingsModel.update(sd);
            loader.opacity = 1
        }

        onDriverStandingsNotAvailable: {
            loader.sourceComponent = noStandingsView
            loader.opacity = 1
        }
        onConstructorStandingsNotAvailable: {
            loader.sourceComponent = noStandingsView
            loader.opacity = 1
        }
        onConnectionError: {
            notification.show("Server or connection error")
        }
    }
    NotificationArea {
        id: notification
    }

    Component.onCompleted: {
        dataManager.getDriverStandings(page.season)
        dataManager.getConstructorStandings(page.season)
    }
}
