/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0
import "../delegates"
import "../items"

Page {
    id: page

    property int season: 2014

    Component {
        id: raceScheduleDelegate
        RaceScheduleDelegate {
            width: page.width
            height: appSettings.fontSizeMedium *  3

            onRaceSelected: {
                pageStack.push(Qt.resolvedUrl("qrc:/qml/android-desktop/pages/RaceResultsPage.qml"), { season: page.season, round: round, pageStack: pageStack });
            }
        }
    }

    MainToolBar {
        id: toolbar
        anchors.top: page.top
        width: page.width
        selectedToolIcon: "calendar"
    }

    ViewPlaceholder {
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom

        opacity: 1 - listView.opacity
    }

    ScrollView {
        id: listView
        width: parent.width
        anchors.top: toolbar.bottom
        anchors.bottom: page.bottom
        anchors.topMargin: appSettings.fontSizeMedium
        opacity: 0

        ListView {            

            anchors.fill: parent
            clip: true

            delegate: raceScheduleDelegate
            model: raceScheduleModel

            Connections {
                target: dataManager
                onRaceScheduleObtained: {
                    raceScheduleModel.update(sd);
                    listView.opacity = 1
                }
                onConnectionError: {
                    notification.show("Server or connection error")
                }
            }
        }
    }
    Component.onCompleted: {
        dataManager.getRaceSchedule(page.season);
    }

    NotificationArea {
        id: notification
    }
}
