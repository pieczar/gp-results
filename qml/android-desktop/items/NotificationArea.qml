/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0

Rectangle {
    id: notification
    property string text
    width: parent.width
    height: parent.height

    z: 1
    enabled : (opacity == 1)
    opacity: 0

    color: "#AA000000"

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right

        anchors.margins: 10
        anchors.verticalCenter: parent.verticalCenter

        height: text.height + 20

        radius: 5

        color: "#111111"

        border.color: "#EEEEEE"
        border.width: 2

        Label {
            id: text
            text: notification.text
            width: parent.width

            horizontalAlignment: Text.AlignHCenter
            anchors.verticalCenter: parent.verticalCenter

            font.pixelSize: appSettings.fontSizeMedium
            color: appSettings.fontColorPrimary
        }
    }

    Behavior on opacity {
        PropertyAnimation {  }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
           notification.opacity = 0
        }
    }

    function show(text)
    {
        notification.text = text
        notification.opacity = 1

        timer.start()
    }

//    Behavior on opacity {
//        FadeAnimation{ }
//    }

    Timer {
        id: timer
        interval: 10000;
        repeat: false
        onTriggered: {
            notification.opacity = 0
        }
    }
}
