/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0

PageToolBar {
    property string selectedToolIcon : "flags"

    id: toolBar

    Row {
        id: row
        anchors.fill: parent
        Rectangle {
            height: parent.height
            width: row.width / 4

            color: "#00000000"

            Image {
                source: "qrc:/icons/flags-white.png"
                smooth: true
                fillMode: Image.PreserveAspectFit

                anchors.verticalCenter: parent.verticalCenter
                anchors.fill: parent
                anchors.margins: 3

                MouseArea {
                    id: ma4
                    anchors.fill: parent
                    enabled : selectedToolIcon != "flags"
                    onClicked: {
                        pageStack.replace(Qt.resolvedUrl("../pages/RaceCountdownPage.qml"), {pageStack: pageStack}, true )
                    }
                }
            }
            Rectangle {
                anchors.bottom: parent.bottom
                height: 2
                color: appSettings.fontColorSecondary
                width: parent.width
                visible: selectedToolIcon == "flags"
            }
        }

        Rectangle {
            height: parent.height
            width: row.width / 4

            color: "#00000000"

            Image {
                id: img
                source: "qrc:/icons/calendar-white.png"
                smooth: true
                fillMode: Image.PreserveAspectFit

                anchors.verticalCenter: parent.verticalCenter
                anchors.fill: parent
                anchors.margins: 3

                MouseArea {
                    id: ma
                    enabled : selectedToolIcon != "calendar"
                    anchors.fill: parent
                    onClicked: {
                        var d = new Date();
                        pageStack.replace(Qt.resolvedUrl("../pages/CalendarPage.qml"), {pageStack: pageStack, season: d.getFullYear()}, true )
                    }
                }
            }
            Rectangle {
                anchors.bottom: parent.bottom
                height: 2
                color: appSettings.fontColorSecondary
                width: parent.width
                visible: selectedToolIcon == "calendar"
            }
        }

        Rectangle {
            height: parent.height
            width: row.width / 4

            color: "#00000000"

            Image {
                source: "qrc:/icons/standings-white.png"
                smooth: true
                fillMode: Image.PreserveAspectFit

                anchors.verticalCenter: parent.verticalCenter
                anchors.fill: parent
                anchors.margins: 3

                MouseArea {
                    id: ma2
                    enabled : selectedToolIcon != "standings"
                    anchors.fill: parent
                    onClicked: {
                        var d = new Date();
                        pageStack.push(Qt.resolvedUrl("../pages/StandingsPage.qml"), {pageStack: pageStack, season: d.getFullYear()}, true )
                    }
                }
            }
            Rectangle {
                anchors.bottom: parent.bottom
                height: 2
                color: appSettings.fontColorSecondary
                width: parent.width
                visible: selectedToolIcon == "standings"
            }
        }

        Rectangle {
            height: parent.height
            width: row.width / 4

            color: "#00000000"

            Image {
                source: "qrc:/icons/history-white.png"
                smooth : true
                fillMode: Image.PreserveAspectFit

                anchors.verticalCenter: parent.verticalCenter
                anchors.fill: parent
                anchors.margins: 3

                MouseArea {
                    id: ma3
                    enabled : selectedToolIcon != "history"
                    anchors.fill: parent
                    onClicked: {
                        pageStack.replace(Qt.resolvedUrl("../pages/SeasonsHistoryPage.qml"), {pageStack: pageStack}, true )
                    }
                }
            }
            Rectangle {
                anchors.bottom: parent.bottom
                height: 2
                color: appSettings.fontColorSecondary
                width: parent.width
                visible: selectedToolIcon == "history"
            }
        }
    }
}
