/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.0

PageToolBar {
    id: toolbar
    property string title
    Row {
        anchors.fill: parent
        anchors.leftMargin: 10
        anchors.rightMargin: 10
        Image {
            id: button
            source: "qrc:/icons/back.png"
            height: toolbar.height * 0.6
            fillMode: Image.PreserveAspectFit

            anchors.verticalCenter: parent.verticalCenter

            scale: ma.pressed ? 0.8 : 1

            MouseArea {
                id: ma
                anchors.fill: parent
                onClicked: {
                    pageStack.pop()
                }
            }
        }
        Label {
            width: parent.width - button.width
            height: parent.height
            color: appSettings.fontColorPrimary
            font.bold: true
            font.italic: true
            font.pixelSize: appSettings.fontSizeExtraLarge
            text: title
            horizontalAlignment: "AlignRight"
            verticalAlignment: "AlignVCenter"
        }
    }
}
