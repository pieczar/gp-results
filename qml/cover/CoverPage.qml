/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0

CoverBackground {
    NextRaceItem {
        id: nrItem
        //visible: false
        width: parent.width
        height: parent.height

        Column {
            width: parent.width
            spacing: 10
            anchors.centerIn: parent

            Image {
                source: "qrc:/icons/flags.png"
                fillMode: Image.PreserveAspectFit
                width: parent.width / 2

                anchors.horizontalCenter: parent.horizontalCenter

                horizontalAlignment: Image.AlignHCenter
            }

            Label {
                width: parent.width
                text: "Next race:"
                font.bold: true

                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                id: tx
                width: parent.width
                text: nrItem.raceName
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                width: parent.width
                text: nrItem.days + "d  " + nrItem.hours + "h  " + nrItem.minutes + "m  " + nrItem.seconds + "s"
                horizontalAlignment: Text.AlignHCenter
            }

            Timer {
                id: timer
                interval: 1000
                repeat: true
                running: false
                onTriggered: nrItem.updateTime()
            }

            Connections {
                target: dataManager
                onNextRaceDataObtained: {
                    nrItem.setData(ed, cd)
                    nrItem.visible = true
                    timer.start()
                }

            }
        }
    }

    Component.onCompleted: {
        dataManager.getNextRaceData();
    }

}


