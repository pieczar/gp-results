/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {

    contentHeight: Theme.itemSizeSmall / 1.6
    Row {
        id: row
        width: parent.width-40
//        height: parent.height
        spacing: 10

        Label {
            text: number
            width: row.width * 0.1
            horizontalAlignment: Text.AlignRight
            font.bold: best_time
        }
        Label {
            text: position
            width: row.width * 0.1
            horizontalAlignment: Text.AlignRight
            font.bold: best_time
        }
        Label {
            text: time
            width: row.width * 0.5
            horizontalAlignment: Text.AlignHCenter
            font.bold: best_time
        }
        Label {
            text: gap
            width: row.width * 0.3
            horizontalAlignment: Text.AlignRight
            font.bold: best_time
        }
    }
}
