/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {

    contentHeight: Theme.itemSizeExtraLarge
    Row {
        id: row
        width: parent.width-20
        height: parent.height
        spacing: 5

        Column {
            width: row.width * 0.18
            anchors.verticalCenter: parent.verticalCenter

            Label {
                text: day
                width: parent.width
                font.pixelSize: Theme.fontSizeLarge
            }
            Label {
                text: date
                width: parent.width
                font.pixelSize: Theme.fontSizeMedium
                color: Theme.secondaryColor
            }
        }

        Column {
            width: row.width * 0.3
            anchors.verticalCenter: parent.verticalCenter
            spacing: 10

            Label {
                text: day_temp + "º/" + night_temp + "º"
                width: parent.width
                font.pixelSize: Theme.fontSizeSmall
            }
            Label {
                text: pressure + "hpa"
                width: parent.width
                font.pixelSize: Theme.fontSizeSmall
            }
        }

        Column {
            width: row.width * 0.32
            anchors.verticalCenter: parent.verticalCenter
            spacing: 10

            Label {
                text: humidity + "%"
                width: parent.width
                font.pixelSize: Theme.fontSizeSmall
            }

            Row {
                width: parent.width
                height: label.height
                Label {
                    id: label
                    text: wind_speed + "m/s"
                    width: parent.width / 2
                    font.pixelSize: Theme.fontSizeSmall
                }
                Image {
                    source: "qrc:/weather_icons/wind.png"
                    rotation: wind_direction
                    fillMode: Image.PreserveAspectFit
                    height: label.height

                    anchors.verticalCenter: parent.verticalCenter
                    verticalAlignment: Image.AlignVCenter
                }
            }
        }

        Image {
            source: "qrc:/weather_icons/" + icon + ".png"
            fillMode: Image.PreserveAspectFit
            height: parent.height

            anchors.verticalCenter: parent.verticalCenter
            verticalAlignment: Image.AlignVCenter
        }

    }
}
