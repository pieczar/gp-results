/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {

    id: item
    signal raceResultClicked
    contentHeight: Theme.itemSizeSmall

    Row {
        id: row
        width: parent.width-60
        height: parent.height
        spacing: 10

        Label {
            text: position
            width: row.width * 0.1
            font.pixelSize: Theme.fontSizeExtraLarge
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }

        Label {
            text: grid
            font.pixelSize: Theme.fontSizeSmall
            width: row.width * 0.05
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            color: Theme.secondaryColor
        }

        Column {
            height: parent.height
            width: row.width * 0.45

            Label {
                text: name
                font.pixelSize: Theme.fontSizeMedium
                verticalAlignment: Text.AlignBottom
            }
            Label {
                text: team_name
                font.pixelSize: Theme.fontSizeExtraSmall
                color: Theme.secondaryColor
                verticalAlignment: Text.AlignTop
            }
        }

        Label {
            text: time
            width: row.width * 0.3
            height: parent.height
            font.pixelSize: Theme.fontSizeSmall
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }

        Column {
            height: parent.height
            width: row.width * 0.1

            Label {
                text: laps
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignBottom
            }
            Label {
                text: points
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignTop
                color: Theme.secondaryColor
            }
        }
    }
    onClicked: {
        raceResultClicked(driver_id, round)
    }
}
