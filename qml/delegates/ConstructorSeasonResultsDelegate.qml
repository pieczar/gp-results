/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0

ListItem {

    id: item
    signal roundSelected;
    contentHeight: Theme.itemSizeSmall / 2


    Row {
        id: row
        width: parent.width-20
        height: parent.height
        spacing: 10

        Label {
            text: round
            width: row.width * 0.1
            height: parent.height
            font.pixelSize: Theme.fontSizeExtraLarge
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter

            onTextChanged: {
                crModel.update(constructorSeasonResultsModel.getSeasonData(), constructor_id, round)
            }
        }

        Column {
            width: parent.width

            Label {
                text: name
                width: row.width * 0.9
            }

            ListView {
                clip: true
                interactive: false
                id: lv

                width: row.width * 0.9
                height: Theme.itemSizeSmall

                delegate:

                    Row {
                    width: row.width * 0.9
                    height: Theme.itemSizeSmall / 2

                    Label {
                        text: driver_name
                        font.pixelSize: Theme.fontSizeSmall
                        width: parent.width * 0.5
                        color: Theme.secondaryColor
                        verticalAlignment: Text.AlignTop

                        onTextChanged: {
                            item.updateHeights(crModel.rowCount())
                        }
                    }

                    Label {
                        text: driver_position + " (" + driver_grid + ")"
                        font.pixelSize: Theme.fontSizeSmall
                        width: parent.width * 0.15
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignTop
                    }

                    Label {
                        text: driver_status
                        font.pixelSize: Theme.fontSizeSmall
                        width: parent.width * 0.25
                        color: Theme.secondaryColor
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignTop
                    }

                    Label {
                        text: driver_points + "P"
                        width: parent.width * 0.1
                        font.pixelSize: Theme.fontSizeSmall
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignTop
                    }
                }

                model: ConstructorRaceResultsModel {
                    id: crModel
                }
            }
        }
    }
    onClicked: {
        roundSelected(round)
    }

    function updateHeights(rows)
    {
        lv.height = (Theme.itemSizeSmall / 2) * rows;
        item.contentHeight = (Theme.itemSizeSmall / 2) * (rows + 1)
        row.height = (Theme.itemSizeSmall / 2) * (rows + 1);
    }
}
