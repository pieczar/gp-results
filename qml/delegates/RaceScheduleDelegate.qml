/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {

    id: item
    contentHeight: Theme.itemSizeSmall
    signal raceSelected

    Row {
        id: row
        width: parent.width-30
        height: parent.height
        spacing: 10

        Label {
            text: round
            width: row.width * 0.1
            font.pixelSize: Theme.fontSizeExtraLarge//Label.font.pointSize * 1.5
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter

            color: race_completed ? Theme.primaryColor : Theme.secondaryColor
        }

        Column {
            height: parent.height
            width: row.width * 0.7

            Label {
                text: race_name
                width: parent.width
                verticalAlignment: Text.AlignBottom
            }

            Label {
                text: location
                font.pixelSize: Theme.fontSizeExtraSmall
                width: parent.width
                color: Theme.secondaryColor
                verticalAlignment: Text.AlignTop
            }
        }

        Label {
            text: race_date
            width: row.width * 0.2
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter

            color: race_completed ? Theme.primaryColor : Theme.secondaryColor
        }

    }
    onClicked: {
        item.raceSelected(round)
    }
}
