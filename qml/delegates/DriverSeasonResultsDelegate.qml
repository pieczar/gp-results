/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

ListItem {

    signal roundSelected;

    Row {
        id: row
        width: parent.width-40
        height: parent.height
        spacing: 10

        Label {
            text: round
            width: row.width * 0.1
            height: parent.height
            font.pixelSize: Theme.fontSizeExtraLarge
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }

        Column {
            id: col
            width: row.width * 0.6
            Label {
                text: name
                width: parent.width
                font.pixelSize: Theme.fontSizeMedium
                verticalAlignment: Text.AlignBottom                
            }
            Label {
                text: team
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                color: Theme.secondaryColor
                verticalAlignment: Text.AlignTop
            }
        }

        Column {
            width: row.width * 0.2

            Label {
                text: position + " (" + grid + ")"
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                 verticalAlignment: Text.AlignBottom
            }

            Label {
                text: status
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: Theme.fontSizeExtraSmall
                color: Theme.secondaryColor
                verticalAlignment: Text.AlignTop
            }
        }
        Label {
            text: points + "P"
            width: row.width * 0.1
            height: parent.height
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
        }
    }

    onClicked: {
        roundSelected(round)
    }
}
