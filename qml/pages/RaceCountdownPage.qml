/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0
import "../items"


Page {
    id: page

    NotificationArea {
        id: notification
        anchors.top: page.top
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: "History";

                onClicked: {
                    pageStack.replace(Qt.resolvedUrl("SeasonsHistoryPage.qml"));
                }
            }

            MenuItem {
                text: "Standings";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("StandingsPage.qml"), { season: d.getFullYear() });
                }
            }
            MenuItem {
                text: "Calendar";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("CalendarPage.qml"), { season: d.getFullYear() });
                }
            }
        }

        PushUpMenu {

            MenuItem {
                text: "Latest results"

                onClicked: {
                    var d = new Date();
                    pageStack.push(Qt.resolvedUrl("RaceResultsPage.qml"), { latestResults: true });
                }
            }

            MenuItem {
                text: "Weather forecast"

                onClicked: {
                    pageStack.push(Qt.resolvedUrl("WeatherForecastPage.qml"));
                }
            }

            MenuItem {
                text: "About"
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("AboutPage.qml"));
                }
            }
        }

        NextRaceItem {
            id: nrItem
            width: parent.width
            height: parent.height

            Column {
                width: parent.width

                anchors.centerIn: parent
                spacing: 10

                Image {
                    source: "qrc:/icons/flags.png"
                    fillMode: Image.PreserveAspectFit
                    width: parent.width / 3

                    anchors.horizontalCenter: parent.horizontalCenter

                    horizontalAlignment: Image.AlignHCenter
                }

                Rectangle {
                    width: parent.width
                    height: 80
                    color: "#00000000"
                }

                Label {
                    id: lab
                    width: parent.width
                    text: "Next race:"
                    font.bold: true
                    horizontalAlignment: Text.AlignHCenter
                }

                ClickableLabel {
                    id: tx
                    width: parent.width
                    text: nrItem.raceName
                    wrapMode: Text.WordWrap
                    font.pixelSize: Theme.fontSizeLarge
                    font.underline: true
                    horizontalAlignment: Text.AlignHCenter

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: nrItem.url} )
                    }
                }

                ClickableLabel {
                    width: parent.width
                    text: nrItem.circuitName                    
                    font.underline: true
                    font.pixelSize: Theme.fontSizeSmall
                    horizontalAlignment: Text.AlignHCenter

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: nrItem.circuitUrl} )
                    }
                }

                Label {
                    width: parent.width
                    text: nrItem.location
                    color: Theme.secondaryColor
                    font.pixelSize: Theme.fontSizeSmall
                    horizontalAlignment: Text.AlignHCenter
                }

                Label {
                    width: parent.width
                    text: nrItem.raceDate
                    horizontalAlignment: Text.AlignHCenter
                }

                Label {
                    width: parent.width
                    font.bold: true
                    font.pixelSize: Theme.fontSizeLarge
                    text: nrItem.days + "d  " + nrItem.hours + "h  " + nrItem.minutes + "m  " + nrItem.seconds + "s"
                    horizontalAlignment: Text.AlignHCenter
                }                

                Rectangle {
                    width: parent.width
                    height: 80
                    color: "#00000000"
                }

                Row {
                    id: row
                    width: parent.width
                    height: ib.height

                    IconButton {
                        width: row.width / 3
                        icon.source: "image://theme/icon-l-date"

                        onClicked: {
                            var d = new Date();
                            pageStack.replace(Qt.resolvedUrl("CalendarPage.qml"), { season: d.getFullYear() });
                        }
                    }

                    IconButton {
                        id: ib
                        width: row.width / 3
                        icon.source: "qrc:/icons/standings.png"

                        onClicked: {
                            var d = new Date();
                            pageStack.replace(Qt.resolvedUrl("StandingsPage.qml"), { season: d.getFullYear() });
                        }
                    }

                    IconButton {
                        width: row.width / 3
                        icon.source: "qrc:/icons/history.png"

                        onClicked: {
                            pageStack.replace(Qt.resolvedUrl("SeasonsHistoryPage.qml"));
                        }
                    }
                }

                Timer {
                    id: timer
                    interval: 1000
                    repeat: true
                    running: false
                    onTriggered: nrItem.updateTime()
                }

                Component.onCompleted: {
                    dataManager.getNextRaceData();                    
                }

                Connections {
                    target: dataManager
                    onNextRaceDataObtained: {
                        nrItem.setData(ed, cd)
                        nrItem.visible = true
                        timer.start()
                    }
                    onConnectionError: {
                        notification.show("Server or connection error")
                    }
                }
            }
        }
    }
}
