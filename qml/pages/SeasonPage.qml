/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page

    property int season: 2013;

    NotificationArea {
        id: notification
        anchors.top: page.top
    }

    Component {
        id: raceScheduleDelegate
        RaceScheduleDelegate {
            width: page.width

            onRaceSelected: {

                pageStack.push(Qt.resolvedUrl("RaceResultsPage.qml"), { season: page.season, round: round });
            }
        }
    }    

    Component {
        id: driversStandingsDelegate
        DriverStandingsDelegate {
            width: page.width

            onDriverClicked: {
                pageStack.push(Qt.resolvedUrl("DriverSeasonResultsPage.qml"), {season: page.season, driverId: driver_id} )
            }
        }
    }

    Component {
        id: constructorStandingsDelegate
        ConstructorStandingsDelegate {
            width: page.width

            onConstructorClicked: {
                pageStack.push(Qt.resolvedUrl("ConstructorSeasonResultsPage.qml"), {season: page.season, constructorId: constructor_id} )
            }
        }
    }


    Column {

        id: headerContainer
        width: parent.width

        PageHeader {
            id: pageHeader
            title: "Season details"
        }

        SeasonInfoItem {
            id: seasonInfo
            width: parent.width
            height: col.height            

            Column {

                id: col
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right

                ClickableLabel {
                    width: parent.width

                    text: seasonInfo.season + " season"
                    font.underline: true
                    font.pixelSize: Theme.fontSizeLarge
                    horizontalAlignment: Text.AlignHCenter

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: seasonInfo.url } )
                    }
                }

                Label {
                    text: "Races: " + seasonInfo.rounds
                    width: parent.width
                }

                Row {
                    width: parent.width
                    height: chCol.height

                    Label {
                        text: (seasonInfo.season > 1957) ? "Champions:" : "Champion:"
                        width: parent.width * 0.35
                        height: chCol.height
                        verticalAlignment: Text.AlignVCenter
                    }
                    Column {
                        id: chCol
                        width: parent.width * 0.65
                        Label {
                            text: (seasonInfo.season > 1957) ? seasonInfo.driverChampion + "," : seasonInfo.driverChampion
                            width: parent.width
                            wrapMode: Text.WordWrap
                        }
                        Label {
                            visible: (seasonInfo.season > 1957)
                            text: seasonInfo.constructorChampion
                            width: parent.width
                        }

                    }

                }

                Separator {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: Theme.primaryColor
                }
            }
        }

        ComboBox {
            id: comboBox
            width: page.width
            label: ""

            menu: ContextMenu {                
                MenuItem {
                    text: "Drivers standings"
                    onClicked: {
                        loader.sourceComponent = driversList
                    }
                }
                MenuItem {
                    visible: (page.season > 1957)
                    text: "Constructor standings"
                    onClicked: {
                        loader.sourceComponent = constructorsList
                    }
                }
                MenuItem {
                    text: "Calendar"
                    onClicked: {
                        loader.sourceComponent = calList
                    }
                }
            }
        }
    }

    Loader {
        id: loader
        opacity: 0
        anchors.fill: parent
        sourceComponent: driversList
    }

    Component {
        id: calList

        SilicaListView {

            clip: true

            header: Item {
                id: calHeaderItem
                width: headerContainer.width
                height: headerContainer.height
                Component.onCompleted: headerContainer.parent = calHeaderItem
            }

            anchors.fill: parent

            delegate: raceScheduleDelegate
            model: raceScheduleModel

            VerticalScrollDecorator { }
        }
    }

    Component {
        id: driversList
        SilicaListView {

            clip: true

            header: Column {
                width: headerContainer.width
                Item {
                    id: driversHeaderItem
                    width: headerContainer.width
                    height: headerContainer.height
                    Component.onCompleted: headerContainer.parent = driversHeaderItem
                }
                DriverStandingsHeader {
                    width: parent.width
                    height: 40
                }
            }

            anchors.fill: parent

            delegate: driversStandingsDelegate
            model: driverStandingsModel

            VerticalScrollDecorator { }
        }
    }

    Component {
        id: constructorsList
        SilicaListView {

            clip: true

            header: Column {
                width: headerContainer.width
                Item {
                    id: constructorsHeaderItem
                    width: headerContainer.width
                    height: headerContainer.height
                    Component.onCompleted: headerContainer.parent = constructorsHeaderItem
                }
                DriverStandingsHeader {
                    width: parent.width
                    height: 40
                }
            }

            anchors.fill: parent

            delegate: constructorStandingsDelegate
            model: constructorStandingsModel

            VerticalScrollDecorator { }
        }
    }


    Connections {
        target: dataManager
        onDriverStandingsObtained: {
            driverStandingsModel.update(sd);
            seasonInfo.updateData(sd);
            loader.opacity = 1
        }
        onConstructorStandingsObtained: {
            constructorStandingsModel.update(sd);
            seasonInfo.updateData(sd);
        }
        onRaceScheduleObtained: {
            raceScheduleModel.update(sd);
            seasonInfo.updateData(sd);
        }

        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    Component.onCompleted: {
        dataManager.getRaceSchedule(page.season);
        dataManager.getDriverStandings(page.season);
        dataManager.getConstructorStandings(page.season);
    }

    SilicaFlickable {
        anchors.fill: parent
        enabled: loader.opacity === 0

        ViewPlaceholder {
            id: viewPlaceHolder

            enabled: loader.opacity === 0
            text: "Loading..."

            BusyIndicator {
                horizontalAlignment: Image.AlignHCenter
                running: loader.opacity === 0
            }
        }
    }
}
