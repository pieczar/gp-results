/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import QtWebKit 3.0
import Sailfish.Silica 1.0
import "../items"

Page {
    id: webViewPage    

    property string url;

    allowedOrientations: Orientation.Landscape | Orientation.Portrait

    NotificationArea {
        id: notification
        anchors.top: webViewPage.top
    }

    //workaround for Sailfish bug
    onForwardNavigationChanged: {
        if (forwardNavigation)
            forwardNavigation = false;
    }

    SilicaWebView {
        id: webView
        url: webViewPage.url

        //force usage of wiki mobile version
        Component.onCompleted: {
            var str = webView.url.toString();

            if (str.indexOf("m.wikipedia") == -1)
                webView.url = str.replace("wikipedia", "m.wikipedia");
        }

        anchors.fill: parent

        header: PageHeader {
            title: webView.title
        }

        PullDownMenu {
            MenuItem {
                text: "Close"
                onClicked: pageStack.pop()
            }
            MenuItem {
                text: "Reload"
                onClicked: webView.reload()
            }
            MenuItem {
                text: "Back"
                onClicked: webView.goBack()
                enabled: webView.canGoBack
            }
        }

        opacity: 0
        onLoadingChanged: {
            switch (loadRequest.status)
            {
            case WebView.LoadSucceededStatus:
                opacity = 1
                break
            case WebView.LoadFailedStatus:
                opacity = 0
                viewPlaceHolder.errorString = loadRequest.errorString
                notification.show("Server or connection error")
                break
            default:
                opacity = 0
                break
            }
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        visible: webView.opacity === 0

        ViewPlaceholder {
            id: viewPlaceHolder
            property string errorString

            enabled: webView.opacity === 0
            text: webView.loading ? "Loading page..." : "Web content load error: " + errorString
            hintText: webView.loading ? "" : "Check network connectivity and pull down to reload"

            BusyIndicator {
                horizontalAlignment: Image.AlignHCenter
                running: webView.loading
            }
        }
    }
}
