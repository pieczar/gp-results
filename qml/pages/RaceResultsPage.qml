/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property int season: 2014;
    property bool latestResults: false
    property int round: 1;
    property string race;

    NotificationArea {
        id: notification
        anchors.top: page.top
    }

    property string activeView: "rList"

    Loader {        
        id: loader
        opacity: 0
        anchors.fill: parent
        sourceComponent: activeView == "rList" ? rrComponent : qrComponent
    }


    Component {
        id: raceResultsDelegate
        RaceResultsDelegate {
            width: page.width

            onRaceResultClicked: {

                pageStack.push(Qt.resolvedUrl("DriverRaceDataPage.qml"), {season: eventInfoItem.season, round: eventInfoItem.round, driverId: driver_id} )
            }
        }
    }

    Component {
        id: qualiResultsDelegate
        QualiResultsDelegate {
            width: page.width

            onQualiResultClicked: {

                pageStack.push(Qt.resolvedUrl("DriverRaceDataPage.qml"), {season: eventInfoItem.season, round: eventInfoItem.round, driverId: driver_id} )
            }
        }
    }

    Column {
        id: headerContainer
        width: page.width

        PageHeader {
            id: pageHeader
            title: "Round " + eventInfoItem.round;
        }

        EventInfoItem {
            id: eventInfoItem
            height: col.height
            width: page.width

            Column {
                id: col
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 5

                ClickableLabel {
                    width: parent.width
                    id: button
                    text: eventInfoItem.season + " " + eventInfoItem.raceName
                    font.pixelSize: Theme.fontSizeLarge
                    font.underline: true
                    wrapMode: Text.WordWrap

                    anchors.horizontalCenter: parent.horizontalCenter
                    horizontalAlignment: Text.AlignHCenter

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: eventInfoItem.infoURL } )
                    }

                }
                ClickableLabel {
                    id: nameLabel
                    text: eventInfoItem.circuit
                    font.pixelSize: Theme.fontSizeMedium                    

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: eventInfoItem.circuitURL } )
                    }
                }

                Row {
                    id: row
                    width: parent.width

                    Label {
                        id: countryLabel
                        width: row.width * 0.7
                        text: eventInfoItem.location + ", " + eventInfoItem.country
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.secondaryColor
                        verticalAlignment: Text.AlignVCenter
                    }
                    Label {
                        id: dateLabel
                        text: eventInfoItem.raceDate
                        width: row.width * 0.3
                        font.pixelSize: Theme.fontSizeSmall
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                    }
                }


                Separator {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: Theme.primaryColor
                }



                Label {                    
                    id: flLabel

                    //fastest laps available from 2004 season onwards
                    visible: (page.season > 2003) && eventInfoItem.raceCompleted
                    text: "Fastest lap: " + eventInfoItem.fastestLap + ", " + eventInfoItem.fastestLapDriver + " (L" + eventInfoItem.fastestLapNumber + ")"
                    width: parent.width
                    font.pixelSize: Theme.fontSizeExtraSmall

                }
            }

            Connections {
                target: dataManager
                onRaceResultsObtained: {
                    eventInfoItem.updateData(sd, round);
                }
            }

        }

        ComboBox {
            id: comboBox
            width: page.width
            label: "Results:"

            //quali results available from 2003 season onwards
            visible: eventInfoItem.qualiResultsPresent && eventInfoItem.raceCompleted

            menu: ContextMenu {
                MenuItem {
                    text: "Race"
                    onClicked: {
                        activeView = "rList"
                    }
                }
                MenuItem {
                    text: "Qualifying"
                    onClicked: {
                        activeView = "qList"
                    }
                }
            }
        }
    }


    Component {
        id: rrComponent

        SilicaListView {
            id: rrView
            clip: true

            header: Item {
                id: header
                width: headerContainer.width
                height: headerContainer.height
                Component.onCompleted: headerContainer.parent = header
            }

            anchors.fill: parent

            delegate: raceResultsDelegate
            model: raceResultsModel

            VerticalScrollDecorator { }
        }
    }

    Component {
        id: qrComponent
        SilicaListView {
            id: qrView
            clip: true

            header: Item {
                id: qHeader
                width: headerContainer.width
                height: headerContainer.height
                Component.onCompleted: headerContainer.parent = qHeader
            }

            anchors.fill: parent

            delegate: qualiResultsDelegate
            model: qualiResultsModel

            VerticalScrollDecorator { }
        }
    }

    Component.onCompleted: {

        if (latestResults)
        {
            dataManager.getLatestRaceResults();
            dataManager.getLatestQualiResults();
        }
        else
        {
            dataManager.getRaceResults(page.season, page.round);
            dataManager.getQualiResults(page.season, page.round);
        }
    }

    Connections {
        target: dataManager
        onQualiResultsObtained: {
            qualiResultsModel.update(sd, round);
        }
        onRaceResultsObtained: {
            loader.opacity = 1
            raceResultsModel.update(sd, round);            
        }

        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        enabled: loader.opacity === 0

        ViewPlaceholder {
            id: viewPlaceHolder

            enabled: loader.opacity === 0
            text: "Loading..."

            BusyIndicator {
                horizontalAlignment: Image.AlignHCenter
                running: loader.opacity === 0
            }
        }
    }
}
