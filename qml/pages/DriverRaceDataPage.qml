/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0
import "../delegates"
import "../items"

Page {

    id: page
    property string driverId;
    property int season;
    property int round;

    NotificationArea {
        id: notification
        anchors.top: page.top
    }

    Column {
        id: headerContainer
        width: page.width

        PageHeader {
            id: pageHeader
            title: "Race details"
        }

        DriverRaceInfoItem {
            id: driverRaceInfo
            width: page.width
            height: column.height

            Column {

                id: column
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 5

                ClickableLabel {
                    text: driverRaceInfo.position + ". "+ driverRaceInfo.name
                    font.underline: true
                    font.pixelSize: Theme.fontSizeLarge

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: driverRaceInfo.url} )
                    }

                    anchors.horizontalCenter: parent.horizontalCenter
                }


                Row {
                    width: parent.width

                    Label {
                        width: parent.width * 0.3
                        text: "Grid: " + driverRaceInfo.grid
                        font.pixelSize: Theme.fontSizeSmall
                    }
                    Label {
                        visible: (page.season > 2010)
                        width: parent.width * 0.7
                        text: "Best lap: " + driverRaceInfo.fastestLap + " (L" + driverRaceInfo.fastestLapNumber +  ")"
                        font.pixelSize: Theme.fontSizeSmall
                        horizontalAlignment: Text.AlignRight
                    }
                }
                Row {
                    width: parent.width

                    Label {
                        width: parent.width * 0.5
                        text: "Status: " + driverRaceInfo.status
                        font.pixelSize: Theme.fontSizeSmall
                    }
                    Label {
                        visible: (page.season > 2010)
                        width: parent.width * 0.5
                        text: "Avg speed: " + driverRaceInfo.avgSpeed
                        font.pixelSize: Theme.fontSizeSmall
                        horizontalAlignment: Text.AlignRight
                    }                    
                }
                Separator {
                    visible: (page.season > 2010)
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: Theme.primaryColor
                }
                Row {
                    id: row
                    width: parent.width-40
                    visible: (page.season > 2010)
                    spacing: 10

                    Label {
                        text: "Lap"
                        width: row.width * 0.1
                        font.pixelSize: Theme.fontSizeExtraSmall
                        horizontalAlignment: Text.AlignRight
                        color: Theme.secondaryColor
                    }
                    Label {
                        text: "P"
                        width: row.width * 0.1
                        font.pixelSize: Theme.fontSizeExtraSmall
                        horizontalAlignment: Text.AlignRight
                        color: Theme.secondaryColor
                    }
                    Label {
                        text: "Time"
                        width: row.width * 0.5
                        font.pixelSize: Theme.fontSizeExtraSmall
                        horizontalAlignment: Text.AlignHCenter
                        color: Theme.secondaryColor
                    }
                    Label {
                        text: "Diff"
                        width: row.width * 0.3
                        font.pixelSize: Theme.fontSizeExtraSmall
                        horizontalAlignment: Text.AlignRight
                        color: Theme.secondaryColor
                    }
                }
            }
        }
    }

    Component {
        id: lapTimesDelegate
        LapTimesDelegate {
            width: page.width
        }
    }

    SilicaListView {
        id: ltView
        clip: true
        opacity: 0

        header: Item {
            id: header
            width: headerContainer.width
            height: headerContainer.height
            Component.onCompleted: headerContainer.parent = header
        }

        anchors.fill: parent

        delegate: lapTimesDelegate
        model: lapTimesModel

        VerticalScrollDecorator { }
    }

    Connections {
        target: dataManager
        onLapTimesObtained: {
            driverRaceInfo.updateData(ed, driverId)
            lapTimesModel.update(ed, driverId)            
            ltView.opacity = 1
        }

        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    Component.onCompleted: {
        dataManager.getLapTimes(page.driverId, page.season, page.round)
    }

    SilicaFlickable {
        anchors.fill: parent
        visible: ltView.opacity === 0

        ViewPlaceholder {
            id: viewPlaceHolder

            enabled: ltView.opacity === 0
            text: "Loading..."

            BusyIndicator {
                horizontalAlignment: Image.AlignHCenter
                running: ltView.opacity === 0
            }
        }
    }
}
