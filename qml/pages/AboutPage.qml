/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    Column {
        width: parent.width
        anchors.centerIn: parent
        spacing: 20

        Image {
            source: "qrc:/icons/icon.png"
            fillMode: Image.PreserveAspectFit
            width: parent.width / 3

            anchors.horizontalCenter: parent.horizontalCenter

            horizontalAlignment: Image.AlignHCenter
        }
        Rectangle {
            width: parent.width
            height: 50
            color: "#00000000"
        }

        Label {
            width: parent.width
            text: "GP Results " + dataManager.appVersion();
            font.bold: true
            font.pixelSize: Theme.fontSizeExtraLarge
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            width: parent.width
            text: "by Mariusz Pilarek"
            font.pixelSize: Theme.fontSizeLarge
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            width: parent.width
            font.italic: true
            text: "pieczaro@gmail.com"
            horizontalAlignment: Text.AlignHCenter

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("mailto:pieczaro@gmail.com")
            }
        }

        Label {
            width: parent.width
            text: "Data source: http://ergast.com/mrd/"
            horizontalAlignment: Text.AlignHCenter
            color: Theme.secondaryColor
            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("http://ergast.com/mrd/")
            }
        }

        Label {
            width: parent.width
            text: "Weather data: http://openweathermap.org"
            horizontalAlignment: Text.AlignHCenter
            color: Theme.secondaryColor
            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("http://openweathermap.org")
            }
        }

        Button {
            text: "License"
            anchors.horizontalCenter: parent.horizontalCenter

            onClicked: {
                pageStack.push(Qt.resolvedUrl("LicensePage.qml"));
            }
        }
    }
}
