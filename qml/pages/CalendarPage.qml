/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property int season: 2014;

    NotificationArea {
        id: notification
        anchors.top: page.top
    }


    Component {
        id: raceScheduleDelegate
        RaceScheduleDelegate {
            width: page.width

            onRaceSelected: {

                pageStack.push(Qt.resolvedUrl("RaceResultsPage.qml"), { season: page.season, round: round });
            }
        }
    }


    SilicaFlickable {
        anchors.fill: parent
        contentHeight: page.height

        PullDownMenu {
            MenuItem {
                text: "History";

                onClicked: {
                    pageStack.replace(Qt.resolvedUrl("SeasonsHistoryPage.qml"));
                }
            }

            MenuItem {
                text: "Standings";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("StandingsPage.qml"), { season: d.getFullYear() });
                }
            }

            MenuItem {
                text: "Next race";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("RaceCountdownPage.qml"));
                }
            }
        }

        SilicaListView {

            id: listView
            opacity: 0

            anchors.fill: parent
            clip: true

            header: PageHeader {
                id: pageHeader
                title: page.season + " Calendar";
            }

            delegate: raceScheduleDelegate
            model: raceScheduleModel

            VerticalScrollDecorator { }

            Connections {
                target: dataManager
                onRaceScheduleObtained: {
                    raceScheduleModel.update(sd);
                    listView.opacity = 1
                }
                onConnectionError: {
                    notification.show("Server or connection error")
                }
            }
        }

        ViewPlaceholder {
            id: viewPlaceHolder

            enabled: listView.opacity === 0
            text: "Loading..."

            BusyIndicator {
                horizontalAlignment: Image.AlignHCenter
                running: listView.opacity === 0
            }
        }
    }

    Component.onCompleted: {
        dataManager.getRaceSchedule(page.season);
    }
}
