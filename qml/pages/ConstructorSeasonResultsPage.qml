/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.gpresults.GPComponents 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property string constructorId;
    property int season;

    NotificationArea {
        id: notification
        anchors.top: page.top
    }

    Component {
        id: constructorSeasonResultsDelegate
        ConstructorSeasonResultsDelegate {
            width: page.width

            onRoundSelected: {
                pageStack.push(Qt.resolvedUrl("RaceResultsPage.qml"), { season: page.season, round: round });
            }
        }
    }

    Column {
        id: headerContainer
        width: page.width

        PageHeader {
            id: pageHeader
            title: page.season + " season results"
        }

        ConstructorSeasonInfoItem {
            id: constructorSeasonInfo;
            width: page.width
            height: column.height

            Column {

                id: column
                anchors.margins: 5
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: 5

                ClickableLabel {
                    text: constructorSeasonInfo.position + ". "+ constructorSeasonInfo.name
                    font.underline: true
                    font.pixelSize: Theme.fontSizeLarge

                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("WebViewPage.qml"), {url: constructorSeasonInfo.url} )
                    }

                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Row {
                    width: parent.width

                    Label {
                        text: "Points: " + constructorSeasonInfo.points
                        width: parent.width * 0.5
                        font.pixelSize: Theme.fontSizeMedium
                    }

                    Label {
                        text: "Wins: " + constructorSeasonInfo.wins
                        width: parent.width * 0.5
                        font.pixelSize: Theme.fontSizeMedium
                    }
                }

                Row {
                    width: parent.width

                    Label {
                        text: "Retirements: " + constructorSeasonInfo.retirements
                        width: parent.width * 0.5
                        font.pixelSize: Theme.fontSizeMedium
                    }

                    Label {
                        text: "Podiums: " + constructorSeasonInfo.podiums
                        width: parent.width * 0.5
                        font.pixelSize: Theme.fontSizeMedium
                    }
                }

                Separator {
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    color: Theme.primaryColor
                }
            }
        }
    }

    SilicaListView {
        clip: true
        id: listView
        opacity: 0

        header: Item {
            id: header
            width: headerContainer.width
            height: headerContainer.height
            Component.onCompleted: headerContainer.parent = header
        }

        anchors.fill: parent

        delegate: constructorSeasonResultsDelegate
        model: constructorSeasonResultsModel

        VerticalScrollDecorator { }
    }

    Connections {
        target: dataManager
        onConstructorResultsObtained: {
            constructorSeasonResultsModel.update(sd, constructorId)
            constructorSeasonInfo.updateData(sd, constructorId)
            listView.opacity = 1
        }
        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    Component.onCompleted: {
        dataManager.getConstructorResults(page.constructorId, page.season)
    }

    SilicaFlickable {
        anchors.fill: parent
        visible: listView.opacity === 0

        ViewPlaceholder {
            id: viewPlaceHolder

            enabled: listView.opacity === 0
            text: "Loading..."

            BusyIndicator {
                horizontalAlignment: Image.AlignHCenter
                running: listView.opacity === 0
            }
        }
    }
}
