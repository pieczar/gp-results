/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    Column {
        id: headerContainer
        width: page.width

        PageHeader {
            id: pageHeader
            title: "History"
        }

    }

    SilicaGridView {
        id: gridView
        clip: true

        PullDownMenu {
            MenuItem {
                text: "Standings";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("StandingsPage.qml"), { season: d.getFullYear() });
                }
            }

            MenuItem {
                text: "Calendar";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("CalendarPage.qml"), { season: d.getFullYear() });
                }
            }

            MenuItem {
                text: "Next race";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("RaceCountdownPage.qml"));
                }
            }
        }

        header: Item {
          id: theader
          width: headerContainer.width
          height: headerContainer.height
          Component.onCompleted: headerContainer.parent = theader
        }

        anchors.fill: parent

        cellWidth: gridView.width / 4
        cellHeight: cellWidth / 2

        delegate: BackgroundItem {
            id: rectangle
            width: gridView.cellWidth
            height: gridView.cellHeight

            Label {
                text: season
                width: parent.width
                height: parent.height
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: Theme.fontSizeLarge
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("SeasonPage.qml"), { season: season });
            }
        }
        model: listModel

        VerticalScrollDecorator { }
    }

    ListModel {
        id: listModel

        function update()
        {
            var d = new Date();
            var size = d.getFullYear() - 1 - 1950;

            for (var i = d.getFullYear() - 1; i >= 1950; --i)
                append({"season": i});
        }
    }

    Component.onCompleted: listModel.update()
}
