/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../delegates"
import "../items"

Page {
    id: page
    property string cityId
    property string city

    NotificationArea {
        id: notification
        anchors.top: page.top
    }

    Component {
        id: weatherDelegate
        WeatherForecastDelegate {
            width: page.width - 10
        }
    }

    SilicaListView {
        id: listView
        header: Column {
            width: page.width
            PageHeader {
                title: "Weather forecast"
            }
            Button {
                id: button
                anchors.horizontalCenter: parent.horizontalCenter

                text: "Full weather report for " + page.city
                onClicked: {
                    Qt.openUrlExternally("http://openweathermap.org/city/" + page.cityId)
                }
            }
        }

        anchors.fill: parent
        anchors.leftMargin: 5
        anchors.rightMargin: 5

        delegate: weatherDelegate
        model: weatherForecastModel
        clip: true

        opacity: 0
    }

    ViewPlaceholder {
        id: viewPlaceHolder

        enabled: listView.opacity === 0
        text: "Loading..."

        BusyIndicator {
            horizontalAlignment: Image.AlignHCenter
            running: listView.opacity === 0
        }
    }

    Component.onCompleted: {
        dataManager.getWeatherForecast();
    }

    Connections  {
        target: dataManager
        onWeatherForecastObtained: {
            weatherForecastModel.update(wd);
            page.cityId = cityId
            page.city = city
            listView.opacity = 1;
        }

        onConnectionError: {
            notification.show("Server or connection error")
        }
    }
}
