/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../delegates"
import "../items"

Page {

    id: page
    property int season;

    NotificationArea {
        id: notification
        anchors.top: page.top
    }

    Column {
        id: headerContainer
        width: page.width

        PageHeader {
            id: pageHeader
            title: "" + season + " standings"
        }

        ComboBox {
            id: comboBox
            width: page.width
            label: "Standings:"

            menu: ContextMenu {

                MenuItem {
                    text: "Drivers"
                    onClicked: {
                        loader.sourceComponent = driversList
                    }
                }
                MenuItem {
                    text: "Constructors"
                    onClicked: {
                        loader.sourceComponent = teamsList
                    }
                }
            }
        }

        DriverStandingsHeader {
            width: parent.width
            height: 40
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: page.height

        PullDownMenu {

            id: menu
            MenuItem {
                text: "History";

                onClicked: {
                    pageStack.replace(Qt.resolvedUrl("SeasonsHistoryPage.qml"));
                }
            }

            MenuItem {
                text: "Calendar";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("CalendarPage.qml"), { season: d.getFullYear() });
                }
            }

            MenuItem {
                text: "Next race";

                onClicked: {
                    var d = new Date();
                    pageStack.replace(Qt.resolvedUrl("RaceCountdownPage.qml"));
                }
            }
        }

        Loader {
            id: loader
            opacity: 0
            anchors.fill: parent
            sourceComponent: driversList
        }

        ViewPlaceholder {
            id: viewPlaceHolder
            enabled: loader.opacity == 0

            text: "Loading..."

            BusyIndicator {
                horizontalAlignment: Image.AlignHCenter
                running: loader.opacity == 0
            }
        }
    }

    Component {
        id: driversStandingsDelegate
        DriverStandingsDelegate {
            width: page.width

            onDriverClicked: {
                pageStack.push(Qt.resolvedUrl("DriverSeasonResultsPage.qml"), {season: page.season, driverId: driver_id} )
            }
        }
    }

    Component {
        id: constructorStandingsDelegate
        ConstructorStandingsDelegate {
            width: page.width

            onConstructorClicked: {
                pageStack.push(Qt.resolvedUrl("ConstructorSeasonResultsPage.qml"), {season: page.season, constructorId: constructor_id} )
            }
        }
    }


    Component {
        id: driversList

            SilicaListView {

                id: dList
                clip: true

                header: Item {
                  id: header
                  width: headerContainer.width
                  height: headerContainer.height                 
                  Component.onCompleted: headerContainer.parent = header
                }

                anchors.fill: parent

                delegate: driversStandingsDelegate                               
                model: driverStandingsModel

                VerticalScrollDecorator { }
            }
    }

    Component {
        id: teamsList

            SilicaListView {
                id: tList
                clip: true

                header: Item {
                  id: theader
                  width: headerContainer.width
                  height: headerContainer.height
                  Component.onCompleted: headerContainer.parent = theader
                }

                anchors.fill: parent

                delegate: constructorStandingsDelegate
                model: constructorStandingsModel

                VerticalScrollDecorator { }
            }
    }

    Component {
        id: noStandingsView

        SilicaFlickable {
            id: standFlick
            anchors.fill: parent

            Label {
                anchors.fill: parent

                anchors.centerIn: parent.Center
                text: page.season + " standings not available yet"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                font.pixelSize: Theme.fontSizeLarge
                wrapMode: Text.WordWrap
            }
        }
    }

    Connections {
        target: dataManager
        onDriverStandingsObtained: {
            driverStandingsModel.update(sd);
            loader.opacity = 1
        }
        onConstructorStandingsObtained: {
            constructorStandingsModel.update(sd);
            loader.opacity = 1
        }

        onDriverStandingsNotAvailable: {
            loader.sourceComponent = noStandingsView
            loader.opacity = 1
        }
        onConstructorStandingsNotAvailable: {
            loader.sourceComponent = noStandingsView
            loader.opacity = 1
        }
        onConnectionError: {
            notification.show("Server or connection error")
        }
    }

    Component.onCompleted: {
        dataManager.getDriverStandings(page.season)
        dataManager.getConstructorStandings(page.season)
    }    
}
