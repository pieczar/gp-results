# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-gpresults

CONFIG += sailfishapp

SOURCES += src/GP-Results.cpp \
    src/core/teamseasondata.cpp \
    src/core/teamdata.cpp \
    src/core/seasondata.cpp \
    src/core/lapdata.cpp \
    src/core/jsondataparser.cpp \
    src/core/eventdata.cpp \
    src/core/driverseasondata.cpp \
    src/core/driverdata.cpp \
    src/core/datamanager.cpp \
    src/core/connectionapimanager.cpp \
    src/core/circuitmanager.cpp \
    src/core/circuitdata.cpp \
    src/items/nextraceitem.cpp \
    src/models/raceschedulemodel.cpp \
    src/models/raceresultsmodel.cpp \
    src/models/qualiresultsmodel.cpp \
    src/models/laptimesmodel.cpp \
    src/models/driverstandingsmodel.cpp \
    src/models/driverresultsmodel.cpp \
    src/models/constructorstandingsmodel.cpp \
    src/models/constructorresultsmodel.cpp \
    src/items/eventinfoitem.cpp \
    src/items/circuitinfoitem.cpp \
    src/items/driverraceinfoitem.cpp \
    src/items/driverseasoninfoitem.cpp \
    src/items/constructorseasoninfoitem.cpp \
    src/models/constructorraceresultsmodel.cpp \
    src/items/seasoninfoitem.cpp \
    src/core/weatherforecastdata.cpp \
    src/models/weatherforecastmodel.cpp

OTHER_FILES += \
    qml/cover/CoverPage.qml \
    rpm/GP-Results.spec \
    rpm/GP-Results.yaml \
    qml/delegates/RaceScheduleDelegate.qml \
    qml/delegates/RaceResultsDelegate.qml \
    qml/delegates/QualiResultsDelegate.qml \
    qml/delegates/LapTimesDelegate.qml \
    qml/delegates/DriverStandingsHeader.qml \
    qml/delegates/DriverStandingsDelegate.qml \
    qml/delegates/DriverSeasonResultsDelegate.qml \
    qml/delegates/ConstructorStandingsHeader.qml \
    qml/delegates/ConstructorStandingsDelegate.qml \
    qml/delegates/ConstructorSeasonResultsDelegate.qml \
    qml/pages/RaceCountdownPage.qml \
    qml/pages/CalendarPage.qml \
    qml/pages/RaceResultsPage.qml \
    qml/pages/WebViewPage.qml \
    qml/pages/DriverRaceDataPage.qml \
    qml/pages/StandingsPage.qml \
    qml/pages/DriverSeasonResultsPage.qml \
    qml/pages/ConstructorSeasonResultsPage.qml \
    qml/items/ClickableLabel.qml \
    qml/pages/SeasonsHistoryPage.qml \
    qml/pages/SeasonPage.qml \
    qml/pages/AboutPage.qml \
    qml/pages/LicensePage.qml \
    qml/harbour-gpresults.qml \
    harbour-gpresults.desktop \
    qml/delegates/WeatherForecastDelegate.qml \
    qml/pages/WeatherForecastPage.qml \
    qml/items/NotificationArea.qml

HEADERS += \
    src/core/teamseasondata.h \
    src/core/teamdata.h \
    src/core/seasondata.h \
    src/core/lapdata.h \
    src/core/jsondataparser.h \
    src/core/eventdata.h \
    src/core/driverseasondata.h \
    src/core/driverdata.h \
    src/core/datamanager.h \
    src/core/connectionapimanager.h \
    src/core/circuitmanager.h \
    src/core/circuitdata.h \
    src/items/nextraceitem.h \
    src/models/raceschedulemodel.h \
    src/models/raceresultsmodel.h \
    src/models/qualiresultsmodel.h \
    src/models/laptimesmodel.h \
    src/models/driverstandingsmodel.h \
    src/models/driverresultsmodel.h \
    src/models/constructorstandingsmodel.h \
    src/models/constructorresultsmodel.h \
    src/items/eventinfoitem.h \
    src/items/circuitinfoitem.h \
    src/items/driverraceinfoitem.h \
    src/items/driverseasoninfoitem.h \
    src/items/constructorseasoninfoitem.h \
    src/models/constructorraceresultsmodel.h \
    src/items/seasoninfoitem.h \
    src/core/weatherforecastdata.h \
    src/models/weatherforecastmodel.h


OBJECTS_DIR = obj/
MOC_DIR = moc/

RESOURCES += \
    icons/icons.qrc
