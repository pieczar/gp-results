/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERSEASONINFOITEM_H
#define DRIVERSEASONINFOITEM_H

#include <QQuickItem>
#include "../core/driverseasondata.h"
#include "../core/seasondata.h"

class DriverSeasonInfoItem : public QQuickItem
{
    Q_OBJECT
public:
    explicit DriverSeasonInfoItem(QQuickItem *parent = 0);

    Q_PROPERTY (QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY (QString wins READ getWins NOTIFY winsChanged)
    Q_PROPERTY (QString position READ getPosition NOTIFY positionChanged)
    Q_PROPERTY (QString points READ getPoints NOTIFY pointsChanged)
    Q_PROPERTY (QString url READ getUrl NOTIFY urlChanged)
    Q_PROPERTY (QString avgPosition READ getAvgPosition NOTIFY avgPositionChanged)
    Q_PROPERTY (QString avgGrid READ getAvgGrid NOTIFY avgGridChanged)
    Q_PROPERTY (QString retirements READ getRetirements NOTIFY retirementsChanged)
    Q_PROPERTY (QString podiums READ getPodiums NOTIFY podiumsChanged)

    QString getName() const;
    QString getWins() const;
    QString getPoints() const;
    QString getUrl() const;
    QString getPosition() const;
    QString getAvgPosition() const;
    QString getAvgGrid() const;
    QString getRetirements() const;
    QString getPodiums() const;

signals:

    void nameChanged();
    void winsChanged();
    void pointsChanged();
    void urlChanged();
    void positionChanged();
    void avgPositionChanged();
    void avgGridChanged();
    void retirementsChanged();
    void podiumsChanged();

public slots:

    void updateData(const SeasonData *sm, QString driverId);

private:
    const DriverSeasonData *seasonData;

};

#endif // DRIVERSEASONINFOITEM_H
