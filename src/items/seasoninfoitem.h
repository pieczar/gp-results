/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef SEASONINFOITEM_H
#define SEASONINFOITEM_H

#include <QQuickItem>
#include "../core/seasondata.h"

class SeasonInfoItem : public QQuickItem
{
    Q_OBJECT
public:
    explicit SeasonInfoItem(QQuickItem *parent = 0);

    Q_PROPERTY (int season READ getSeason NOTIFY seasonChanged)
    Q_PROPERTY (int rounds READ getRounds NOTIFY roundsChanged)
    Q_PROPERTY (QString url READ getUrl NOTIFY urlChanged)
    Q_PROPERTY (QString driverChampion READ getDriverChampion NOTIFY driverChampionChanged)
    Q_PROPERTY (QString constructorChampion READ getConstructorChampion NOTIFY constructorChampionChanged)

    int getSeason() const;
    int getRounds() const;
    QString getUrl() const;
    QString getDriverChampion() const;
    QString getConstructorChampion() const;

signals:

    void seasonChanged();
    void roundsChanged();
    void urlChanged();
    void driverChampionChanged();
    void constructorChampionChanged();

public slots:

    void updateData(const SeasonData *sd);

private:

    const SeasonData *seasonData;

};

#endif // SEASONINFOITEM_H
