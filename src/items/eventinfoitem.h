/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef EVENTINFOITEM_H
#define EVENTINFOITEM_H

#include <QQuickItem>
#include "../core/seasondata.h"

class EventInfoItem : public QQuickItem
{
    Q_OBJECT
public:
    explicit EventInfoItem(QQuickItem *parent = 0);

    Q_PROPERTY (QString season READ getSeason NOTIFY seasonChanged)
    Q_PROPERTY (QString round READ getRound NOTIFY roundChanged)
    Q_PROPERTY (QString raceName READ getRaceName NOTIFY raceNameChanged)
    Q_PROPERTY (QString infoURL READ getInfoUrl NOTIFY infoURLChanged)
    Q_PROPERTY (QString raceDate READ getRaceDate NOTIFY raceDateChanged)
    Q_PROPERTY (QString circuitID READ getCircuitID NOTIFY circuitIDChanged)
    Q_PROPERTY (QString circuit READ getCircuit NOTIFY circuitChanged)
    Q_PROPERTY (QString fastestLap READ getFastestLap NOTIFY fastestLapChanged)
    Q_PROPERTY (QString fastestLapDriver READ getFastestLapDriver NOTIFY fastestLapDriverChanged)
    Q_PROPERTY (QString fastestLapNumber READ getFastestLapNumber NOTIFY fastestLapNumberChanged)
    Q_PROPERTY (QString location READ getLocation NOTIFY locationChanged)
    Q_PROPERTY (QString country READ getCountry NOTIFY countryChanged)
    Q_PROPERTY (QString circuitURL READ getCircuitURL NOTIFY circuitURLChanged)
    Q_PROPERTY (bool raceCompleted READ getRaceCompleted NOTIFY raceCompletedChanged)
    Q_PROPERTY (bool qualiResultsPresent READ getQualiResultsPresent NOTIFY qualiResultsPresentChanged)

    QString getSeason() const;
    QString getRound() const;
    QString getRaceName() const;
    QString getInfoUrl() const;
    QString getRaceDate() const;
    QString getCircuitID() const;
    QString getCircuit() const;

    QString getLocation() const;
    QString getCountry() const;
    QString getCircuitURL() const;

    QString getFastestLap() const
    {
        return flTime;
    }
    QString getFastestLapDriver() const
    {
        return flDriver;
    }
    QString getFastestLapNumber() const
    {
        return flNumber;
    }       

    bool getRaceCompleted() const;
    bool getQualiResultsPresent() const;

signals:
    void seasonChanged();
    void roundChanged();
    void raceNameChanged();
    void infoURLChanged();
    void raceDateChanged();
    void circuitIDChanged();
    void circuitChanged();
    void fastestLapChanged();
    void fastestLapDriverChanged();
    void fastestLapNumberChanged();

    void locationChanged();
    void countryChanged();
    void circuitURLChanged();
    void raceCompletedChanged();
    void qualiResultsPresentChanged();


public slots:

    void updateData(const SeasonData *sm, int round);

private:
    const SeasonData *seasonData;
    int round;

    QString flDriver;
    QString flTime;
    QString flNumber;

};

#endif // EVENTINFOITEM_H
