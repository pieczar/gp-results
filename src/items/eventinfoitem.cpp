/*******************************************************************************
 *                                                                             * 
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "eventinfoitem.h"
#include "../core/datamanager.h"

EventInfoItem::EventInfoItem(QQuickItem *parent) :
    QQuickItem(parent)
{
    seasonData = NULL;
}

QString EventInfoItem::getSeason() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
            return QString::number(ed->getSeason());
    }
    return QString();
}

QString EventInfoItem::getRound() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
            return QString::number(ed->getRound());
    }
    return QString();
}

QString EventInfoItem::getRaceName() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
            return ed->getRaceName();
    }
    return QString();
}

QString EventInfoItem::getInfoUrl() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
            return ed->getInfoURL();
    }
    return QString();
}

QString EventInfoItem::getRaceDate() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
        {
            //race time data available from 2005 season onwards
            if (ed->getSeason() > 2004)
                return ed->getRaceDate().toLocalTime().toString("hh:mm, dd MMM");

            return ed->getRaceDate().toLocalTime().toString("dd MMM");
        }
    }
    return QString();
}

QString EventInfoItem::getCircuitID() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
            return ed->getCircuitID();
    }
    return QString();
}

QString EventInfoItem::getCircuit() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
        {
            const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(ed->getCircuitID());

            if (cd != NULL)
                return cd->getName();
        }
    }
    return QString();
}

QString EventInfoItem::getLocation() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
        {
            const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(ed->getCircuitID());

            if (cd != NULL)
                return cd->getLocation().locality;
        }
    }
    return QString();
}

QString EventInfoItem::getCountry() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
        {
            const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(ed->getCircuitID());

            if (cd != NULL)
                return cd->getLocation().country;
        }
    }
    return QString();
}

QString EventInfoItem::getCircuitURL() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
        {
            const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(ed->getCircuitID());

            if (cd != NULL)
                return cd->getInfoURL();
        }
    }
    return QString();
}

bool EventInfoItem::getRaceCompleted() const
{
    QDateTime dt = QDateTime::currentDateTime().toLocalTime();

    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
        {
            QDateTime qualiTime = ed->getRaceDate().toLocalTime().addDays(-1);

            return qualiTime < dt;
        }
    }
    return false;
}

bool EventInfoItem::getQualiResultsPresent() const
{
    if (seasonData != NULL)
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL)
        {
            return ed->getQualiResultsObtained();
        }
    }
    return false;
}

void EventInfoItem::updateData(const SeasonData *sm, int round)
{
    if (sm != NULL && sm->getEvent(round) != NULL)
    {
        seasonData = sm;
        this->round = round;

        int lapNo = 0;
        sm->getEvent(round)->getFastestLap(flDriver, flTime, lapNo);

        if (lapNo != 0)
            flNumber = QString::number(lapNo);


        emit seasonChanged();
        emit roundChanged();
        emit raceNameChanged();
        emit infoURLChanged();
        emit raceDateChanged();
        emit circuitIDChanged();
        emit circuitChanged();
        emit fastestLapChanged();
        emit fastestLapDriverChanged();
        emit fastestLapNumberChanged();
        emit locationChanged();
        emit countryChanged();
        emit circuitURLChanged();
        emit raceCompletedChanged();
        emit qualiResultsPresentChanged();
    }
}
