/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "seasoninfoitem.h"
#include "../core/datamanager.h"

SeasonInfoItem::SeasonInfoItem(QQuickItem *parent) :
    QQuickItem(parent)
{
    seasonData = NULL;
}

int SeasonInfoItem::getSeason() const
{
    if (seasonData != NULL)
        return seasonData->getSeason();

    return 0;
}

int SeasonInfoItem::getRounds() const
{
    if (seasonData != NULL)
        return seasonData->getRounds();

    return 0;
}

QString SeasonInfoItem::getUrl() const
{
    if (seasonData != NULL)
        return seasonData->getInfoURL();

    return QString();
}

QString SeasonInfoItem::getDriverChampion() const
{
    if (seasonData != NULL)
    {
        QHash<QString, DriverSeasonData>::ConstIterator iter = seasonData->getDriverSeasonData()->begin();

        while (iter != seasonData->getDriverSeasonData()->end())
        {
            if (iter.value().getPosition() == 1)
            {
                QString driver;
                const DriverData *dd = DataManager::getInstance().getDriver(iter.value().getDriverId());

                if (dd != NULL)
                {
                    driver = dd->getDriverName();

                    if (!iter.value().getConstructorId().isEmpty())
                    {
                        const TeamData *td = DataManager::getInstance().getTeam(iter.value().getConstructorId().last());

                        if (td != NULL)
                            driver += QString(" (%1)").arg(td->getTeamName());
                    }

                    return driver;
                }

            }
            ++iter;
        }
    }

    return QString();
}

QString SeasonInfoItem::getConstructorChampion() const
{
    if (seasonData != NULL)
    {
        QHash<QString, TeamSeasonData>::ConstIterator iter = seasonData->getTeamSeasonData()->begin();

        while (iter != seasonData->getTeamSeasonData()->end())
        {
            if (iter.value().getPosition() == 1)
            {
                const TeamData *td = DataManager::getInstance().getTeam(iter.value().getConstructorId());

                if (td != NULL)
                    return td->getTeamName();
            }
            ++iter;
        }
    }

    return QString();
}

void SeasonInfoItem::updateData(const SeasonData *sd)
{
    if (sd != NULL)
    {
        seasonData = sd;
        emit seasonChanged();
        emit roundsChanged();
        emit urlChanged();
        emit driverChampionChanged();
        emit constructorChampionChanged();
    }
}
