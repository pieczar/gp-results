/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "circuitinfoitem.h"
#include "../core/datamanager.h"
#include "../core/circuitdata.h"

CircuitInfoItem::CircuitInfoItem(QQuickItem *parent) :
    QQuickItem(parent)
{
}

QString CircuitInfoItem::getName() const
{
    const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(circuitId);

    if (cd != NULL)
        return cd->getName();

    return "";
}

QString CircuitInfoItem::getLocation() const
{
    const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(circuitId);

    if (cd != NULL)
        return cd->getLocation().locality;

    return "";
}

QString CircuitInfoItem::getCountry() const
{
    const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(circuitId);

    if (cd != NULL)
        return cd->getLocation().country;

    return "";
}

QString CircuitInfoItem::getInfoURL() const
{
    const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(circuitId);

    if (cd != NULL)
        return cd->getInfoURL();

    return "";
}

double CircuitInfoItem::getLoc_lat() const
{
    const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(circuitId);

    if (cd != NULL)
        return cd->getLocation().loc_lat;

    return 0;
}

double CircuitInfoItem::getLoc_long() const
{
    const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(circuitId);

    if (cd != NULL)
        return cd->getLocation().loc_long;

    return 0;
}

void CircuitInfoItem::setData(QString circuit)
{
    circuitId = circuit;

    emit nameChanged();
    emit locationChanged();
    emit countryChanged();
    emit infoURLChanged();
    emit lot_latChanged();
    emit lot_longChanged();
}
