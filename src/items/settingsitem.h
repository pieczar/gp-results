/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef SETTINGSITEM_H
#define SETTINGSITEM_H

#include <QQuickItem>
#include <QSettings>

class SettingsItem : public QQuickItem
{
    Q_OBJECT
public:
    explicit SettingsItem(QQuickItem *parent = 0);
    ~SettingsItem();

    void loadSettings();
    void saveSettings();

    Q_PROPERTY (int fontSizeSmall READ getSmallFontSize WRITE setSmallFontSize NOTIFY smallFontSizeChanged)
    Q_PROPERTY (int fontSizeMedium READ getMediumFontSize WRITE setMediumFontSize NOTIFY mediumFontSizeChanged)
    Q_PROPERTY (int fontSizeLarge READ getLargeFontSize WRITE setLargeFontSize NOTIFY largeFontSizeChanged)
    Q_PROPERTY (int fontSizeExtraLarge READ getExtraLargeFontSize WRITE setExtraLargeFontSize NOTIFY extraLargeFontSizeChanged)
    Q_PROPERTY (QColor fontColorPrimary READ getFontPrimaryColor WRITE setFontPrimaryColor NOTIFY fontPrimaryColorChanged)
    Q_PROPERTY (QColor fontColorSecondary READ getFontSecondaryColor WRITE setFontSecondaryColor NOTIFY fontSecondaryColorChanged)
    Q_PROPERTY (bool isAndroid READ getIsAndroid)

    int getSmallFontSize() const
    {
        return m_smallFontSize;
    }
    int getMediumFontSize() const
    {
        return m_mediumFontSize;
    }
    int getLargeFontSize() const
    {
        return m_largeFontSize;
    }
    int getExtraLargeFontSize() const
    {
        return m_extraLargeFontSize;
    }
    QColor getFontPrimaryColor() const
    {
        return m_fontPrimaryColor;
    }
    QColor getFontSecondaryColor() const
    {
        return m_fontSecondaryColor;
    }

    void setSmallFontSize (int val)
    {
        m_smallFontSize = val;
        emit smallFontSizeChanged();
    }
    void setMediumFontSize (int val)
    {
        m_mediumFontSize = val;
        emit mediumFontSizeChanged();

        setSmallFontSize(val - 2);
        setLargeFontSize(val + 4);
        setExtraLargeFontSize(val + 8);

        saveSettings();
    }
    void setLargeFontSize (int val)
    {
        m_largeFontSize = val;
        emit largeFontSizeChanged();
    }
    void setExtraLargeFontSize (int val)
    {
        m_extraLargeFontSize = val;
        emit extraLargeFontSizeChanged();
    }
    void setFontPrimaryColor (QColor val)
    {
        m_fontPrimaryColor = val;
        emit fontPrimaryColorChanged();
    }
    void setFontSecondaryColor (QColor val)
    {
        m_fontSecondaryColor = val;
        emit fontSecondaryColorChanged();
    }

    bool getIsAndroid() const
    {
#if defined(Q_OS_ANDROID)
        return true;
#else
        return false;
#endif
    }

signals:
    void smallFontSizeChanged();
    void mediumFontSizeChanged();
    void largeFontSizeChanged();
    void extraLargeFontSizeChanged();
    void fontPrimaryColorChanged();
    void fontSecondaryColorChanged();

public slots:

private:
    int m_smallFontSize;
    int m_mediumFontSize;
    int m_largeFontSize;
    int m_extraLargeFontSize;
    QColor m_fontPrimaryColor;
    QColor m_fontSecondaryColor;

    QSettings *settings;

};

#endif // SETTINGSITEM_H
