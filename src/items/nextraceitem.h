/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef NEXTRACEITEM_H
#define NEXTRACEITEM_H

#include <QQuickItem>
#include "../core/eventdata.h"
#include "../core/circuitdata.h"

class NextRaceItem : public QQuickItem
{
    Q_OBJECT
public:
    explicit NextRaceItem(QQuickItem *parent = 0);

    Q_PROPERTY (QString raceDate READ getRaceDate() NOTIFY raceDateChanged)
    Q_PROPERTY (QString raceName READ getRaceName() NOTIFY raceNameChanged)
    Q_PROPERTY (QString circuitName READ getCircuitName() NOTIFY circuitNameChanged)
    Q_PROPERTY (QString location READ getLocation() NOTIFY locationChanged)
    Q_PROPERTY (QString country READ getCountry() NOTIFY countryChanged)
    Q_PROPERTY (QString url READ getUrl() NOTIFY urlChanged)
    Q_PROPERTY (QString circuitUrl READ getCircuitUrl() NOTIFY circuitUrlChanged)
    Q_PROPERTY (QString round READ getRound() NOTIFY roundChanged)

    Q_PROPERTY (QString days READ getDays() NOTIFY daysChanged)
    Q_PROPERTY (QString hours READ getHours() NOTIFY hoursChanged)
    Q_PROPERTY (QString minutes READ getMinutes() NOTIFY minutesChanged)
    Q_PROPERTY (QString seconds READ getSeconds() NOTIFY secondsChanged)


    QString getRaceDate() const { return ed.getRaceDate().toLocalTime().toString("dd MMM yyyy, hh:mm"); }
    QString getRaceName() const { return ed.getRaceName(); }
    QString getCircuitName() const { return cd.getName(); }
    QString getLocation() const { return QString("%1, %2").arg(cd.getLocation().locality).arg(cd.getLocation().country); }
    QString getCountry() const { return cd.getLocation().country; }
    QString getUrl() const { return ed.getInfoURL(); }
    QString getCircuitUrl() const { return cd.getInfoURL(); }
    QString getRound() const { return QString::number(ed.getRound()); }

    QString getDays() const { return m_days; }
    QString getHours() const { return m_hours; }
    QString getMinutes() const { return m_minutes; }
    QString getSeconds() const { return m_seconds;}


signals:
    void raceDateChanged();
    void raceNameChanged();
    void circuitNameChanged();
    void locationChanged();
    void countryChanged();
    void urlChanged();
    void circuitUrlChanged();
    void nameChanged();
    void roundChanged();

    void daysChanged();
    void hoursChanged();
    void minutesChanged();
    void secondsChanged();

public slots:

    void setData(const EventData &ed, const CircuitData &cd);
    void updateTime();

private:
    EventData ed;
    CircuitData cd;

    QString m_days;
    QString m_hours;
    QString m_minutes;
    QString m_seconds;

};

#endif // NEXTRACEITEM_H
