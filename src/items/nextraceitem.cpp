/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "nextraceitem.h"

NextRaceItem::NextRaceItem(QQuickItem *parent) :
    QQuickItem(parent)
{
}


void NextRaceItem::setData(const EventData &ed, const CircuitData &cd)
{
    this->ed = ed;
    this->cd = cd;

    emit raceDateChanged();
    emit raceNameChanged();
    emit circuitNameChanged();
    emit locationChanged();
    emit countryChanged();
    emit urlChanged();
    emit circuitUrlChanged();
    emit roundChanged();

    updateTime();
}

void NextRaceItem::updateTime()
{
    QDateTime currentDate = QDateTime::currentDateTime();

    int days = currentDate.daysTo(ed.getRaceDate());

    int seconds = currentDate.secsTo(ed.getRaceDate()) - 86400 * days;

    if (seconds < 0)
    {
        days -= 1;
        seconds = currentDate.secsTo(ed.getRaceDate()) - 86400 * days;
    }
    m_days = QString::number(days);

    int hours = seconds / 3600;

    if (hours < 0)
        hours = 0;

    seconds -= (hours * 3600);

    int minutes = seconds / 60;

    if (minutes < 0)
        minutes = 0;

    seconds -= minutes * 60;

    if (seconds < 0)
        seconds = 0;

    m_seconds = QString::number(seconds);

    m_minutes = QString::number(minutes);
    m_hours = QString::number(hours);

    emit secondsChanged();
    emit minutesChanged();
    emit hoursChanged();
    emit daysChanged();

}
