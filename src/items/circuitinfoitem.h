/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef CIRCUITINFOITEM_H
#define CIRCUITINFOITEM_H

#include <QQuickItem>
#include "../core/seasondata.h"

class CircuitInfoItem : public QQuickItem
{
    Q_OBJECT
public:
    explicit CircuitInfoItem(QQuickItem *parent = 0);

    Q_PROPERTY (QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY (QString location READ getLocation NOTIFY locationChanged)
    Q_PROPERTY (QString country READ getCountry NOTIFY countryChanged)
    Q_PROPERTY (QString infoURL READ getInfoURL NOTIFY infoURLChanged)
    Q_PROPERTY (double loc_lat READ getLoc_lat NOTIFY lot_latChanged)
    Q_PROPERTY (double loc_long READ getLoc_long NOTIFY lot_longChanged)

    QString getName() const;
    QString getLocation() const;
    QString getCountry() const;
    QString getInfoURL() const;
    double getLoc_lat() const;
    double getLoc_long() const;

signals:

    void nameChanged();
    void locationChanged();
    void countryChanged();
    void infoURLChanged();
    void lot_latChanged();
    void lot_longChanged();

public slots:

    void setData(QString circuit);

private:

    QString circuitId;

};

#endif // CIRCUITINFOITEM_H
