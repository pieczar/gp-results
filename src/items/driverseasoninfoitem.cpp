/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverseasoninfoitem.h"
#include "../core/datamanager.h"

DriverSeasonInfoItem::DriverSeasonInfoItem(QQuickItem *parent) :
    QQuickItem(parent)
{
    seasonData = NULL;
}

QString DriverSeasonInfoItem::getName() const
{
    if (seasonData != NULL)
    {
        const DriverData *dd = DataManager::getInstance().getDriver(seasonData->getDriverId());

        if (dd != NULL)
            return dd->getDriverName();
    }

    return QString();
}

QString DriverSeasonInfoItem::getWins() const
{
    if (seasonData != NULL)
    {
        return QString::number(seasonData->getWins());
    }

    return QString();
}

QString DriverSeasonInfoItem::getPoints() const
{
    if (seasonData != NULL)
    {
        return QString::number(seasonData->getPoints());
    }

    return QString();
}

QString DriverSeasonInfoItem::getUrl() const
{
    if (seasonData != NULL)
    {
        const DriverData *dd = DataManager::getInstance().getDriver(seasonData->getDriverId());

        if (dd != NULL)
            return dd->getBiographyURL();
    }

    return QString();
}

QString DriverSeasonInfoItem::getPosition() const
{
    if (seasonData != NULL)
    {
        return QString::number(seasonData->getPosition());
    }

    return QString();
}

QString DriverSeasonInfoItem::getAvgPosition() const
{
    if (seasonData != NULL)
    {
        const SeasonData *sd = DataManager::getInstance().getSeasonData(seasonData->getSeason());

        if (sd != NULL)
        {

            QHash<int, EventData>::ConstIterator iter = sd->getEvents().begin();

            int positionSum = 0;
            int rounds = 0;
            while (iter != sd->getEvents().end())
            {
                const DriverRaceData *rd = iter.value().getRaceData(seasonData->getDriverId());

                if (rd != NULL)
                {
                    ++rounds;
                    positionSum += rd->position;
                }
                ++iter;
            }

            if (rounds > 0)
            {
                double avg = (double)positionSum / (double)rounds;
                return QString::number(avg, 'f', 1);
            }
        }
    }
    return QString();
}

QString DriverSeasonInfoItem::getAvgGrid() const
{
    if (seasonData != NULL)
    {
        const SeasonData *sd = DataManager::getInstance().getSeasonData(seasonData->getSeason());

        if (sd != NULL)
        {

            QHash<int, EventData>::ConstIterator iter = sd->getEvents().begin();

            int positionSum = 0;
            int rounds = 0;
            while (iter != sd->getEvents().end())
            {
                const DriverRaceData *rd = iter.value().getRaceData(seasonData->getDriverId());

                if (rd != NULL)
                {
                    ++rounds;
                    positionSum += rd->grid;
                }
                ++iter;
            }

            if (rounds > 0)
            {
                double avg = (double)positionSum / (double)rounds;
                return QString::number(avg, 'f', 1);
            }
        }
    }
    return QString();
}

QString DriverSeasonInfoItem::getRetirements() const
{
    if (seasonData != NULL)
    {
        const SeasonData *sd = DataManager::getInstance().getSeasonData(seasonData->getSeason());

        if (sd != NULL)
        {

            QHash<int, EventData>::ConstIterator iter = sd->getEvents().begin();

            int ret = 0;
            while (iter != sd->getEvents().end())
            {
                const DriverRaceData *rd = iter.value().getRaceData(seasonData->getDriverId());

                if ((rd != NULL) && ((rd->status != "Finished") && !rd->status.contains("+")))
                    ++ret;

                ++iter;
            }

            if (ret > 0)
                return QString::number(ret);
        }
    }
    return "0";
}

QString DriverSeasonInfoItem::getPodiums() const
{
    if (seasonData != NULL)
    {
        const SeasonData *sd = DataManager::getInstance().getSeasonData(seasonData->getSeason());

        if (sd != NULL)
        {

            QHash<int, EventData>::ConstIterator iter = sd->getEvents().begin();

            int podiums = 0;
            while (iter != sd->getEvents().end())
            {
                const DriverRaceData *rd = iter.value().getRaceData(seasonData->getDriverId());

                if ((rd != NULL) && (rd->position < 4))
                    ++podiums;

                ++iter;
            }

            return QString::number(podiums);
        }
    }
    return "0";
}

void DriverSeasonInfoItem::updateData(const SeasonData *sm, QString driverId)
{
    if (sm != NULL && !sm->getEvents().isEmpty())
    {        
        seasonData = sm->getDriverSeasonData(driverId);

        if (seasonData != NULL)
        {
            emit nameChanged();
            emit winsChanged();
            emit pointsChanged();
            emit urlChanged();
            emit positionChanged();
            emit avgPositionChanged();
            emit avgGridChanged();
            emit retirementsChanged();
            emit podiumsChanged();
        }
    }
}
