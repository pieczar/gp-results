/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverraceinfoitem.h"
#include "../core/datamanager.h"


DriverRaceInfoItem::DriverRaceInfoItem(QQuickItem *parent) :
    QQuickItem(parent)
{
    raceData = NULL;
}

QString DriverRaceInfoItem::getName() const
{
    if (raceData != NULL)
    {
        const DriverData *dd = DataManager::getInstance().getDriver(raceData->driverID);

        if (dd != NULL)
            return dd->getDriverName();
    }
    return QString();
}

QString DriverRaceInfoItem::getTeam() const
{
    if (raceData != NULL)
    {
        const TeamData *td = DataManager::getInstance().getTeam(raceData->constructorID);

        if (td != NULL)
            return td->getTeamName();
    }
    return QString();
}

QString DriverRaceInfoItem::getPosition() const
{
    if (raceData != NULL)
        return QString::number(raceData->position);

    return QString();
}

QString DriverRaceInfoItem::getUrl() const
{
    if (raceData != NULL)
    {
        const DriverData *dd = DataManager::getInstance().getDriver(raceData->driverID);

        if (dd != NULL)
            return dd->getBiographyURL();
    }

    return QString();
}

QString DriverRaceInfoItem::getGrid() const
{
    if (raceData != NULL)
        return QString::number(raceData->grid);

    return QString();
}

QString DriverRaceInfoItem::getStatus() const
{
    if (raceData != NULL)
        return raceData->status;

    return QString();
}

QString DriverRaceInfoItem::getRaceTime() const
{
    if (raceData != NULL)
        return raceData->raceTime;

    return QString();
}

QString DriverRaceInfoItem::getFastestLap() const
{
    if (raceData != NULL)
        return raceData->fastestLap.toString();

    return QString();
}

QString DriverRaceInfoItem::getFastestLapNumber() const
{
    if (raceData != NULL)
        return QString::number(raceData->fastestLapNumber);

    return QString();
}

QString DriverRaceInfoItem::getAvgSpeed() const
{
    if (raceData != NULL)
        return raceData->avgSpeed + " " + raceData->avgSpeedUnits;

    return QString();
}

void DriverRaceInfoItem::updateData(const EventData *ed, QString driverId)
{
    if (ed != NULL)
    {
        raceData = ed->getRaceData(driverId);

        if (raceData != NULL)
        {
            emit nameChanged();
            emit teamChanged();
            emit positionChanged();
            emit urlChanged();
            emit gridChanged();
            emit statusChanged();
            emit raceTimeChanged();
            emit fastestLapChanged();
            emit fastestLapNumberChanged();
            emit avgSpeedChanged();
        }
    }
}
