/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "constructorseasoninfoitem.h"
#include "../core/datamanager.h"

ConstructorSeasonInfoItem::ConstructorSeasonInfoItem(QQuickItem *parent) :
    QQuickItem(parent)
{
    seasonData = NULL;
}

QString ConstructorSeasonInfoItem::getName() const
{
    if (seasonData != NULL)
    {
        const TeamData *td = DataManager::getInstance().getTeam(seasonData->getConstructorId());

        if (td != NULL)
            return td->getTeamName();
    }

    return QString();
}

QString ConstructorSeasonInfoItem::getWins() const
{
    if (seasonData != NULL)
    {
        return QString::number(seasonData->getWins());
    }

    return QString();
}

QString ConstructorSeasonInfoItem::getPoints() const
{
    if (seasonData != NULL)
    {
        return QString::number(seasonData->getPoints());
    }

    return QString();
}

QString ConstructorSeasonInfoItem::getUrl() const
{
    if (seasonData != NULL)
    {
        const TeamData *td = DataManager::getInstance().getTeam(seasonData->getConstructorId());

        if (td != NULL)
            return td->getInfoURL();
    }

    return QString();
}

QString ConstructorSeasonInfoItem::getPosition() const
{
    if (seasonData != NULL)
    {
        return QString::number(seasonData->getPosition());
    }

    return QString();
}

QString ConstructorSeasonInfoItem::getRetirements() const
{
    if (seasonData != NULL)
    {
        const SeasonData *sd = DataManager::getInstance().getSeasonData(seasonData->getSeason());

        if (sd != NULL)
        {

            QHash<int, EventData>::ConstIterator iter = sd->getEvents().begin();

            int ret = 0;
            while (iter != sd->getEvents().end())
            {
                const QVector<const DriverRaceData *> rd = iter.value().getRaceDataFromConstructor(seasonData->getConstructorId());

                for (int i = 0; i < rd.size(); ++i)
                {
                    if (((rd[i]->status != "Finished") && !rd[i]->status.contains("+")))
                        ++ret;
                }

                ++iter;
            }

            if (ret > 0)
                return QString::number(ret);
        }
    }
    return "0";
}

QString ConstructorSeasonInfoItem::getPodiums() const
{
    if (seasonData != NULL)
    {
        const SeasonData *sd = DataManager::getInstance().getSeasonData(seasonData->getSeason());

        if (sd != NULL)
        {

            QHash<int, EventData>::ConstIterator iter = sd->getEvents().begin();

            int podiums = 0;
            while (iter != sd->getEvents().end())
            {
                const QVector<const DriverRaceData *> rd = iter.value().getRaceDataFromConstructor(seasonData->getConstructorId());

                for (int i = 0; i < rd.size(); ++i)
                {

                    if ((rd[i]->position < 4))
                        ++podiums;
                }

                ++iter;
            }

            return QString::number(podiums);
        }
    }
    return "0";
}

void ConstructorSeasonInfoItem::updateData(const SeasonData *sm, QString constructorId)
{
    if (sm != NULL && !sm->getEvents().isEmpty())
    {
        seasonData = sm->getTeamSeasonData(constructorId);

        if (seasonData != NULL)
        {
            emit nameChanged();
            emit winsChanged();
            emit pointsChanged();
            emit urlChanged();
            emit positionChanged();
            emit retirementsChanged();
            emit podiumsChanged();
        }
    }
}
