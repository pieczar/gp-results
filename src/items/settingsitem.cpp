/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "settingsitem.h"

#include <QCoreApplication>
#include <QDir>

SettingsItem::SettingsItem(QQuickItem *parent) :
    QQuickItem(parent)
{
    m_smallFontSize = 18;
    m_mediumFontSize = 20;
    m_largeFontSize = 24;
    m_extraLargeFontSize = 28;

    m_fontPrimaryColor = "#DDDDDD";
    m_fontSecondaryColor = "#888888";

#if defined(Q_OS_ANDROID)
    QDir dir("/sdcard/GPResults");

    if (!dir.exists())
        dir.mkdir("/sdcard/GPResults");

    settings = new QSettings("/sdcard/GPResults/settings.ini", QSettings::IniFormat, this);
#elif WIN32
    settings = new QSettings(QCoreApplication::applicationDirPath() + "/settings.ini", QSettings::IniFormat, this);
#else
    QDir dir(QDir::homePath() + "/.config/GPResults");

    if (!dir.exists())
        dir.mkdir(QDir::homePath() + "/.config/GPResults");

    settings = new QSettings(QDir::homePath() + "/.config/GPResults/settings.ini", QSettings::IniFormat, this);
#endif

    loadSettings();
}

SettingsItem::~SettingsItem()
{
    saveSettings();
}

void SettingsItem::loadSettings()
{
    m_smallFontSize = settings->value("smallFontSize", 18).toInt();
    m_mediumFontSize = settings->value("mediumFontSize", 20).toInt();
    m_largeFontSize = settings->value("largeFontSize", 24).toInt();
    m_extraLargeFontSize = settings->value("extraLargeFontSize", 28).toInt();
}

void SettingsItem::saveSettings()
{
    settings->setValue("smallFontSize", m_smallFontSize); settings->sync();
    settings->setValue("mediumFontSize", m_mediumFontSize); settings->sync();
    settings->setValue("largeFontSize", m_largeFontSize); settings->sync();
    settings->setValue("extraLargeFontSize", m_extraLargeFontSize); settings->sync();

}
