/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERRACEINFOITEM_H
#define DRIVERRACEINFOITEM_H

#include <QQuickItem>
#include "../core/eventdata.h"

class DriverRaceInfoItem : public QQuickItem
{
    Q_OBJECT
public:
    explicit DriverRaceInfoItem(QQuickItem *parent = 0);

    Q_PROPERTY (QString name READ getName NOTIFY nameChanged)
    Q_PROPERTY (QString team READ getTeam NOTIFY teamChanged)
    Q_PROPERTY (QString position READ getPosition NOTIFY positionChanged)
    Q_PROPERTY (QString grid READ getGrid NOTIFY gridChanged)
    Q_PROPERTY (QString status READ getStatus NOTIFY statusChanged)
    Q_PROPERTY (QString raceTime READ getRaceTime NOTIFY raceTimeChanged)
    Q_PROPERTY (QString fastestLap READ getFastestLap NOTIFY fastestLapChanged)
    Q_PROPERTY (QString fastestLapNumber READ getFastestLapNumber NOTIFY fastestLapNumberChanged)
    Q_PROPERTY (QString avgSpeed READ getAvgSpeed NOTIFY avgSpeedChanged)
    Q_PROPERTY (QString url READ getUrl NOTIFY urlChanged)

    QString getName() const;
    QString getTeam() const;
    QString getPosition() const;
    QString getUrl() const;
    QString getGrid() const;
    QString getStatus() const;
    QString getRaceTime() const;
    QString getFastestLap() const;
    QString getFastestLapNumber() const;
    QString getAvgSpeed() const;

signals:

    void nameChanged();
    void teamChanged();
    void positionChanged();
    void urlChanged();
    void gridChanged();
    void statusChanged();
    void raceTimeChanged();
    void fastestLapChanged();
    void fastestLapNumberChanged();
    void avgSpeedChanged();

public slots:
    void updateData(const EventData *ed, QString driverId);

private:
    const DriverRaceData *raceData;
};

#endif // DRIVERRACEINFOITEM_H
