/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "raceschedulemodel.h"

#include "../core/datamanager.h"

RaceScheduleModel::RaceScheduleModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;    
}
QHash<int, QByteArray> RaceScheduleModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[ROUND_ROLE] = "round";
    roles[RACE_NAME_ROLE] = "race_name";
    roles[RACE_DATE_ROLE] = "race_date";
    roles[LOCATION_ROLE] = "location";
    roles[RACE_COMPLETED_ROLE] = "race_completed";

    return roles;
}

int RaceScheduleModel::rowCount(const QModelIndex &) const
{
    if (seasonData)
        return seasonData->getEvents().size();

    return 0;
}

int RaceScheduleModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant RaceScheduleModel::data(const QModelIndex &index, int role) const
{
    if ((seasonData == NULL) ||
        (index.row() < 0) ||
        (index.row() >= seasonData->getEvents().size()))
        return QString();

    int i = index.row();
    int round = i + 1;

    const CircuitData *cd = DataManager::getInstance().getCircuitManager().getCircuit(seasonData->getEvents()[round].getCircuitID());

    if ((cd == NULL) || (seasonData->getEvent(round) == NULL))
    {
        return QString();
    }

    switch (role)
    {
        case ROUND_ROLE: return index.row() + 1;
        case RACE_NAME_ROLE : return seasonData->getEvent(round)->getRaceName();
        case RACE_DATE_ROLE :
        {
            QString raceDate;

            int diff = 2;

            if (cd->getLocation().country.contains("Monaco"))
                diff = 3;

            int fpDay = seasonData->getEvent(round)->getRaceDate().date().day() - diff;

            if (fpDay > 0)
                raceDate = QString("%1-%2").arg(fpDay).arg(seasonData->getEvent(round)->getRaceDate().toString("d MMM"));

            else
            {
                QDateTime fpDate = seasonData->getEvent(round)->getRaceDate().addDays(-diff);

                raceDate = QString("%1-%2").arg(fpDate.toString("d MMM")).arg(seasonData->getEvent(round)->getRaceDate().toString("d MMM"));
            }
            return raceDate;
        }
        case LOCATION_ROLE: return QString("%1, %2").arg(cd->getLocation().locality).arg(cd->getLocation().country);

        case RACE_COMPLETED_ROLE:
        {
            QDateTime dt = QDateTime::currentDateTime().toLocalTime();
            QDateTime qualiTime = seasonData->getEvent(round)->getRaceDate().toLocalTime().addDays(-1);

            return qualiTime < dt;
        }
    }

    return QString();
}


void RaceScheduleModel::update(const SeasonData *dm)
{    
    if (dm != NULL)
    {
        if (rowCount() > 0)
            removeRows(0, rowCount());

        seasonData = dm;

        if (!seasonData->getEvents().isEmpty())
        {
            insertRows(0, seasonData->getEvents().size()-1);

            QModelIndex topLeft = QAbstractTableModel::index(0, 0);
            QModelIndex bottomRight = QAbstractTableModel::index(seasonData->getEvents().size()-1, 0);
            emit dataChanged(topLeft, bottomRight);
        }
    }
}
