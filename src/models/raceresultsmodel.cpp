/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "raceresultsmodel.h"

RaceResultsModel::RaceResultsModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;
}

QHash<int, QByteArray> RaceResultsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[POSITION_ROLE] = "position";
    roles[NAME_ROLE] = "name";
    roles[TEAM_NAME_ROLE] = "team_name";
    roles[GRID_ROLE] = "grid";
    roles[TIME_ROLE] = "time";
    roles[STATUS_ROLE] = "status";
    roles[LAPS_ROLE] = "laps";
    roles[DRIVER_ID_ROLE] = "driver_id";
    roles[ROUND_ROLE] = "round";
    roles[POINTS_ROLE] = "points";    

    return roles;
}

int RaceResultsModel::rowCount(const QModelIndex &) const
{
    return driverResults.size();
}

int RaceResultsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant RaceResultsModel::data(const QModelIndex &index, int role) const
{
    if ((driverResults.isEmpty()) ||
        (index.row() < 0) ||
        (index.row() >= driverResults.size()))
        return QString();

    int i = index.row();

    const DriverData *dd = DataManager::getInstance().getDriver(driverResults[i]->driverID);
    const TeamData *td = DataManager::getInstance().getTeam(driverResults[i]->constructorID);

    if ((dd == NULL) || (td == NULL))
        return QString();

    switch (role)
    {
        case POSITION_ROLE: return driverResults[i]->positionText;//index.row() + 1;
        case NAME_ROLE : return dd->getDriverName();
        case TEAM_NAME_ROLE : return td->getTeamName();
        case GRID_ROLE : return driverResults[i]->grid;
        case TIME_ROLE:
            if (driverResults[i]->raceTime.isEmpty())
            {
                if (driverResults[i]->status == "Did not qualify")
                    return "DNQ";

                if (driverResults[i]->status == "Did not prequalify")
                    return "DNPQ";

                return driverResults[i]->status;
            }

            return driverResults[i]->raceTime;
        case LAPS_ROLE: return driverResults[i]->laps;
        case DRIVER_ID_ROLE: return driverResults[i]->driverID;
        case ROUND_ROLE: return driverResults[i]->round;
        case POINTS_ROLE:
            if (driverResults[i]->points > 0)
                return QString("%1 P").arg(driverResults[i]->points);

            return "";                     
    }

    return QString();
}


void RaceResultsModel::update(const SeasonData *dm, int round)
{
    if (!driverResults.isEmpty())
        removeRows(0, driverResults.size());

    driverResults.clear();

    seasonData = dm;

    if (seasonData != NULL && !seasonData->getDriverSeasonData()->isEmpty())
    {        
        const EventData *ed = seasonData->getEvent(round);                

        if (ed != NULL && !seasonData->getDriverSeasonData()->isEmpty())
        {
            QHash<QString, DriverRaceData>::ConstIterator iter = ed->getRaceData()->begin();

            while (iter != ed->getRaceData()->end())
            {
                driverResults.append(&iter.value());
                ++iter;
            }
//            QHash<QString, DriverSeasonData>::ConstIterator iter = seasonData->getDriverSeasonData()->begin();

//            while (iter != seasonData->getDriverSeasonData()->end())
//            {
//                const DriverRaceData *drd = iter.value().getRaceData(round);

//                if (drd != NULL)
//                    driverResults.append(drd);

//                ++iter;
//            }

            if (!driverResults.isEmpty())
            {
                qSort(driverResults.begin(), driverResults.end(), DriverRaceDataLessThan());
                insertRows(0, driverResults.size()-1);

                QModelIndex topLeft = QAbstractTableModel::index(0, 0);
                QModelIndex bottomRight = QAbstractTableModel::index(driverResults.size()-1, 0);
                emit dataChanged(topLeft, bottomRight);
            }
        }
    }
}
