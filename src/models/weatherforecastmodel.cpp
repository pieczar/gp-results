/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "weatherforecastmodel.h"

WeatherForecastModel::WeatherForecastModel(QObject *parent) :
    QAbstractTableModel(parent)
{
}

QHash<int, QByteArray> WeatherForecastModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[DATE_ROLE] = "date";
    roles[DAY_ROLE] = "day";
    roles[DAY_TEMP_ROLE] = "day_temp";
    roles[NIGHT_TEMP_ROLE] = "night_temp";
    roles[MIN_TEMP_ROLE] = "min_temp";
    roles[MAX_TEMP_ROLE] = "max_temp";
    roles[PRESSURE_ROLE] = "pressure";
    roles[HUMIDITY_ROLE] = "humidity";
    roles[ICON_ROLE] = "icon";
    roles[WEATHER_ROLE] = "weather";
    roles[WIND_SPEED_ROLE] = "wind_speed";
    roles[WIND_DIRECTION_ROLE] = "wind_direction";

    return roles;
}

int WeatherForecastModel::rowCount(const QModelIndex &) const
{
    return weatherData.getDailyForecast().size();
}

int WeatherForecastModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant WeatherForecastModel::data(const QModelIndex &index, int role) const
{
    if ((index.row() < 0) ||
        (index.row() >= weatherData.getDailyForecast().size()))
        return QString();

    int i = index.row();
    switch (role)
    {
        case DATE_ROLE: return weatherData.getDailyForecast()[i].date.toString("dd-MM");
        case DAY_ROLE: return weatherData.getDailyForecast()[i].date.toString("ddd");
        case DAY_TEMP_ROLE: return QString::number(weatherData.getDailyForecast()[i].dayTemp, 'f', 1);
        case NIGHT_TEMP_ROLE: return QString::number(weatherData.getDailyForecast()[i].nightTemp, 'f', 1);
        case MIN_TEMP_ROLE: return QString::number(weatherData.getDailyForecast()[i].minTemp, 'f', 1);
        case MAX_TEMP_ROLE: return QString::number(weatherData.getDailyForecast()[i].maxTemp, 'f', 1);
        case PRESSURE_ROLE: return QString::number(weatherData.getDailyForecast()[i].pressure, 'f', 1);
        case HUMIDITY_ROLE: return weatherData.getDailyForecast()[i].humidity;
        case ICON_ROLE: return weatherData.getDailyForecast()[i].icon;
        case WEATHER_ROLE: return weatherData.getDailyForecast()[i].weather;
        case WIND_SPEED_ROLE: return QString::number(weatherData.getDailyForecast()[i].windSpeed, 'f', 1);
        case WIND_DIRECTION_ROLE: return weatherData.getDailyForecast()[i].windDirection;
    }

    return QString();
}

void WeatherForecastModel::update(const WeatherForecastData &wd)
{
    if (!weatherData.getDailyForecast().isEmpty())
        removeRows(0, weatherData.getDailyForecast().size());

    weatherData = wd;

    if (!weatherData.getDailyForecast().isEmpty())
    {
        insertRows(0, weatherData.getDailyForecast().size() - 1);

        QModelIndex topLeft = QAbstractTableModel::index(0, 0);
        QModelIndex bottomRight = QAbstractTableModel::index(weatherData.getDailyForecast().size()-1, 0);
        emit dataChanged(topLeft, bottomRight);
    }
}
