/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERSTANDINGSMODEL_H
#define DRIVERSTANDINGSMODEL_H

#include <QAbstractTableModel>
#include "../core/datamanager.h"

class DriverSeasonDataLessThan
{
public:
    bool operator() (const DriverSeasonData *d1, const DriverSeasonData *d2)
    {
        if (d1->getPosition() == 0)
            return false;

        if (d2->getPosition() == 0)
            return true;

        return d1->getPosition() < d2->getPosition();
    }
};

class DriverStandingsModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit DriverStandingsModel(QObject *parent = 0);

    enum Roles
    {
        POSITION_ROLE = Qt::UserRole + 1,
        NAME_ROLE,
        TEAM_NAME_ROLE,
        NATIONALITY_ROLE,
        POINTS_ROLE,
        WINS_ROLE,
        DRIVER_ID_ROLE
    };

    virtual QHash<int, QByteArray> roleNames() const;

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count);
        endInsertRows();
        return true;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginRemoveRows(parent, row, row + count - 1);
        endRemoveRows();
        return true;
    }
signals:

public slots:

    void update(const SeasonData *dm);

private:
    const SeasonData *seasonData;
    QList<const DriverSeasonData*> driverSeasonData;

};

#endif // DRIVERSTANDINGSMODEL_H
