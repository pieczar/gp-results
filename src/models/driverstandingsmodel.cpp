/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/


#include "../core/datamanager.h"
#include "driverstandingsmodel.h"

DriverStandingsModel::DriverStandingsModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;
}

QHash<int, QByteArray> DriverStandingsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[POSITION_ROLE] = "position";
    roles[NAME_ROLE] = "name";
    roles[TEAM_NAME_ROLE] = "team_name";
    roles[NATIONALITY_ROLE] = "nationality";
    roles[POINTS_ROLE] = "points";
    roles[WINS_ROLE] = "wins";
    roles[DRIVER_ID_ROLE] = "driver_id";

    return roles;
}

int DriverStandingsModel::rowCount(const QModelIndex &) const
{
    return driverSeasonData.size();
}

int DriverStandingsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant DriverStandingsModel::data(const QModelIndex &index, int role) const
{
    if ((driverSeasonData.isEmpty()) ||
        (index.row() < 0) ||
        (index.row() >= driverSeasonData.size()))
        return QString();

    int i = index.row();

    const DriverData *dd = DataManager::getInstance().getDriver(driverSeasonData[i]->getDriverId());

    QString teamNames;
    QVector<QString> constructorsId = driverSeasonData[i]->getConstructorId();

    for (int i = 0; i < constructorsId.size(); ++i)
    {
        const TeamData *td = DataManager::getInstance().getTeam(constructorsId[i]);

        if (td != NULL)
        {
            if (i == 0)
                teamNames = td->getTeamName();
            else
                teamNames += "/" + td->getTeamName();
        }
    }


    if (dd == NULL)
        return QString();

    switch (role)
    {
        case POSITION_ROLE: return driverSeasonData[i]->getPositionText();
        case NAME_ROLE : return dd->getDriverName();
        case TEAM_NAME_ROLE: return teamNames;
        case NATIONALITY_ROLE : return dd->getNationality();
        case POINTS_ROLE: return driverSeasonData[i]->getPoints();
        case WINS_ROLE: return driverSeasonData[i]->getWins();
        case DRIVER_ID_ROLE: return driverSeasonData[i]->getDriverId();
    }

    return QString();
}

void DriverStandingsModel::update(const SeasonData *dm)
{
    seasonData = dm;

    if (seasonData != NULL)
    {
        if (!driverSeasonData.isEmpty())
            removeRows(0, driverSeasonData.size());

        driverSeasonData.clear();

        if (!seasonData->getDriverSeasonData()->isEmpty())
        {
            insertRows(0, seasonData->getDriverSeasonData()->size()-1);

            QHash<QString, DriverSeasonData>::ConstIterator iter = seasonData->getDriverSeasonData()->begin();

            while (iter != seasonData->getDriverSeasonData()->end())
            {
                driverSeasonData.append(&iter.value());
                ++iter;
            }

            qSort(driverSeasonData.begin(), driverSeasonData.end(), DriverSeasonDataLessThan());

            QModelIndex topLeft = QAbstractTableModel::index(0, 0);
            QModelIndex bottomRight = QAbstractTableModel::index(driverSeasonData.size()-1, 0);
            emit dataChanged(topLeft, bottomRight);
        }
    }
}
