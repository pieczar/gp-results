/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverresultsmodel.h"

DriverResultsModel::DriverResultsModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;
}

QHash<int, QByteArray> DriverResultsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[ROUND_ROLE] = "round";
    roles[RACE_NAME_ROLE] = "name";
    roles[CONSTRUCTOR_NAME_ROLE] = "team";
    roles[GRID_ROLE] = "grid";
    roles[POSITION_ROLE] = "position";
    roles[STATUS_ROLE] = "status";
    roles[POINTS_ROLE] = "points";
    roles[DRIVER_ID_ROLE] = "driver_id";

    return roles;
}

int DriverResultsModel::rowCount(const QModelIndex &) const
{
    return rounds.size();
}

int DriverResultsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant DriverResultsModel::data(const QModelIndex &index, int role) const
{
    if ((seasonData == NULL) ||
        (seasonData->getEvents().isEmpty()) ||
        (index.row() < 0) ||
        (index.row() >= rounds.size()))
        return QString();


    int i = index.row();

    const EventData *ed = seasonData->getEvent(rounds[i]);

    if (ed == NULL)
        return QString();

    const DriverRaceData *dr = ed->getRaceData(driverId);

    if (dr == NULL)
        return QString();

    const TeamData *td = DataManager::getInstance().getTeam(dr->constructorID);

    switch (role)
    {
        case ROUND_ROLE: return ed->getRound();
        case RACE_NAME_ROLE : return ed->getRaceName();
        case CONSTRUCTOR_NAME_ROLE :
            if (td != NULL)
                return td->getTeamName();

            return QString();
        case GRID_ROLE : return dr->grid;
        case POSITION_ROLE: return dr->positionText;
        case STATUS_ROLE:
            if (dr->status.contains("+"))
                return "Finished";

            return dr->status;
        case POINTS_ROLE: return dr->points;
        case DRIVER_ID_ROLE: return driverId;
    }

    return QString();
}


void DriverResultsModel::update(const SeasonData *dm, QString driverId)
{
    if (dm != NULL && !dm->getEvents().isEmpty())
    {
        this->driverId = driverId;               

        if (!rounds.isEmpty())
            removeRows(0, rounds.size());

        seasonData = dm;

        QHash<int, EventData>::ConstIterator iter = seasonData->getEvents().begin();

        rounds.clear();
        while (iter != seasonData->getEvents().end())
        {
            if (iter.value().getRaceData(driverId) != NULL)            
                rounds.append(iter.value().getRound());

            ++iter;
        }

        qSort(rounds);
        insertRows(0, rounds.size()-1);

        QModelIndex topLeft = QAbstractTableModel::index(0, 0);
        QModelIndex bottomRight = QAbstractTableModel::index(rounds.size()-1, 0);

        emit dataChanged(topLeft, bottomRight);
    }
}
