/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "constructorraceresultsmodel.h"
#include "../core/datamanager.h"

ConstructorRaceResultsModel::ConstructorRaceResultsModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;
}

QHash<int, QByteArray> ConstructorRaceResultsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[ROUND_ROLE] = "round";

    roles[DRIVER_NAME_ROLE] = "driver_name";
    roles[DRIVER_GRID_ROLE] = "driver_grid";
    roles[DRIVER_POSITION_ROLE] = "driver_position";
    roles[DRIVER_STATUS_ROLE] = "driver_status";
    roles[DRIVER_POINTS_ROLE] = "driver_points";

    return roles;
}

int ConstructorRaceResultsModel::rowCount(const QModelIndex &) const
{
    return rows;
}

int ConstructorRaceResultsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant ConstructorRaceResultsModel::data(const QModelIndex &index, int role) const
{
    if ((seasonData == NULL) ||
        (rows == 0) ||
        (index.row() < 0) ||
        (index.row() >= rows))
        return QString();

    int i = index.row();

//    const EventData *ed = seasonData->getEvent(round);

//    if (ed == NULL)
//        return QString();

//    QVector<const DriverRaceData*> drivers = ed->getRaceDataFromConstructor(constructorId);

    if (drivers.isEmpty() || (i >= drivers.size()))
        return QString();

    switch (role)
    {
        case ROUND_ROLE: return round;
        case DRIVER_NAME_ROLE:
        {
            const DriverData *dd = DataManager::getInstance().getDriver(drivers[i]->driverID);

            if (dd != NULL)
                return dd->getDriverName();

            return QString();
        }
        case DRIVER_GRID_ROLE : return drivers[i]->grid;
        case DRIVER_POSITION_ROLE: return drivers[i]->positionText;
        case DRIVER_STATUS_ROLE:
            if (drivers[i]->status.contains("+"))
                return "Finished";

            return drivers[i]->status;
        case DRIVER_POINTS_ROLE: return drivers[i]->points;
    }

    return QString();
}


void ConstructorRaceResultsModel::update(const SeasonData *dm, QString constructorId, int round)
{
    if (dm != NULL && !dm->getEvents().isEmpty())
    {
        this->constructorId = constructorId;

        if (rows > 0)
            removeRows(0, rows);

        seasonData = dm;
        this->round = round;

        const EventData *ed = seasonData->getEvent(round);
        drivers = ed->getRaceDataFromConstructor(constructorId);

        rows = drivers.size();

        if (rows > 0)
        {
            insertRows(0, rows-1);

            QModelIndex topLeft = QAbstractTableModel::index(0, 0);
            QModelIndex bottomRight = QAbstractTableModel::index(rows-1, 0);
            emit dataChanged(topLeft, bottomRight);
        }
    }
}
