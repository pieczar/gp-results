/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "constructorstandingsmodel.h"

ConstructorStandingsModel::ConstructorStandingsModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;
}

QHash<int, QByteArray> ConstructorStandingsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[POSITION_ROLE] = "position";
    roles[NAME_ROLE] = "name";
    roles[NATIONALITY_ROLE] = "nationality";
    roles[POINTS_ROLE] = "points";
    roles[WINS_ROLE] = "wins";
    roles[CONSTRUCTOR_ID_ROLE] = "constructor_id";

    return roles;
}

int ConstructorStandingsModel::rowCount(const QModelIndex &) const
{
    return teamSeasonData.size();
}

int ConstructorStandingsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant ConstructorStandingsModel::data(const QModelIndex &index, int role) const
{
    if ((teamSeasonData.isEmpty()) ||
        (index.row() < 0) ||
        (index.row() >= teamSeasonData.size()))
        return QString();

    int i = index.row();

    const TeamData *td = DataManager::getInstance().getTeam(teamSeasonData[i]->getConstructorId());

    if (td == NULL)
        return QString();

    switch (role)
    {
        case POSITION_ROLE: return teamSeasonData[i]->getPositionText();
        case NAME_ROLE : return td->getTeamName();
        case NATIONALITY_ROLE : return td->getNationality();
        case POINTS_ROLE: return teamSeasonData[i]->getPoints();
        case WINS_ROLE: return teamSeasonData[i]->getWins();
        case CONSTRUCTOR_ID_ROLE: return teamSeasonData[i]->getConstructorId();
    }

    return QString();
}


void ConstructorStandingsModel::update(const SeasonData *dm)
{
    seasonData = dm;

    if (seasonData != NULL)
    {
        if (!teamSeasonData.isEmpty())
            removeRows(0, teamSeasonData.size());

        teamSeasonData.clear();

        if (!seasonData->getTeamSeasonData()->isEmpty())
        {
            insertRows(0, seasonData->getTeamSeasonData()->size()-1);

            QHash<QString, TeamSeasonData>::ConstIterator iter = seasonData->getTeamSeasonData()->begin();

            while (iter != seasonData->getTeamSeasonData()->end())
            {
                teamSeasonData.append(&iter.value());
                ++iter;
            }

            qSort(teamSeasonData.begin(), teamSeasonData.end(), TeamSeasonDataLessThan());

            QModelIndex topLeft = QAbstractTableModel::index(0, 0);
            QModelIndex bottomRight = QAbstractTableModel::index(teamSeasonData.size()-1, 0);
            emit dataChanged(topLeft, bottomRight);
        }
    }
}
