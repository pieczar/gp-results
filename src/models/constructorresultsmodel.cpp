/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "constructorresultsmodel.h"
#include "../core/datamanager.h"

ConstructorResultsModel::ConstructorResultsModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;
}

QHash<int, QByteArray> ConstructorResultsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[ROUND_ROLE] = "round";
    roles[RACE_NAME_ROLE] = "name";
    roles[CONSTRUCTOR_ID_ROLE] = "constructor_id";

    return roles;
}

int ConstructorResultsModel::rowCount(const QModelIndex &) const
{
    return rounds.size();
}

int ConstructorResultsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant ConstructorResultsModel::data(const QModelIndex &index, int role) const
{
    if ((seasonData == NULL) ||
        (rounds.size() == 0) ||
        (index.row() < 0) ||
        (index.row() >= rounds.size()))
        return QString();

    int i = index.row();

    const EventData *ed = seasonData->getEvent(rounds[i]);

    if (ed == NULL)
        return QString();

    switch (role)
    {
        case ROUND_ROLE: return ed->getRound();
        case RACE_NAME_ROLE : return ed->getRaceName();
        case CONSTRUCTOR_ID_ROLE: return constructorId;
    }

    return QString();
}


void ConstructorResultsModel::update(const SeasonData *dm, QString constructorId)
{
    if (dm != NULL && !dm->getEvents().isEmpty())
    {
        this->constructorId = constructorId;

        if ((seasonData != NULL) && !rounds.isEmpty())
            removeRows(0, rounds.size());

        rounds.clear();
        seasonData = dm;

        QHash<int, EventData>::ConstIterator iter = seasonData->getEvents().begin();

        while (iter != seasonData->getEvents().end())
        {
            QVector<const DriverRaceData*> drivers = iter.value().getRaceDataFromConstructor(constructorId);

            if (!drivers.isEmpty())
                rounds.append(iter.value().getRound());

            ++iter;
        }

        qSort(rounds);

        insertRows(0, rounds.size()-1);

        QModelIndex topLeft = QAbstractTableModel::index(0, 0);
        QModelIndex bottomRight = QAbstractTableModel::index(rounds.size()-1, 0);
        emit dataChanged(topLeft, bottomRight);
    }
}
