/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef WEATHERFORECASTMODEL_H
#define WEATHERFORECASTMODEL_H

#include <QAbstractTableModel>

#include "../core/weatherforecastdata.h"

class WeatherForecastModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit WeatherForecastModel(QObject *parent = 0);

    enum Roles
    {
        DATE_ROLE = Qt::UserRole + 1,
        DAY_ROLE,
        DAY_TEMP_ROLE,
        NIGHT_TEMP_ROLE,
        MIN_TEMP_ROLE,
        MAX_TEMP_ROLE,
        PRESSURE_ROLE,
        HUMIDITY_ROLE,
        ICON_ROLE,
        WEATHER_ROLE,
        WIND_SPEED_ROLE,
        WIND_DIRECTION_ROLE
    };

    virtual QHash<int, QByteArray> roleNames() const;

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count);
        endInsertRows();
        return true;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginRemoveRows(parent, row, row + count - 1);
        endRemoveRows();
        return true;
    }
signals:

public slots:

    void update(const WeatherForecastData &wd);

private:
    WeatherForecastData weatherData;

};

#endif // WEATHERFORECASTMODEL_H
