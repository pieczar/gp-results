/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef CONSTRUCTORRACERESULTSMODEL_H
#define CONSTRUCTORRACERESULTSMODEL_H

#include <QAbstractTableModel>
#include "../core/seasondata.h"

class ConstructorRaceResultsModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ConstructorRaceResultsModel(QObject *parent = 0);

    enum Roles
    {
        ROUND_ROLE = Qt::UserRole + 1,
        DRIVER_NAME_ROLE,
        DRIVER_GRID_ROLE,
        DRIVER_POSITION_ROLE,
        DRIVER_STATUS_ROLE,
        DRIVER_POINTS_ROLE
    };

    virtual QHash<int, QByteArray> roleNames() const;

    virtual int columnCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count);
        endInsertRows();
        return true;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginRemoveRows(parent, row, row + count - 1);
        endRemoveRows();
        return true;
    }

signals:

public slots:

    virtual int rowCount(const QModelIndex & parent = QModelIndex()) const;
    void update(const SeasonData *sm, QString constructorId, int round);

private:
    const SeasonData *seasonData;
    QVector<const DriverRaceData*> drivers;
    QString constructorId;
    int round;
    int rows;

};

#endif // CONSTRUCTORRACERESULTSMODEL_H
