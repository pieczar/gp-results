/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "qualiresultsmodel.h"

QualiResultsModel::QualiResultsModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    seasonData = NULL;
}

QHash<int, QByteArray> QualiResultsModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[POSITION_ROLE] = "position";
    roles[NAME_ROLE] = "name";
    roles[TEAM_NAME_ROLE] = "team_name";
    roles[Q1_ROLE] = "q1";
    roles[Q2_ROLE] = "q2";
    roles[Q3_ROLE] = "q3";
    roles[SEASON_ROLE] = "season";
    roles[DRIVER_ID_ROLE] = "driver_id";
    roles[ROUND_ROLE] = "round";

    return roles;
}

int QualiResultsModel::rowCount(const QModelIndex &) const
{
    return driverResults.size();
}

int QualiResultsModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant QualiResultsModel::data(const QModelIndex &index, int role) const
{
    if ((driverResults.isEmpty()) ||
        (index.row() < 0) ||
        (index.row() >= driverResults.size()))
        return QString();

    int i = index.row();

    const DriverData *dd = DataManager::getInstance().getDriver(driverResults[i]->driverID);
    const TeamData *td = DataManager::getInstance().getTeam(driverResults[i]->constructorID);

    if ((dd == NULL) || (td == NULL))
        return QString();

    switch (role)
    {
        case POSITION_ROLE: return driverResults[i]->position;
        case NAME_ROLE : return dd->getDriverName();
        case TEAM_NAME_ROLE : return td->getTeamName();
        case Q1_ROLE : return driverResults[i]->time[DriverQualiData::Q1].toString();
        case Q2_ROLE : return driverResults[i]->time[DriverQualiData::Q2].toString();
        case Q3_ROLE : return driverResults[i]->time[DriverQualiData::Q3].toString();
        case SEASON_ROLE : return driverResults[i]->season;
        case DRIVER_ID_ROLE: return driverResults[i]->driverID;
        case ROUND_ROLE: return driverResults[i]->round;
    }

    return QString();
}


void QualiResultsModel::update(const SeasonData *dm, int round)
{
    seasonData = dm;

    if (!driverResults.isEmpty())
        removeRows(0, driverResults.size());

    driverResults.clear();

    if (seasonData != NULL && !seasonData->getDriverSeasonData()->isEmpty())
    {
        const EventData *ed = seasonData->getEvent(round);

        if (ed != NULL && !seasonData->getDriverSeasonData()->isEmpty())
        {
            QHash<QString, DriverQualiData>::ConstIterator iter = ed->getQualiData()->begin();

            while (iter != ed->getQualiData()->end())
            {
                driverResults.append(&iter.value());
                ++iter;
            }
//            QHash<QString, DriverSeasonData>::ConstIterator iter = seasonData->getDriverSeasonData()->begin();

//            while (iter != seasonData->getDriverSeasonData()->end())
//            {
//                const DriverRaceData *drd = iter.value().getRaceData(round);

//                if (drd != NULL)
//                    driverResults.append(drd);

//                ++iter;
//            }

            if (!driverResults.isEmpty())
            {
                qSort(driverResults.begin(), driverResults.end(), DriverQualiDataLessThan());
                insertRows(0, driverResults.size()-1);

                QModelIndex topLeft = QAbstractTableModel::index(0, 0);
                QModelIndex bottomRight = QAbstractTableModel::index(driverResults.size()-1, 0);
                emit dataChanged(topLeft, bottomRight);
            }
        }
    }
}
