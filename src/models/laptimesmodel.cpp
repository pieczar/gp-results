/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "laptimesmodel.h"

LapTimesModel::LapTimesModel(QObject *parent) :
    QAbstractTableModel(parent)
{
    raceData = NULL;
}

QHash<int, QByteArray> LapTimesModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[NUMBER_ROLE] = "number";
    roles[POSITION_ROLE] = "position";
    roles[TIME_ROLE] = "time";
    roles[GAP_ROLE] = "gap";
    roles[BEST_TIME_ROLE] = "best_time";

    return roles;
}

int LapTimesModel::rowCount(const QModelIndex &) const
{
    if (raceData != NULL)
        return raceData->lapData.size();

    return 0;
}

int LapTimesModel::columnCount(const QModelIndex &) const
{
    return 1;
}

QVariant LapTimesModel::data(const QModelIndex &index, int role) const
{
    if ((raceData == NULL) || (raceData->lapData.isEmpty()) ||
        (index.row() < 0) ||
        (index.row() >= raceData->lapData.size()))
        return QString();

    int i = index.row();

    const LapData &ld = raceData->lapData[i];

    LapTime bestTime = raceData->fastestLap;

    switch (role)
    {
        case NUMBER_ROLE: return ld.getLapNumber();
        case POSITION_ROLE : return ld.getPosition();
        case TIME_ROLE :
        {
            QString time = ld.getLapTime().toString();
            PitStopData pd;
            pd.driverID = raceData->driverID;
            pd.lap = ld.getLapNumber();
            int idx = raceData->pitStopData.indexOf(pd);
            if (idx != -1)
                time += QString(" (P %1)").arg(raceData->pitStopData[idx].duration.toString());

            return time;
        }
        case GAP_ROLE:
            if (ld.getLapNumber() != raceData->fastestLapNumber)
                return QString("+%1").arg((ld.getLapTime().toDouble() - bestTime.toDouble()), 0, 'f', 3);

            return "";

        case BEST_TIME_ROLE :
            if (ld.getLapNumber() == raceData->fastestLapNumber)
                return true;

            return false;
    }

    return QString();
}

void LapTimesModel::update(const EventData *ed, QString driverId)
{
    if (ed != NULL)
    {
        if ((raceData != NULL) && !raceData->lapData.isEmpty())
            removeRows(0, raceData->lapData.size());

        const DriverRaceData *dr = ed->getRaceData(driverId);

        if (dr != NULL && !dr->lapData.isEmpty())
        {
            raceData = dr;
            insertRows(0, raceData->lapData.size()-1);

            QModelIndex topLeft = QAbstractTableModel::index(0, 0);
            QModelIndex bottomRight = QAbstractTableModel::index(raceData->lapData.size()-1, 0);
            emit dataChanged(topLeft, bottomRight);
        }
    }
}
