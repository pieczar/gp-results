/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "seasondata.h"

SeasonData::SeasonData() : season(0), rounds(0), scheduleObtained(false)
{
}

void SeasonData::loadData(QDataStream &stream)
{
    stream >> season;
    stream >> rounds;
    stream >> infoURL;

    int size;
    stream >> size;

    for (int i = 0; i < size; ++i)
    {
        DriverSeasonData ds;
        QString buf; int ibuf;
        QVector<QString> vbuf; double dbuf;

        stream >> buf; ds.setDriverId(buf);
        stream >> vbuf; ds.setConstructorId(vbuf);
        stream >> ibuf; ds.setPosition(ibuf);
        stream >> ibuf; ds.setWins(ibuf);
        stream >> dbuf; ds.setPoints(dbuf);
        stream >> ibuf; ds.setNumber(ibuf);

        ds.setSeason(season);

        addDriverSeasonData(ds);
    }

    stream >> size;

    for (int i = 0; i < size; ++i)
    {
        TeamSeasonData ts;
        QString buf; int ibuf; double dbuf;
        stream >> buf; ts.setConstructorId(buf);
        stream >> ibuf; ts.setPosition(ibuf);
        stream >> ibuf; ts.setWins(ibuf);
        stream >> dbuf; ts.setPoints(dbuf);

        ts.setSeason(season);

        addTeamSeasonData(ts);
    }

    stream >> size;
    for (int i = 0; i < size; ++i)
    {
        EventData ed;
        ed.loadData(stream, season);

        addEvent(ed);
    }

    if (size > 0)
        scheduleObtained = true;
}

void SeasonData::saveData(QDataStream &stream)
{
    stream << season;
    stream << rounds;
    stream << infoURL;

    QHash<QString, DriverSeasonData>::ConstIterator iter = driverSeasonData.begin();

    stream << driverSeasonData.size();
    while (iter != driverSeasonData.end())
    {
        stream << iter.value().getDriverId();
        stream << iter.value().getConstructorId();
        stream << iter.value().getPosition();
        stream << iter.value().getWins();
        stream << iter.value().getPoints();
        stream << iter.value().getNumber();

        ++iter;
    }

    stream << teamSeasonData.size();

    QHash<QString, TeamSeasonData>::ConstIterator tIter = teamSeasonData.begin();
    while (tIter != teamSeasonData.end())
    {
        stream << tIter.value().getConstructorId();
        stream << tIter.value().getPosition();
        stream << tIter.value().getWins();
        stream << tIter.value().getPoints();

        ++tIter;
    }

    stream << events.size();
    QHash<int, EventData>::ConstIterator eIter = events.begin();
    while (eIter != events.end())
    {
        eIter.value().saveData(stream);
        ++eIter;
    }
}


const EventData *SeasonData::getEvent(int round) const
{
    QHash<int, EventData>::ConstIterator iter = events.find(round);

    if (iter != events.end())
        return &iter.value();

    return NULL;
}

EventData *SeasonData::getEvent(int round)
{
    QHash<int, EventData>::Iterator iter = events.find(round);

    if (iter != events.end())
        return &iter.value();

    return NULL;
}
int SeasonData::getSeason() const
{
    return season;
}

void SeasonData::setSeason(int value)
{
    season = value;
}
QString SeasonData::getInfoURL() const
{
    return infoURL;
}

void SeasonData::setInfoURL(const QString &value)
{
    infoURL = value;
}
const QHash<QString, DriverSeasonData> *SeasonData::getDriverSeasonData() const
{
    return &driverSeasonData;
}

void SeasonData::setDriverSeasonData(const QVector<DriverSeasonData> &value)
{
    for (int i = 0; i < value.size(); ++i)
    {
        driverSeasonData[value[i].getDriverId()] = value[i];
    }
}

DriverSeasonData *SeasonData::getDriverSeasonData(QString driverId)
{
    if (driverSeasonData.contains(driverId))
        return &driverSeasonData[driverId];

    return NULL;
}

const DriverSeasonData *SeasonData::getDriverSeasonData(QString driverId) const
{
    QHash<QString, DriverSeasonData>::ConstIterator iter = driverSeasonData.find(driverId);

    if (iter != driverSeasonData.end())
        return &iter.value();

    return NULL;
}

void SeasonData::addDriverSeasonData(const DriverSeasonData &dsd)
{
    driverSeasonData[dsd.getDriverId()] = dsd;
}
const QHash<QString, TeamSeasonData> *SeasonData::getTeamSeasonData() const
{
    return &teamSeasonData;
}

void SeasonData::setTeamSeasonData(const QVector<TeamSeasonData> &value)
{
    for (int i = 0; i < value.size(); ++i)
        teamSeasonData[value[i].getConstructorId()] = value[i];
}

TeamSeasonData *SeasonData::getTeamSeasonData(QString constructorId)
{
    if (teamSeasonData.contains(constructorId))
        return &teamSeasonData[constructorId];

    return NULL;
}

const TeamSeasonData *SeasonData::getTeamSeasonData(QString constructorId) const
{
    QHash<QString, TeamSeasonData>::ConstIterator iter = teamSeasonData.find(constructorId);

    if (iter != teamSeasonData.end())
        return &iter.value();

    return NULL;
}

void SeasonData::addTeamSeasonData(const TeamSeasonData &tsd)
{
    teamSeasonData[tsd.getConstructorId()] = tsd;
}
int SeasonData::getRounds() const
{
    return rounds;
}

void SeasonData::setRounds(int value)
{
    rounds = value;
}
QDate SeasonData::getDriversStandingsDate() const
{
    return driversStandingsDate;
}

void SeasonData::setDriversStandingsDate(const QDate &value)
{
    driversStandingsDate = value;
}
QDate SeasonData::getConstructorsStandingsDate() const
{
    return constructorsStandingsDate;
}

void SeasonData::setConstructorsStandingsDate(const QDate &value)
{
    constructorsStandingsDate = value;
}
bool SeasonData::getScheduleObtained() const
{
    return scheduleObtained;
}

void SeasonData::setScheduleObtained(bool value)
{
    scheduleObtained = value;
}









