/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef SEASONDATA_H
#define SEASONDATA_H

#include <QDataStream>
#include <QMetaType>
#include <QHash>

#include "driverseasondata.h"
#include "eventdata.h"
#include "teamseasondata.h"

class SeasonData
{
public:
    SeasonData();

    void loadData(QDataStream &stream);
    void saveData(QDataStream &stream);

    const QHash<int, EventData> &getEvents() const
    {
        return events;
    }
    void addEvent(const EventData &ed)
    {
        events[ed.getRound()] = ed;
    }

    const EventData *getEvent(int round) const;
    EventData *getEvent(int round);

    int getSeason() const;
    void setSeason(int value);

    QString getInfoURL() const;
    void setInfoURL(const QString &value);

    bool operator < (const SeasonData &sd) const
    {
        return season < sd.season;
    }

    const QHash<QString, DriverSeasonData> *getDriverSeasonData() const;
    void setDriverSeasonData(const QVector<DriverSeasonData> &value);

    DriverSeasonData *getDriverSeasonData(QString driverId);
    const DriverSeasonData *getDriverSeasonData(QString driverId) const;
    void addDriverSeasonData(const DriverSeasonData &dsd);

    const QHash<QString, TeamSeasonData> *getTeamSeasonData() const;
    void setTeamSeasonData(const QVector<TeamSeasonData> &value);

    TeamSeasonData *getTeamSeasonData(QString constructorId);
    const TeamSeasonData *getTeamSeasonData(QString constructorId) const;
    void addTeamSeasonData(const TeamSeasonData &tsd);

    int getRounds() const;
    void setRounds(int value);

    QDate getDriversStandingsDate() const;
    void setDriversStandingsDate(const QDate &value);

    QDate getConstructorsStandingsDate() const;
    void setConstructorsStandingsDate(const QDate &value);

    bool getScheduleObtained() const;
    void setScheduleObtained(bool value);

private:
    int season;
    int rounds;
    QString infoURL;

    QHash<QString, DriverSeasonData> driverSeasonData;
    QHash<QString, TeamSeasonData> teamSeasonData;

    QHash<int, EventData> events;
    QDate driversStandingsDate;
    QDate constructorsStandingsDate;

    bool scheduleObtained;
};

Q_DECLARE_METATYPE(SeasonData)
Q_DECLARE_METATYPE(const SeasonData*)

#endif // SEASONDATA_H
