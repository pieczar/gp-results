/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "teamdata.h"

TeamData::TeamData()
{
}

TeamData::TeamData(QString ID, QString name, QString nat, QString url) : constructorID(ID), teamName(name), nationality(nat), infoURL(url)
{

}
QString TeamData::getConstructorID() const
{
    return constructorID;
}

void TeamData::setConstructorID(const QString &value)
{
    constructorID = value;
}
QString TeamData::getTeamName() const
{
    return teamName;
}

void TeamData::setTeamName(const QString &value)
{
    teamName = value;
}
QString TeamData::getNationality() const
{
    return nationality;
}

void TeamData::setNationality(const QString &value)
{
    nationality = value;
}
QString TeamData::getInfoURL() const
{
    return infoURL;
}

void TeamData::setInfoURL(const QString &value)
{
    infoURL = value;
}






