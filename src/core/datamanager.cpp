/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "datamanager.h"
#include <QDir>
#include <QFile>
#include <QDataStream>


DataManager::DataManager()
{
    connectionManager = new ConnectionAPIManager(this);
    connect(connectionManager, SIGNAL(nextRaceDataObtained(EventData,CircuitData)), this,
            SLOT(onNextRaceDataObtained(EventData,CircuitData)));

    connect (connectionManager, SIGNAL(driversStandingsObtained(QVector<DriverData>,QVector<DriverSeasonData>,QVector<TeamData>)), this,
            SLOT(onDriverStandingsObtained(QVector<DriverData>,QVector<DriverSeasonData>,QVector<TeamData>)));
    connect (connectionManager, SIGNAL(constructorStandingsObtained(QVector<TeamData>,QVector<TeamSeasonData>)), this,
            SLOT(onConstructorStandingsObtained(QVector<TeamData>,QVector<TeamSeasonData>)));
    connect (connectionManager, SIGNAL(raceScheduleObtained(QVector<EventData>,QVector<CircuitData>)), this,
            SLOT(onRaceScheduleObtained(QVector<EventData>,QVector<CircuitData>)));
    connect (connectionManager, SIGNAL(raceResultsObtained(EventData,CircuitData,QVector<DriverData>,QVector<DriverSeasonData>,QVector<TeamData>)), this,
            SLOT(onRaceResultsObtained(EventData,CircuitData,QVector<DriverData>,QVector<DriverSeasonData>,QVector<TeamData>)));
    connect (connectionManager, SIGNAL(qualiResultsObtained(EventData,CircuitData,QVector<DriverData>,QVector<DriverSeasonData>,QVector<TeamData>)), this,
            SLOT(onQualiResultsObtained(EventData,CircuitData,QVector<DriverData>,QVector<DriverSeasonData>,QVector<TeamData>)));
    connect (connectionManager, SIGNAL(lapTimesObtained(QVector<LapData>,int,int,QString)), this,
            SLOT(onLapTimesObtained(QVector<LapData>,int,int,QString)));
    connect (connectionManager, SIGNAL(pitStopsObtained(QVector<PitStopData>,int,int,QString)), this,
            SLOT(onPitStopsObtained(QVector<PitStopData>,int,int,QString)));
    connect (connectionManager, SIGNAL(driverResultsObtained(QVector<EventData>,QVector<CircuitData>,QVector<TeamData>,QString)), this,
            SLOT(onDriverResultsObtained(QVector<EventData>,QVector<CircuitData>,QVector<TeamData>,QString)));
    connect (connectionManager, SIGNAL(constructorResultsObtained(QVector<EventData>,QVector<CircuitData>,QVector<DriverData>,QString)), this,
            SLOT(onConstructorResultsObtained(QVector<EventData>,QVector<CircuitData>,QVector<DriverData>,QString)));
    connect (connectionManager, SIGNAL(weatherForecastObtained(WeatherForecastData)), this,
            SLOT(onWeatherForecastObtained(WeatherForecastData)));

    connect (connectionManager, SIGNAL(connectionError(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));

    getRaceSchedule();
    getDriverStandings();
    getConstructorStandings();
}

DataManager::~DataManager()
{
//    saveData();
}

void DataManager::loadData(QString fileName)
{    
    dataFilePath = fileName;
    QFile file(fileName);

    if (file.open(QIODevice::ReadOnly))
    {
        QDataStream stream(&file);

        QString buf;
        stream >> buf;

        if (buf == "FData")
        {
            int size;
            stream >> size;

            for (int i = 0; i < size; ++i)
            {
                DriverData dd;
                QDate birth;
                stream >> buf; dd.setDriverID(buf);
                stream >> buf; dd.setDriverName(buf);
                stream >> birth; dd.setBirthDate(birth);
                stream >> buf; dd.setNationality(buf);
                stream >> buf; dd.setBiographyURL(buf);

                addDriver(dd);
            }
            stream >> size;
            for (int i = 0; i < size; ++i)
            {
                TeamData td;
                stream >> buf; td.setConstructorID(buf);
                stream >> buf; td.setTeamName(buf);
                stream >> buf; td.setNationality(buf);
                stream >> buf; td.setInfoURL(buf);
                addTeam(td);
            }

            circuitManager.loadData(stream);

            bool seasonStored;
            stream >> seasonStored;

            if (seasonStored)
            {
                SeasonData sd;
                sd.loadData(stream);

                seasons[sd.getSeason()] = sd;
            }
        }
    }
}

void DataManager::saveData()
{
    QFile file(dataFilePath);
    if (file.open(QIODevice::WriteOnly))
    {
        QDataStream stream(&file);

        stream << QString("FData");

        stream << drivers.size();

        QHash <QString, DriverData>::ConstIterator iter = drivers.begin();

        int i = 0;
        while (iter != drivers.end())
        {
            stream << iter.value().getDriverID();
            stream << iter.value().getDriverName();
            stream << iter.value().getBirthDate();
            stream << iter.value().getNationality();
            stream << iter.value().getBiographyURL();

            ++iter;
            ++i;
        }

        stream << teams.size();
        QHash <QString, TeamData>::ConstIterator tIter = teams.begin();

        while (tIter != teams.end())
        {
            stream << tIter.value().getConstructorID();
            stream << tIter.value().getTeamName();
            stream << tIter.value().getNationality();
            stream << tIter.value().getInfoURL();

            ++tIter;
        }

        circuitManager.saveData(stream);

        //store only this years data
        int year = QDate::currentDate().year();

        stream << seasons.contains(year);
        if (seasons.contains(year))
        {
            seasons[year].saveData(stream);
        }
    }
}

const SeasonData *DataManager::getCurrentSeasonData() const
{
    int season = QDate::currentDate().year();
    return getSeasonData(season);
}

const SeasonData *DataManager::getSeasonData(int season) const
{
    QHash<int, SeasonData>::ConstIterator iter = seasons.find(season);

    if (iter != seasons.end())
        return &iter.value();

    return NULL;
}

void DataManager::addDriver(const DriverData &dd)
{
    drivers[dd.getDriverID()] = dd;
}

void DataManager::addTeam(const TeamData &td)
{
    teams[td.getConstructorID()] = td;
}

const DriverData *DataManager::getDriver(QString driverId) const
{
    QHash<QString, DriverData>::ConstIterator iter = drivers.find(driverId);

    if (iter != drivers.end())
        return &iter.value();

    return NULL;
}

const TeamData *DataManager::getTeam(QString constructorId) const
{
    QHash<QString, TeamData>::ConstIterator iter = teams.find(constructorId);

    if (iter != teams.end())
        return &iter.value();

    return NULL;
}

const EventData *DataManager::getNextRace() const
{
    int year = QDate::currentDate().year();

    if (seasons.contains(year) && seasons[year].getScheduleObtained() == true)
    {
        QHash<int, EventData>::ConstIterator iter = seasons[year].getEvents().begin();

        const EventData *ed = NULL;
        QDate today = QDate::currentDate();
        while (iter != seasons[year].getEvents().end())
        {
            if (iter.value().getRaceDate().date() > today)
            {
                if ((ed == NULL) || (iter.value().getRaceDate() < ed->getRaceDate()))
                {
                    ed = &iter.value();
                }
            }
            ++iter;
        }

        return ed;
    }
    return NULL;
}

void DataManager::getNextRaceData()
{
    const EventData *ed = getNextRace();

    if (ed != NULL)
    {
        const CircuitData *cd = circuitManager.getCircuit(ed->getCircuitID());

        if (cd != NULL)
        {
            emit nextRaceDataObtained(*ed, *cd);
            return;
        }
    }
    connectionManager->obtainNextRaceData();
}

void DataManager::getRaceSchedule()
{
    int year = QDate::currentDate().year();

    getRaceSchedule(year);
}

void DataManager::getRaceSchedule(int year)
{
    if (seasons.contains(year) && seasons[year].getScheduleObtained())
    {
        emit raceScheduleObtained(&seasons[year]);
    }
    else
    {
        connectionManager->obtainRaceSchedule(year);
    }
}

void DataManager::getDriverStandings()
{
    int year = QDate::currentDate().year();
    if (seasons.contains(year) && !seasons[year].getDriverSeasonData()->isEmpty() &&
            (seasons[year].getDriversStandingsDate() == QDate::currentDate()))
        emit driverStandingsObtained(&seasons[year]);

    else
        connectionManager->obtainDriversCurrentStandings();

}

void DataManager::getDriverStandings(int year)
{    
    if (seasons.contains(year) && !seasons[year].getDriverSeasonData()->isEmpty() &&
            (seasons[year].getDriversStandingsDate() == QDate::currentDate()))
    {
        emit driverStandingsObtained(&seasons[year]);
    }

    else
        connectionManager->obtainDriversStandings(year);
}

void DataManager::getConstructorStandings()
{
    int year = QDate::currentDate().year();
    if (seasons.contains(year) && !seasons[year].getTeamSeasonData()->isEmpty() &&
          (seasons[year].getConstructorsStandingsDate() == QDate::currentDate()))
        emit constructorStandingsObtained(&seasons[year]);

    else
        connectionManager->obtainConstructorCurrentStandings();
}

void DataManager::getConstructorStandings(int year)
{
    if (seasons.contains(year) && !seasons[year].getTeamSeasonData()->isEmpty() &&
          (seasons[year].getConstructorsStandingsDate() == QDate::currentDate()))
        emit constructorStandingsObtained(&seasons[year]);

    else
        connectionManager->obtainConstructorStandings(year);
}

void DataManager::getLatestRaceResults()
{
    QDateTime today = QDateTime::currentDateTime().toLocalTime();

    if (!seasons.contains(today.date().year()) || !seasons[today.date().year()].getScheduleObtained())
    {
        connectionManager->obtainLatestRaceResults();
        latestResults.obtainingRaceResults = true;
    }

    else
    {
        QHash<int, EventData>::ConstIterator iter = seasons[today.date().year()].getEvents().begin();

        const EventData *ed = NULL;
        while (iter != seasons[today.date().year()].getEvents().end())
        {
            QDateTime eventQualiDate = iter.value().getRaceDate().addDays(-1);

            if (eventQualiDate <= today)
            {
                if ((ed == NULL) || (eventQualiDate < ed->getRaceDate().addDays(-1)))
                {
                    ed = &iter.value();
                }
            }
            ++iter;
        }

        if (ed != NULL)
        {
            emit raceResultsObtained(&seasons[ed->getSeason()], ed->getRound());
        }

        else if (seasons.contains(latestResults.season) &&
                 (seasons[latestResults.season].getEvent(latestResults.round) != NULL))
        {
            emit raceResultsObtained(&seasons[latestResults.season], latestResults.round);
        }

        else
        {
            connectionManager->obtainLatestRaceResults();
            latestResults.obtainingRaceResults = true;
        }
    }
}

void DataManager::getLatestQualiResults()
{
    QDateTime today = QDateTime::currentDateTime().toLocalTime();

    if (!seasons.contains(today.date().year()) || !seasons[today.date().year()].getScheduleObtained())
    {
        connectionManager->obtainLatestQualiResults();
        latestResults.obtainingQualiResults = true;
    }

    else
    {
        QHash<int, EventData>::ConstIterator iter = seasons[today.date().year()].getEvents().begin();

        const EventData *ed = NULL;
        while (iter != seasons[today.date().year()].getEvents().end())
        {
            QDateTime eventQualiDate = iter.value().getRaceDate().addDays(-1);

            if (eventQualiDate <= today)
            {
                if ((ed == NULL) || (eventQualiDate < ed->getRaceDate().addDays(-1)))
                {
                    ed = &iter.value();
                }
            }
            ++iter;
        }

        if (ed != NULL)
            emit qualiResultsObtained(&seasons[ed->getSeason()], ed->getRound());

        else if (seasons.contains(latestResults.season) &&
                 (seasons[latestResults.season].getEvent(latestResults.round) != NULL))
            emit qualiResultsObtained(&seasons[latestResults.season], latestResults.round);

        else
        {
            connectionManager->obtainLatestQualiResults();
            latestResults.obtainingQualiResults = true;
        }
    }
}

void DataManager::getRaceResults(int season, int round)
{
    if (seasons.contains(season) &&
        (seasons[season].getEvent(round) != NULL) &&
        seasons[season].getEvent(round)->getRaceResultsObtained())
        emit raceResultsObtained(&seasons[season], round);

    else
        connectionManager->obtainRaceResults(season, round);
}

void DataManager::getQualiResults(int season, int round)
{
    if (seasons.contains(season) &&
        (seasons[season].getEvent(round) != NULL) &&
        seasons[season].getEvent(round)->getQualiResultsObtained())
        emit qualiResultsObtained(&seasons[season], round);

    else
        connectionManager->obtainQualiResults(season, round);
}

void DataManager::getLapTimes(QString driverId, int season, int round)
{
    if (seasons.contains(season) &&
        (seasons[season].getEvent(round) != NULL) &&
        (seasons[season].getEvent(round)->getRaceData(driverId) != NULL) &&
        (!seasons[season].getEvent(round)->getRaceData(driverId)->lapData.isEmpty()) &&
        (!seasons[season].getEvent(round)->getRaceData(driverId)->pitStopData.isEmpty()))
        emit lapTimesObtained(seasons[season].getEvent(round), driverId);

    else
    {
        connectionManager->obtainLapTimes(driverId, season, round);
        connectionManager->obtainPitStops(driverId, season, round);
    }
}

void DataManager::getDriverResults(QString driverId, int season)
{
    if (seasons.contains(season) &&
        (seasons[season].getDriverSeasonData(driverId) != NULL) &&
        (seasons[season].getDriverSeasonData(driverId)->getSeasonResultsObtained() == true))
        emit driverResultsObtained(&seasons[season], driverId);

    else
        connectionManager->obtainDriverResults(driverId, season);
}

void DataManager::getConstructorResults(QString constructorId, int season)
{
    if (seasons.contains(season) &&
        (seasons[season].getTeamSeasonData(constructorId) != NULL) &&
        (seasons[season].getTeamSeasonData(constructorId)->getSeasonResultsObtained() == true))
        emit constructorResultsObtained(&seasons[season], constructorId);
    else
        connectionManager->obtainConstructorResults(constructorId, season);
}

void DataManager::getWeatherForecast()
{
    const EventData *ed = getNextRace();
    QString city = weatherForecast.getCity();

    if (ed != NULL)
    {
        const CircuitData *cd = circuitManager.getCircuit(ed->getCircuitID());

        if (cd != NULL)
            city = cd->getLocation().locality;

        if ((weatherForecast.getSeason() != ed->getSeason()) ||
            (weatherForecast.getRound() != ed->getRound()) ||
            (weatherForecast.getForecastDate() != QDate::currentDate()))
        {            

            if (cd != NULL)
            {
                connectionManager->obtainWeatherForecast(cd->getLocation().loc_lat, cd->getLocation().loc_long);
            }
        }
        else
            emit weatherForecastObtained(weatherForecast, weatherForecast.getCityId(), city);

    }
    else if (weatherForecast.getForecastDate() == QDate::currentDate())
        emit weatherForecastObtained(weatherForecast, weatherForecast.getCityId(), city);
}

void DataManager::onNextRaceDataObtained(const EventData &ed, const CircuitData &cd)
{
    circuitManager.addCircuit(cd);    

    emit nextRaceDataObtained(ed, cd);

    //get fresh weather forecast
    connectionManager->obtainWeatherForecast(cd.getLocation().loc_lat, cd.getLocation().loc_long);
}

void DataManager::onDriverStandingsObtained(const QVector<DriverData> &dd, const QVector<DriverSeasonData> &ds, const QVector<TeamData> &td)
{
    if (!ds.isEmpty() && !dd.isEmpty())
    {
        for (int i = 0; i < dd.size(); ++i)
            addDriver(dd[i]);

        for (int i = 0; i < td.size(); ++i)
            addTeam(td[i]);

        int season = ds.first().getSeason();
        seasons[season].setSeason(season);

        seasons[season].setDriverSeasonData(ds);

        seasons[season].setDriversStandingsDate(QDate::currentDate());

        emit driverStandingsObtained(&seasons[season]);
    }
    else
        emit driverStandingsNotAvailable();
}

void DataManager::onConstructorStandingsObtained(const QVector<TeamData> &td, const QVector<TeamSeasonData> &ts)
{
    if (!td.isEmpty() && !ts.isEmpty())
    {
        for (int i = 0; i < td.size(); ++i)
            addTeam(td[i]);

        int season = ts.first().getSeason();
        seasons[season].setSeason(season);

        seasons[season].setTeamSeasonData(ts);

        seasons[season].setConstructorsStandingsDate(QDate::currentDate());

        emit constructorStandingsObtained(&seasons[season]);
    }
    else
        emit constructorStandingsNotAvailable();
}

void DataManager::onRaceScheduleObtained(const QVector<EventData> &ed, const QVector<CircuitData> &cd)
{
    for (int j = 0; j < cd.size(); ++j)
        circuitManager.addCircuit(cd[j]);

    if (!ed.isEmpty())
    {        
        int season = ed.first().getSeason();

        seasons[season].setSeason(season);
        seasons[season].setRounds(ed.size());
        seasons[season].setInfoURL(QString("http://en.wikipedia.org/wiki/%1_Formula_One_season").arg(season));

        for (int j = 0; j < ed.size(); ++j)
        {
            seasons[season].addEvent(ed[j]);
        }

        seasons[season].setScheduleObtained(true);

        emit raceScheduleObtained(&seasons[season]);
    }
}

void DataManager::onRaceResultsObtained(const EventData &ed, const CircuitData &cd, const QVector<DriverData> &dd, const QVector<DriverSeasonData> &ds, const QVector<TeamData> &td)
{
    if ((ed.getSeason() == 0) || (ed.getRound() == 0))
        return;

    circuitManager.addCircuit(cd);

    int season = ed.getSeason();

    seasons[season].setSeason(season);

    seasons[season].addEvent(ed);

    for (int i = 0; i < dd.size(); ++i)
        addDriver(dd[i]);

    for (int i = 0; i < td.size(); ++i)
        addTeam(td[i]);

    for (int i = 0; i < ds.size(); ++i)
        seasons[season].addDriverSeasonData(ds[i]);

    if (latestResults.obtainingRaceResults)
    {
        latestResults.obtainingRaceResults = false;
        latestResults.season = season;
        latestResults.round = ed.getRound();
    }

    emit raceResultsObtained(&seasons[season], ed.getRound());
}

void DataManager::onQualiResultsObtained(const EventData &ed, const CircuitData &cd, const QVector<DriverData> &dd, const QVector<DriverSeasonData> &ds, const QVector<TeamData> &td)
{
    if ((ed.getSeason() == 0) || (ed.getRound() == 0))
        return;

    circuitManager.addCircuit(cd);

    int season = ed.getSeason();

    seasons[season].setSeason(season);

    seasons[season].addEvent(ed);

    for (int i = 0; i < dd.size(); ++i)
        addDriver(dd[i]);

    for (int i = 0; i < td.size(); ++i)
        addTeam(td[i]);

    for (int i = 0; i < ds.size(); ++i)
        seasons[season].addDriverSeasonData(ds[i]);

    if (latestResults.obtainingQualiResults)
    {
        latestResults.obtainingQualiResults = false;
        latestResults.season = season;
        latestResults.round = ed.getRound();
    }

    emit qualiResultsObtained(&seasons[season], ed.getRound());
}

void DataManager::onLapTimesObtained(const QVector<LapData> &ld, int season, int round, QString driverId)
{
    EventData *ed = seasons[season].getEvent(round);

    if (ed != NULL)
    {
        DriverRaceData *dr = ed->getRaceData(driverId);

        if (dr != NULL)
            dr->lapData = ld;

        else
        {
            DriverRaceData dr;
            dr.driverID = driverId;
            dr.lapData = ld;

            ed->addRaceData(dr);
        }

        emit lapTimesObtained(ed, driverId);
    }    
}

void DataManager::onPitStopsObtained(const QVector<PitStopData> &pd, int season, int round, QString driverId)
{
    EventData *ed = seasons[season].getEvent(round);

    if (ed != NULL)
    {
        DriverRaceData *dr = ed->getRaceData(driverId);

        if (dr != NULL)
            dr->pitStopData = pd;

        else
        {
            DriverRaceData dr;
            dr.driverID = driverId;
            dr.pitStopData = pd;

            ed->addRaceData(dr);
        }

        emit lapTimesObtained(ed, driverId);
    }
}

void DataManager::onDriverResultsObtained(const QVector<EventData> &ed, const QVector<CircuitData> &cd, const QVector<TeamData> &teams, QString driverId)
{
    if (!ed.isEmpty())
    {
        int season = ed.first().getSeason();

        seasons[season].setSeason(season);

        for (int i = 0; i < teams.size(); ++i)
        {
            TeamSeasonData ts;
            ts.setConstructorId(teams[i].getConstructorID());
            ts.setSeason(season);

            addTeam(teams[i]);
            seasons[season].addTeamSeasonData(ts);
        }

        for (int i = 0; i < ed.size(); ++i)
            seasons[season].addEvent(ed[i]);

        for (int i = 0; i < cd.size(); ++i)
            circuitManager.addCircuit(cd[i]);

        DriverSeasonData *ds = seasons[season].getDriverSeasonData(driverId);

        if (ds != NULL)
            ds->setSeasonResultsObtained(true);

        else
        {
            DriverSeasonData ds;
            ds.setDriverId(driverId);
            ds.setSeason(season);
            ds.setSeasonResultsObtained(true);

            seasons[season].addDriverSeasonData(ds);
        }

        emit driverResultsObtained(&seasons[season], driverId);
    }
}

void DataManager::onConstructorResultsObtained(const QVector<EventData> &ed, const QVector<CircuitData> &cd, const QVector<DriverData> &drivers, QString constructorId)
{
    if (!ed.isEmpty())
    {        
        int season = ed.first().getSeason();

        seasons[season].setSeason(season);

        for (int i = 0; i < drivers.size(); ++i)
        {
            DriverSeasonData ds;
            ds.addConstructorId(constructorId);
            ds.setDriverId(drivers[i].getDriverID());
            ds.setSeason(season);

            addDriver(drivers[i]);
            seasons[season].addDriverSeasonData(ds);
        }

        for (int i = 0; i < ed.size(); ++i)
            seasons[season].addEvent(ed[i]);

        for (int i = 0; i < cd.size(); ++i)
            circuitManager.addCircuit(cd[i]);

        TeamSeasonData *ts = seasons[season].getTeamSeasonData(constructorId);

        if (ts != NULL)
            ts->setSeasonResultsObtained(true);

        else
        {
            TeamSeasonData ts;
            ts.setConstructorId(constructorId);
            ts.setSeasonResultsObtained(true);
            ts.setSeason(season);

            seasons[season].addTeamSeasonData(ts);
        }


        emit constructorResultsObtained(&seasons[season], constructorId);
    }
}

void DataManager::onWeatherForecastObtained(const WeatherForecastData &wd)
{
    const EventData *ed = getNextRace();

    weatherForecast = wd;
    QString city = wd.getCity();
    if (ed != NULL)
    {
        weatherForecast.setSeason(ed->getSeason());
        weatherForecast.setRound(ed->getRound());
        const CircuitData *cd = circuitManager.getCircuit(ed->getCircuitID());

        if (cd != NULL)
            city = cd->getLocation().locality;
    }

    emit weatherForecastObtained(weatherForecast, weatherForecast.getCityId(), city);
}

