/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DRIVERDATA_H
#define DRIVERDATA_H

#include <QDate>
#include <QVector>


class DriverData
{
public:
    DriverData();

    QString getDriverID() const;
    void setDriverID(const QString &value);

    QString getDriverName() const;
    void setDriverName(const QString &value);

    QDate getBirthDate() const;
    void setBirthDate(const QDate &value);

    QString getBiographyURL() const;
    void setBiographyURL(const QString &value);

    QString getNationality() const;
    void setNationality(const QString &value);

    bool operator < (const DriverData &dd) const
    {
        return driverID < dd.driverID;
    }

private:

    QString driverID;
    QString driverName;
    QDate birthDate;
    QString biographyURL;
    QString nationality;
};

#endif // DRIVERDATA_H
