/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "circuitmanager.h"

CircuitManager::CircuitManager()
{
}

void CircuitManager::loadData(QDataStream &stream)
{
    int size;
    stream >> size;

    for (int i = 0; i < size; ++i)
    {
        CircuitData cd;
        CircuitLocation cl;
        QString buf;

        stream >> buf; cd.setCircuitID(buf);
        stream >> buf; cd.setName(buf);
        stream >> buf; cd.setInfoURL(buf);

        stream >> cl.loc_lat;
        stream >> cl.loc_long;
        stream >> cl.locality;
        stream >> cl.country;

        cd.setLocation(cl);

        addCircuit(cd);
    }
}

void CircuitManager::saveData(QDataStream &stream) const
{
    QHash<QString, CircuitData>::ConstIterator iter = circuits.begin();

    stream << circuits.size();
    while (iter != circuits.end())
    {
        stream << iter.value().getCircuitID();
        stream << iter.value().getName();
        stream << iter.value().getInfoURL();
        stream << iter.value().getLocation().loc_lat;
        stream << iter.value().getLocation().loc_long;
        stream << iter.value().getLocation().locality;
        stream << iter.value().getLocation().country;

        ++iter;
    }
}

const CircuitData *CircuitManager::getCircuit(QString circuitID)
{
    if (circuits.contains(circuitID))
        return &circuits[circuitID];

    return NULL;
}

void CircuitManager::addCircuit(const CircuitData &cd)
{
    circuits[cd.getCircuitID()] = cd;
}
