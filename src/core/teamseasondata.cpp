/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "teamseasondata.h"

TeamSeasonData::TeamSeasonData() : position(0), wins(0), points(0), seasonResultsObtained(false)
{
}
int TeamSeasonData::getSeason() const
{
    return season;
}

void TeamSeasonData::setSeason(int value)
{
    season = value;
}
QString TeamSeasonData::getConstructorId() const
{
    return constructorId;
}

void TeamSeasonData::setConstructorId(const QString &value)
{
    constructorId = value;
}
double TeamSeasonData::getPoints() const
{
    return points;
}

void TeamSeasonData::setPoints(double value)
{
    points = value;
}
int TeamSeasonData::getWins() const
{
    return wins;
}

void TeamSeasonData::setWins(int value)
{
    wins = value;
}

TeamSeasonData &TeamSeasonData::operator=(const TeamSeasonData &tsd)
{
    if (this == &tsd)
        return *this;

    if (tsd.position != 0)
    {
        position = tsd.position;
        positionText = tsd.positionText;
    }

    season = tsd.season;
    constructorId = tsd.constructorId;
    seasonResultsObtained =  tsd.seasonResultsObtained;

    if (tsd.points > points)
        points = tsd.points;

    if (tsd.wins > wins)
        wins = tsd.wins;

    return *this;
}
int TeamSeasonData::getPosition() const
{
    return position;
}

void TeamSeasonData::setPosition(int value)
{
    position = value;
}
bool TeamSeasonData::getSeasonResultsObtained() const
{
    return seasonResultsObtained;
}

void TeamSeasonData::setSeasonResultsObtained(bool value)
{
    seasonResultsObtained = value;
}
QString TeamSeasonData::getPositionText() const
{
    return positionText;
}

void TeamSeasonData::setPositionText(const QString &value)
{
    positionText = value;
}







