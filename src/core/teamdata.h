/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef TEAMDATA_H
#define TEAMDATA_H

#include <QString>

class TeamData
{
public:
    TeamData();
    TeamData(QString ID, QString name, QString nat, QString url);

    QString getConstructorID() const;
    void setConstructorID(const QString &value);

    QString getTeamName() const;
    void setTeamName(const QString &value);

    QString getNationality() const;
    void setNationality(const QString &value);

    QString getInfoURL() const;
    void setInfoURL(const QString &value);

private:
    QString constructorID;
    QString teamName;
    QString nationality;
    QString infoURL;
};

#endif // TEAMDATA_H
