/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QHash>
#include "circuitmanager.h"
#include "seasondata.h"
#include "connectionapimanager.h"

class DataManager : public QObject
{
    Q_OBJECT
public:    

    static DataManager &getInstance()
    {
        static DataManager dm;

        return dm;
    }

    ~DataManager();    

    const SeasonData *getCurrentSeasonData() const;
    const SeasonData *getSeasonData(int season) const;

    void addDriver(const DriverData &dd);
    void addTeam(const TeamData &td);

    const DriverData *getDriver(QString driverId) const;
    const TeamData *getTeam(QString constructorId) const;

    CircuitManager &getCircuitManager()
    {
        return circuitManager;
    }

    const EventData *getNextRace() const;

public slots:

    void loadData(QString fileName);
    void saveData();

    void getNextRaceData();
    void getRaceSchedule();
    void getRaceSchedule(int year);    
    void getDriverStandings();
    void getDriverStandings(int year);
    void getConstructorStandings();
    void getConstructorStandings(int year);
    void getLatestRaceResults();
    void getLatestQualiResults();
    void getRaceResults(int season, int round);
    void getQualiResults(int season, int round);
    void getLapTimes(QString driverId, int season, int round);
    void getDriverResults(QString driverId, int season);
    void getConstructorResults(QString constructorId, int season);
    void getWeatherForecast();


    void onNextRaceDataObtained(const EventData &ed, const CircuitData &cd);
    void onDriverStandingsObtained(const QVector<DriverData> &dd, const QVector<DriverSeasonData> &ds, const QVector<TeamData> &td);
    void onConstructorStandingsObtained(const QVector<TeamData> &td, const QVector<TeamSeasonData> &ts);
    void onRaceScheduleObtained(const QVector<EventData> &, const QVector<CircuitData> &);
    void onRaceResultsObtained(const EventData &ed, const CircuitData &cd, const QVector<DriverData> &dd, const QVector<DriverSeasonData> &ds, const QVector<TeamData> &td);
    void onQualiResultsObtained(const EventData &ed, const CircuitData &cd, const QVector<DriverData> &dd, const QVector<DriverSeasonData> &ds, const QVector<TeamData> &td);
    void onLapTimesObtained(const QVector<LapData> &ld, int season, int round, QString driverId);
    void onPitStopsObtained(const QVector<PitStopData> &pd, int season, int round, QString driverId);
    void onDriverResultsObtained(const QVector<EventData> &ed, const QVector<CircuitData> &cd, const QVector<TeamData> &td, QString driverId);
    void onConstructorResultsObtained(const QVector<EventData> &ed, const QVector<CircuitData> &cd, const QVector<DriverData> &drivers, QString constructorId);
    void onWeatherForecastObtained(const WeatherForecastData &wd);

    QString appVersion() const
    {
        return "1.0.0";
    }

signals:
    void nextRaceDataObtained(const EventData &ed, const CircuitData &cd);
    void driverStandingsObtained(const SeasonData *sd);
    void driverStandingsNotAvailable();
    void constructorStandingsObtained(const SeasonData *sd);
    void constructorStandingsNotAvailable();
    void raceScheduleObtained(const SeasonData *sd);
    void raceResultsObtained(const SeasonData *sd, int round);
    void qualiResultsObtained(const SeasonData *sd, int round);
    void lapTimesObtained(const EventData *ed, QString driverId);
    void pitStopsObtained(const SeasonData *sd);
    void driverResultsObtained(const SeasonData *sd, QString driverId);
    void constructorResultsObtained(const SeasonData *sd, QString constructorId);
    void weatherForecastObtained(const WeatherForecastData &wd, QString cityId, QString city);
    void connectionError(QNetworkReply::NetworkError);

private:
    DataManager();

    QString dataFilePath;

    CircuitManager circuitManager;
    QHash<int, SeasonData> seasons;
    QHash<QString, DriverData> drivers;
    QHash<QString, TeamData> teams;

    ConnectionAPIManager *connectionManager;

    struct LatestResultsData {

        int season;
        int round;

        bool obtainingRaceResults;
        bool obtainingQualiResults;

        LatestResultsData() : season(0), round(0), obtainingRaceResults(false), obtainingQualiResults(false)
        { }
    };

    LatestResultsData latestResults;

    WeatherForecastData weatherForecast;
};

#endif // DATAMANAGER_H
