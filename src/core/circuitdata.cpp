/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "circuitdata.h"

CircuitData::CircuitData()
{
}

QString CircuitData::getCircuitID() const
{
    return circuitID;
}

void CircuitData::setCircuitID(const QString &value)
{
    circuitID = value;
}
QString CircuitData::getInfoURL() const
{
    return infoURL;
}

void CircuitData::setInfoURL(const QString &value)
{
    infoURL = value;
}
QString CircuitData::getName() const
{
    return name;
}

void CircuitData::setName(const QString &value)
{
    name = value;
}
CircuitLocation CircuitData::getLocation() const
{
    return location;
}

void CircuitData::setLocation(const CircuitLocation &value)
{
    location = value;
}



