/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "eventdata.h"

#include "datamanager.h"

EventData::EventData() : season(0), round(0), raceResultsObtained(false), qualiResultsObtained(false)
{
}

void EventData::loadData(QDataStream &stream, int season)
{
    this->season = season;
    stream >> round;
    stream >> raceName;
    stream >> infoURL;
    stream >> raceDate;
    stream >> circuitID;
    stream >> raceResultsObtained;
    stream >> qualiResultsObtained;

    int size;
    stream >> size;

    for (int i = 0; i < size; ++i)
    {
        DriverRaceData dr;
        dr.loadData(stream);

        dr.round = round;
        dr.season = season;

        raceData[dr.driverID] = dr;
    }

    stream >> size;

    for (int i = 0; i < size; ++i)
    {
        DriverQualiData dq;
        stream >> dq.driverID;
        stream >> dq.constructorID;
        stream >> dq.position;
        stream >> dq.number;

        for (int j = 0; j < 3; ++j)
        {
            QString buf;
            stream >> buf; dq.time[j] = LapTime(buf);
        }
        qualiData[dq.driverID] = dq;
    }
}

void EventData::saveData(QDataStream &stream) const
{
    stream << round;
    stream << raceName;
    stream << infoURL;
    stream << raceDate;
    stream << circuitID;
    stream << raceResultsObtained;
    stream << qualiResultsObtained;

    stream << raceData.size();
    QHash<QString, DriverRaceData>::ConstIterator iter = raceData.begin();
    while (iter != raceData.end())
    {
        iter.value().saveData(stream);
        ++iter;
    }

    stream << qualiData.size();
    QHash<QString, DriverQualiData>::ConstIterator qIter = qualiData.begin();
    while (qIter != qualiData.end())
    {
        stream << qIter.value().driverID;
        stream << qIter.value().constructorID;
        stream << qIter.value().position;
        stream << qIter.value().number;

        for (int i = 0; i < 3; ++i)
            stream << qIter.value().time[i].toString();

        ++qIter;
    }
}

int EventData::getSeason() const
{
    return season;
}

void EventData::setSeason(int value)
{
    season = value;
}
int EventData::getRound() const
{
    return round;
}

void EventData::setRound(int value)
{
    round = value;
}
QString EventData::getInfoURL() const
{
    return infoURL;
}

void EventData::setInfoURL(const QString &value)
{
    infoURL = value;
}

QString EventData::getCircuitID() const
{
    return circuitID;
}

void EventData::setCircuitID(const QString &value)
{
    circuitID = value;
}
QDateTime EventData::getRaceDate() const
{
    return raceDate;
}

void EventData::setRaceDate(const QDateTime &value)
{
    raceDate = value;
}
QString EventData::getRaceName() const
{
    return raceName;
}

void EventData::setRaceName(const QString &value)
{
    raceName = value;
}

const QHash<QString, DriverRaceData> *EventData::getRaceData() const
{
    return &raceData;
}

void EventData::setRaceData(const QVector<DriverRaceData> &value)
{
    for (int i = 0; i < value.size(); ++i)
        raceData[value[i].driverID] = value[i];
}

void EventData::addRaceData(const DriverRaceData &drd)
{
    raceData[drd.driverID] = drd;
}

const DriverRaceData *EventData::getRaceData(QString driverId) const
{
    QHash<QString, DriverRaceData>::ConstIterator iter = raceData.find(driverId);

    if (iter != raceData.end())
        return &iter.value();

    return NULL;
}

DriverRaceData *EventData::getRaceData(QString driverId)
{
    QHash<QString, DriverRaceData>::Iterator iter = raceData.find(driverId);

    if (iter != raceData.end())
        return &iter.value();

    return NULL;
}

const QVector<const DriverRaceData *> EventData::getRaceDataFromConstructor(QString constructorId) const
{
    QVector<const DriverRaceData *> drivers;
    QHash<QString, DriverRaceData>::ConstIterator iter = raceData.begin();

    while (iter != raceData.end())
    {
        if (iter.value().constructorID == constructorId)
            drivers.append(&iter.value());

        ++iter;
    }
    return drivers;
}

const QHash<QString, DriverQualiData> *EventData::getQualiData() const
{
    return &qualiData;
}

void EventData::setQualiData(const QVector<DriverQualiData> &value)
{
    for (int i = 0; i < value.size(); ++i)
        qualiData[value[i].driverID] = value[i];
}

void EventData::addQualiData(const DriverQualiData &dqd)
{
    qualiData[dqd.driverID] = dqd;
}

const DriverQualiData *EventData::getQualiData(QString driverId) const
{
    QHash<QString, DriverQualiData>::ConstIterator iter = qualiData.find(driverId);

    if (iter != qualiData.end())
        return &iter.value();

    return NULL;
}

DriverQualiData *EventData::getQualiData(QString driverId)
{
    QHash<QString, DriverQualiData>::Iterator iter = qualiData.find(driverId);

    if (iter != qualiData.end())
        return &iter.value();

    return NULL;
}

EventData &EventData::operator=(const EventData &event)
{
    if (this == &event)
        return *this;

    season = event.season;
    round = event.round;

    if (!event.raceName.isEmpty())
        raceName = event.raceName;

    if (!event.infoURL.isEmpty())
        infoURL = event.infoURL;

    if (!event.raceDate.isNull())
        raceDate = event.raceDate;

    if (!event.circuitID.isEmpty())
        circuitID = event.circuitID;

    if (event.raceResultsObtained)
        raceResultsObtained = event.raceResultsObtained;

    if (event.qualiResultsObtained)
        qualiResultsObtained = event.qualiResultsObtained;

    QHash<QString, DriverRaceData>::ConstIterator iter = event.raceData.begin();

    while (iter != event.raceData.end())
    {
        raceData[iter.value().driverID] = iter.value();
        ++iter;
    }

    QHash<QString, DriverQualiData>::ConstIterator qiter = event.qualiData.begin();

    while (qiter != event.qualiData.end())
    {
        qualiData[qiter.value().driverID] = qiter.value();
        ++qiter;
    }

    return *this;
}
bool EventData::getRaceResultsObtained() const
{
    return raceResultsObtained;
}

void EventData::setRaceResultsObtained(bool value)
{
    raceResultsObtained = value;
}
bool EventData::getQualiResultsObtained() const
{
    return qualiResultsObtained;
}

void EventData::setQualiResultsObtained(bool value)
{
    qualiResultsObtained = value;
}

void EventData::getFastestLap(QString &driver, QString &time, int &lapNo) const
{
    QHash<QString, DriverRaceData>::ConstIterator iter = getRaceData()->begin();

    LapTime lt(10, 0, 0);
    while (iter != getRaceData()->end())
    {
        if (lt > iter.value().fastestLap)
        {
            lt = iter.value().fastestLap;

            const DriverData *dd = DataManager::getInstance().getDriver(iter.value().driverID);

            if (dd != NULL)
                driver = dd->getDriverName();

            time = lt.toString();
            lapNo = iter.value().fastestLapNumber;
        }
        ++iter;
    }
}




void DriverRaceData::loadData(QDataStream &stream)
{
    stream >> driverID;
    stream >> constructorID;
    stream >> number;
    stream >> position;
    stream >> positionText;
    stream >> points;
    stream >> grid;
    stream >> laps;
    stream >> status;
    stream >> raceTime;
    stream >> raceTimeMillis;

    QString buf;
    stream >> buf; fastestLap = LapTime(buf);
    stream >> fastestLapNumber;
    stream >> avgSpeed;
    stream >> avgSpeedUnits;

    int size;
    stream >> size;

    for (int i = 0; i < size; ++i)
    {
        LapData ld;
        int ibuf;
        QString buf;

        stream >> ibuf; ld.setLapNumber(ibuf);
        stream >> buf; ld.setLapTime(LapTime(buf));
        stream >> ibuf; ld.setPosition(ibuf);

        ld.setDriverID(driverID);

        lapData.append(ld);
    }

    stream >> size;
    for (int i = 0; i < size; ++i)
    {
        PitStopData pd;
        QString buf;

        stream >> pd.lap;
        stream >> pd.raceTime;
        stream >> buf; pd.duration = LapTime(buf);

        pd.driverID = driverID;

        pitStopData.append(pd);
    }
}

void DriverRaceData::saveData(QDataStream &stream) const
{
    stream << driverID;
    stream << constructorID;
    stream << number;
    stream << position;
    stream << positionText;
    stream << points;
    stream << grid;
    stream << laps;
    stream << status;
    stream << raceTime;
    stream << raceTimeMillis;

    stream << fastestLap.toString();
    stream << fastestLapNumber;
    stream << avgSpeed;
    stream << avgSpeedUnits;

    stream << lapData.size();

    for (int i = 0; i < lapData.size(); ++i)
    {
        stream << lapData[i].getLapNumber();
        stream << lapData[i].getLapTime().toString();
        stream << lapData[i].getPosition();
    }

    stream << pitStopData.size();
    for (int i = 0; i < pitStopData.size(); ++i)
    {
        stream << pitStopData[i].lap;
        stream << pitStopData[i].raceTime;
        stream << pitStopData[i].duration.toString();
    }
}

DriverRaceData &DriverRaceData::operator=(const DriverRaceData &dr)
{
    if (this == &dr)
        return *this;

    season = dr.season;
    round = dr.round;
    number = dr.number;

    driverID = dr.driverID;
    constructorID = dr.constructorID;
    position = dr.position;
    positionText = dr.positionText;
    points = dr.points;
    grid = dr.grid;
    laps = dr.laps;
    status = dr.status;

    raceTime = dr.raceTime;
    raceTimeMillis = dr.raceTimeMillis;

    fastestLap = dr.fastestLap;
    fastestLapNumber = dr.fastestLapNumber;

    avgSpeed = dr.avgSpeed;
    avgSpeedUnits = dr.avgSpeedUnits;

    if (lapData.size() < dr.lapData.size())
        lapData = dr.lapData;

    if (pitStopData.size() < dr.pitStopData.size())
        pitStopData = dr.pitStopData;

    return *this;
}
