/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef LAPDATA_H
#define LAPDATA_H

#include <QMetaType>
#include <QString>
#include <cmath>

class LapTime
{
public:
    LapTime() { }
    ~LapTime() { }
    LapTime(const QString &t) { time = t; }
    LapTime(const LapTime &t) { time = t.time; }
    LapTime(int msecs);
    LapTime(int m, int s, int ms)
    {
        QString strM = QString::number(m);
        QString strS = QString::number(s);
        QString strMS = QString::number(ms);

        time = strM + (s < 10 ? ":0": ":") + strS + (ms < 100 ? (ms < 10 ? ".00" : ".0") : ".")  + strMS;
    }

    const QString &toString() const { return time; }
    int toMsecs() const;
    QString toSecs() const;
    double toDouble() const
    {
        if (isValid())
            return (double)(toMsecs() / 1000.0);

        return 0.0;
    }

    LapTime calc107p() const
    {
        double msecs = toMsecs();
        msecs = msecs * 1.07;

        return LapTime((int)(round(msecs)));
    }

    bool isValid() const;

    bool operator < (const LapTime &) const;
    bool operator <= (const LapTime &) const;

    bool operator > (const LapTime &) const;
    bool operator >= (const LapTime &) const;
    operator QString() const
    {
        return toString();
    }
    bool operator == (const LapTime &) const;
    bool operator != (const LapTime &lt) const
    {
        return !(*this == lt);
    }
    LapTime operator - (const LapTime&) const;
    LapTime operator + (const LapTime&) const;

private:
    QString time;
};

Q_DECLARE_METATYPE(LapTime)

class LapData
{
public:
    LapData();
    LapData(int no, LapTime lt, int pos, QString driver);

    int getPosition() const;
    void setPosition(int value);

    QString getDriverID() const;
    void setDriverID(const QString &value);

    LapTime getLapTime() const;
    void setLapTime(const LapTime &value);

    int getLapNumber() const;
    void setLapNumber(int value);

private:
    int lapNumber;
    LapTime lapTime;
    int position;
    QString driverID;
};

#endif // LAPDATA_H
