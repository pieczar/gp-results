/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef CIRCUITDATA_H
#define CIRCUITDATA_H

#include <QMetaType>
#include <QString>

struct CircuitLocation
{
    double loc_lat, loc_long;
    QString locality;
    QString country;
};

class CircuitData
{
public:
    CircuitData();
    CircuitData(QString id, QString url, QString n, CircuitLocation loc) :
        circuitID(id), infoURL(url), name(n), location(loc)
    {

    }

    QString getCircuitID() const;
    void setCircuitID(const QString &value);

    QString getInfoURL() const;
    void setInfoURL(const QString &value);

    QString getName() const;
    void setName(const QString &value);

    CircuitLocation getLocation() const;
    void setLocation(const CircuitLocation &value);

private:
    QString circuitID;
    QString infoURL;
    QString name;

    CircuitLocation location;
};

Q_DECLARE_METATYPE(CircuitData)

#endif // CIRCUITDATA_H
