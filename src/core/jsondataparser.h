/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef JSONDATAPARSER_H
#define JSONDATAPARSER_H

#include <QVector>

#include "driverdata.h"
#include "driverseasondata.h"
#include "eventdata.h"
#include "teamdata.h"
#include "teamseasondata.h"
#include "weatherforecastdata.h"

class JsonDataParser
{
public:
    JsonDataParser();

    void parseNextRaceData(const QByteArray &data, EventData &ed, CircuitData &cd);
    void parseDriverStandings(const QByteArray &data, QVector<DriverData> &drivers, QVector<TeamData> &teams, QVector<DriverSeasonData> &driversSeasonData);
    void parseConstructorStandings(const QByteArray &data, QVector<TeamData> &teams, QVector<TeamSeasonData> &teamSeasonData);
    void parseRaceSchedule(const QByteArray &data, QVector<EventData> &events, QVector<CircuitData> &circuits);
    void parseRaceResults(const QByteArray &data, EventData &ed, CircuitData &cd, QVector<DriverData> &drivers, QVector<DriverSeasonData> &driversSeasonData, QVector<TeamData> &teams);
    void parseQualiResults(const QByteArray &data, EventData &ed, CircuitData &cd, QVector<DriverData> &drivers, QVector<DriverSeasonData> &driversSeasonData, QVector<TeamData> &teams);
    void parseLapTimes(const QByteArray &data, QVector<LapData> &ld, int &season, int &round, QString &driverId);
    void parsePitStops(const QByteArray &data, QVector<PitStopData> &pd, int &season, int &round, QString &driverId);
    void parseDriverResults(const QByteArray &data, QVector<EventData> &events, QVector<CircuitData> &circuits, QVector<TeamData> &teams, QString &driverId);
    void parseConstructorResults(const QByteArray &data, QVector<EventData> &events, QVector<CircuitData> &circuits, QVector<DriverData> &driverData, QString &constructorId);

    void parseWeatherForecast(const QByteArray &data, WeatherForecastData &weatherData);


    void parseEventData(const QJsonObject &object, EventData &ed, CircuitData &cd);
    void parseCircuitData(const QJsonObject &object, CircuitData &cd);
    void parseDriverData(const QJsonObject &object, DriverData &dd, DriverSeasonData &ds, bool parsingStandings = false);
    void parseTeamData(const QJsonObject &object, TeamData &td, TeamSeasonData &ts, bool parsingStandings = false);
    void parseRaceData(const QJsonObject &object, DriverRaceData &drd);
    void parseQualiData(const QJsonObject &object, DriverQualiData &drd);
    void parseLapData(const QJsonObject &object, LapData &ld);
    void parsePitData(const QJsonObject &object, PitStopData &pd);
    void parseWeatherData(const QJsonObject &object, WeatherData &wd);
};

#endif // JSONDATAPARSER_H
