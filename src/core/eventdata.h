/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef EVENTDATA_H
#define EVENTDATA_H

#include <QDateTime>
#include <QDataStream>
#include <QMetaType>
#include <QString>
#include <QVector>
#include "circuitdata.h"
#include "lapdata.h"

struct DriverQualiData
{
    enum QualiPeriod
    {
        Q1 = 0, Q2, Q3
    };

    int season;
    int round;
    int number;

    QString driverID;
    QString constructorID;

    LapTime time[3];

    int position;

    bool operator < (const DriverQualiData &dd) const
    {
        return round < dd.round;
    }

};

struct PitStopData
{
    QString driverID;
    int lap;
    QTime raceTime;
    LapTime duration;

    bool operator == (const PitStopData &pd) const
    {
        return ((driverID == pd.driverID) &&
                (lap == pd.lap));
    }
};

struct DriverRaceData
{
    void loadData(QDataStream &stream);
    void saveData(QDataStream &stream) const;

    int season;
    int round;
    int number;

    QString driverID;
    QString constructorID;

    int position;
    QString positionText;
    double points;
    int grid;
    int laps;
    QString status;

    QString raceTime;
    int raceTimeMillis;

    LapTime fastestLap;
    int fastestLapNumber;

    QString avgSpeed;
    QString avgSpeedUnits;

    QVector<LapData> lapData;
    QVector<PitStopData> pitStopData;

    bool operator < (const DriverRaceData &dd) const
    {
        return round < dd.round;
    }

    DriverRaceData &operator=(const DriverRaceData &dr);
};


class EventData
{
public:
    EventData();

    void loadData(QDataStream &stream, int season);
    void saveData(QDataStream &stream) const;

    int getSeason() const;
    void setSeason(int value);

    int getRound() const;
    void setRound(int value);

    QString getInfoURL() const;
    void setInfoURL(const QString &value);

    QString getCircuitID() const;
    void setCircuitID(const QString &value);

    QDateTime getRaceDate() const;
    void setRaceDate(const QDateTime &value);

    QString getRaceName() const;
    void setRaceName(const QString &value);

    const QHash<QString, DriverRaceData> *getRaceData() const;
    void setRaceData(const QVector<DriverRaceData> &value);

    void addRaceData(const DriverRaceData &drd);
    const DriverRaceData *getRaceData(QString driverId) const;
    DriverRaceData *getRaceData(QString driverId);
    const QVector<const DriverRaceData *> getRaceDataFromConstructor(QString constructorId) const;

    const QHash<QString, DriverQualiData> *getQualiData() const;
    void setQualiData(const QVector<DriverQualiData> &value);

    void addQualiData(const DriverQualiData &dqd);
    const DriverQualiData *getQualiData(QString driverId) const;
    DriverQualiData *getQualiData(QString driverId);

    bool operator < (const EventData &ed) const
    {
        return round < ed.round;
    }

    EventData &operator=(const EventData &event);


    bool getRaceResultsObtained() const;
    void setRaceResultsObtained(bool value);

    bool getQualiResultsObtained() const;
    void setQualiResultsObtained(bool value);

    void getFastestLap(QString &driver, QString &time, int &lapNo) const;

private:
    int season;
    int round;

    QString raceName;
    QString infoURL;
    QDateTime raceDate;

    QString circuitID;


    QHash<QString, DriverRaceData> raceData;
    QHash<QString, DriverQualiData> qualiData;

    bool raceResultsObtained;
    bool qualiResultsObtained;
};

Q_DECLARE_METATYPE(EventData)
Q_DECLARE_METATYPE(const EventData*)

#endif // EVENTDATA_H
