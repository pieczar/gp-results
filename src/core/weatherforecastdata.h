/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef WEATHERFORECASTDATA_H
#define WEATHERFORECASTDATA_H

#include <QDate>
#include <QMetaType>
#include <QString>

struct WeatherData
{
    QDate date;
    double dayTemp;
    double nightTemp;
    double minTemp;
    double maxTemp;

    double pressure;
    double humidity;

    QString icon;
    QString weather;

    double windSpeed;
    double windDirection;
};

class WeatherForecastData
{
public:
    WeatherForecastData();

    QString getCity() const;
    void setCity(const QString &value);

    QString getCityId() const;
    void setCityId(const QString &value);

    QString getCountryCode() const;
    void setCountryCode(const QString &value);

    double getLat() const;
    void setLat(double value);

    double getLon() const;
    void setLon(double value);

    QList<WeatherData> getDailyForecast() const;
    void setDailyForecast(const QList<WeatherData> &value);
    void addDailyForecast(const WeatherData &data);

    QDate getForecastDate() const;
    void setForecastDate(const QDate &value);

    int getSeason() const;
    void setSeason(int value);

    int getRound() const;
    void setRound(int value);

private:

    QString city;
    QString cityId;
    QString countryCode;
    double lat;
    double lon;
    QList<WeatherData> dailyForecast;

    QDate forecastDate;
    int season;
    int round;
};

Q_DECLARE_METATYPE(WeatherForecastData)

#endif // WEATHERFORECASTDATA_H
