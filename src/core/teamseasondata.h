/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef TEAMSEASONDATA_H
#define TEAMSEASONDATA_H

#include "teamdata.h"

class TeamSeasonData
{
public:
    TeamSeasonData();

    int getSeason() const;
    void setSeason(int value);

    QString getConstructorId() const;
    void setConstructorId(const QString &value);

    double getPoints() const;
    void setPoints(double value);

    int getWins() const;
    void setWins(int value);

    TeamSeasonData &operator=(const TeamSeasonData &tsd);

    int getPosition() const;
    void setPosition(int value);

    bool getSeasonResultsObtained() const;
    void setSeasonResultsObtained(bool value);

    QString getPositionText() const;
    void setPositionText(const QString &value);

private:
    int season;
    int position;

    QString positionText;

    QString constructorId;

    int wins;
    double points;

    bool seasonResultsObtained;
};

#endif // TEAMSEASONDATA_H
