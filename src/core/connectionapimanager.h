/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#ifndef CONNECTIONAPIMANAGER_H
#define CONNECTIONAPIMANAGER_H

#include "driverdata.h"
#include "eventdata.h"
#include "jsondataparser.h"
#include "teamdata.h"
#include "teamseasondata.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QObject>

class ConnectionAPIManager : public QObject
{
    Q_OBJECT
public:
    ConnectionAPIManager(QObject *parent = 0);
    ~ConnectionAPIManager();

    void obtainNextRaceData();
    void obtainDriversStandings(int year);
    void obtainDriversCurrentStandings();
    void obtainConstructorStandings(int year);
    void obtainConstructorCurrentStandings();
    void obtainRaceSchedule(int year);
    void obtainLatestRaceResults();
    void obtainLatestQualiResults();
    void obtainRaceResults(int season, int round);
    void obtainQualiResults(int season, int round);
    void obtainLapTimes(QString driverId, int season, int round);
    void obtainPitStops(QString driverId, int season, int round);
    void obtainDriverResults(QString driverId, int season);
    void obtainConstructorResults(QString constructorId, int season);

    void obtainWeatherForecast(double lat, double lon);

signals:
    void nextRaceDataObtained(const EventData &, const CircuitData &);    
    void driversStandingsObtained(const QVector<DriverData> &, const QVector<DriverSeasonData> &, const QVector<TeamData> &);
    void constructorStandingsObtained(const QVector<TeamData> &, const QVector<TeamSeasonData> &);
    void raceScheduleObtained(const QVector<EventData> &, const QVector<CircuitData> &);
    void raceResultsObtained(const EventData &, const CircuitData &, const QVector<DriverData> &, const QVector<DriverSeasonData> &, const QVector<TeamData>&);
    void qualiResultsObtained(const EventData &, const CircuitData &, const QVector<DriverData> &, const QVector<DriverSeasonData> &, const QVector<TeamData>&);
    void lapTimesObtained(const QVector<LapData> &, int season, int round, QString driverId);
    void pitStopsObtained(const QVector<PitStopData> &, int season, int round, QString driverId);
    void driverResultsObtained(const QVector<EventData> &, const QVector<CircuitData> &, const QVector<TeamData> &, QString driverId);
    void constructorResultsObtained(const QVector<EventData> &, const QVector<CircuitData> &, const QVector<DriverData> &, QString constructorId);
    void weatherForecastObtained(const WeatherForecastData &wd);
    void connectionError(QNetworkReply::NetworkError);

public slots:
    void finishedNextRaceData();
    void finishedDriverStandings();
    void finishedConstructorStandings();
    void finishedRaceSchedule();
    void finishedRaceResults();
    void finishedQualiResults();
    void finishedLapTimes();
    void finishedPitStops();
    void finishedDriverResults();
    void finishedConstructorResults();
    void finishedWeatherForecast();

private:

    QString serverURL;
    QString nextRaceURL;
    QString lastRaceResultsURL;
    QString lastQualiResultsURL;
    QString raceResultsURL;
    QString qualiResultsURL;
    QString driverStandingsURL;
    QString driverCurrentStandingsURL;
    QString teamStandingsURL;
    QString teamCurrentStandingsURL;
    QString raceScheduleURL;
    QString lapTimesURL;
    QString pitStopsURL;
    QString driverResultsURL;
    QString constructorResultsURL;

    QString openWeatherApiURL;

    QNetworkAccessManager manager;
    QNetworkRequest req;

    JsonDataParser *dataParser;
};

#endif // CONNECTIONAPIMANAGER_H
