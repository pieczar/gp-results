/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "jsondataparser.h"

#include <QDate>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <QStringList>

JsonDataParser::JsonDataParser()
{
}

void JsonDataParser::parseNextRaceData(const QByteArray &data, EventData &ed, CircuitData &cd)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject raceTable = data.value("RaceTable").toObject();
        QJsonObject race = raceTable.value("Races").toArray().at(0).toObject();

        parseEventData(race, ed, cd);
    }
}

void JsonDataParser::parseDriverStandings(const QByteArray &data, QVector<DriverData> &drivers, QVector<TeamData> &teams, QVector<DriverSeasonData> &driversSeasonData)
{    
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject standingsTable = data.value("StandingsTable").toObject();
        QJsonObject standingsList = standingsTable.value("StandingsLists").toArray().at(0).toObject();
        QJsonArray driverStandings = standingsList.value("DriverStandings").toArray();

        int season = standingsTable.value("season").toString().toInt();

        bool ok = false;
        int size = data.value("total").toString().toInt(&ok);

        if (ok)
        {
            for (int i = 0; i < size; ++i)
            {
                DriverData dd;
                DriverSeasonData ds;                

                parseDriverData(driverStandings.at(i).toObject(), dd, ds, true);

                QJsonArray constructors = driverStandings.at(i).toObject().value("Constructors").toArray();

                for (int j = 0; j < constructors.size(); ++j)
                {
                    TeamData td;

                    td.setConstructorID(constructors.at(j).toObject().value("constructorId").toString());
                    td.setInfoURL(constructors.at(j).toObject().value("url").toString());
                    td.setTeamName(constructors.at(j).toObject().value("name").toString());
                    td.setNationality(constructors.at(j).toObject().value("nationality").toString());

                    bool found = false;
                    for (int k = 0; k < teams.size(); ++k)
                    {
                        if (teams[k].getConstructorID() == td.getConstructorID())
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        teams.append(td);

                }
                ds.setSeason(season);

                drivers.append(dd);
                driversSeasonData.append(ds);
            }
        }
    }
}

void JsonDataParser::parseConstructorStandings(const QByteArray &data, QVector<TeamData> &teams, QVector<TeamSeasonData> &teamSeasonData)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject standingsTable = data.value("StandingsTable").toObject();
        QJsonObject standingsList = standingsTable.value("StandingsLists").toArray().at(0).toObject();
        QJsonArray constructorStandings = standingsList.value("ConstructorStandings").toArray();

        int season = standingsTable.value("season").toString().toInt();

        bool ok = false;
        int size = data.value("total").toString().toInt(&ok);

        if (ok)
        {
            for (int i = 0; i < size; ++i)
            {
                TeamData td;
                TeamSeasonData ts;

                parseTeamData(constructorStandings.at(i).toObject(), td, ts, true);

                ts.setSeason(season);

                teams.append(td);
                teamSeasonData.append(ts);
            }
        }
    }
}

void JsonDataParser::parseRaceSchedule(const QByteArray &data, QVector<EventData> &events, QVector<CircuitData> &circuits)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject racesTable = data.value("RaceTable").toObject();
        QJsonArray races = racesTable.value("Races").toArray();

        bool ok = false;
        int size = data.value("total").toString().toInt(&ok);

        if (ok)
        {
            for (int i = 0; i < size; ++i)
            {
                EventData ed;
                CircuitData cd;
                parseEventData(races.at(i).toObject(), ed, cd);

                if (ed.getRound() == 0)
                {
                    ed.setRound(racesTable.value("round").toString().toInt());
                    ed.setSeason(racesTable.value("season").toString().toInt());
                }

                events.append(ed);
                circuits.append(cd);
            }
        }
    }
}

void JsonDataParser::parseRaceResults(const QByteArray &data, EventData &ed, CircuitData &cd, QVector<DriverData> &drivers, QVector<DriverSeasonData> &driversSeasonData, QVector<TeamData> &teams)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject racesTable = data.value("RaceTable").toObject();
        QJsonArray races = racesTable.value("Races").toArray();
        QJsonArray results = races.at(0).toObject().value("Results").toArray();

        bool ok = false;
        int size = data.value("total").toString().toInt(&ok);

        parseEventData(races.at(0).toObject(), ed, cd);

        if (ed.getRound() == 0)
        {
            ed.setRound(racesTable.value("round").toString().toInt());
            ed.setSeason(racesTable.value("season").toString().toInt());
        }

        if (ok)
        {
            for (int i = 0; i < size; ++i)
            {
                DriverData dd;
                DriverSeasonData dsd;
                DriverRaceData drd;
                TeamData td;
                TeamSeasonData ts;

                parseDriverData(results.at(i).toObject(), dd, dsd);
                parseTeamData(results.at(i).toObject(), td, ts);
                parseRaceData(results.at(i).toObject(), drd);

                drd.season = ed.getSeason();                                
                drd.round = ed.getRound();
                drd.driverID = dd.getDriverID();

                if (!dsd.getConstructorId().isEmpty())
                    drd.constructorID = dsd.getConstructorId().first();

                dsd.setSeason(ed.getSeason());
                dsd.setNumber(drd.number);

                ed.addRaceData(drd);

                drivers.append(dd);
                teams.append(td);
                driversSeasonData.append(dsd);
            }
            ed.setRaceResultsObtained(true);
        }
    }
}

void JsonDataParser::parseQualiResults(const QByteArray &data, EventData &ed, CircuitData &cd, QVector<DriverData> &drivers, QVector<DriverSeasonData> &driversSeasonData, QVector<TeamData> &teams)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject racesTable = data.value("RaceTable").toObject();
        QJsonArray races = racesTable.value("Races").toArray();
        QJsonArray results = races.at(0).toObject().value("QualifyingResults").toArray();

        bool ok = false;
        int size = data.value("total").toString().toInt(&ok);

        parseEventData(races.at(0).toObject(), ed, cd);
        if (ok)
        {
            for (int i = 0; i < size; ++i)
            {
                DriverData dd;
                DriverSeasonData dsd;
                DriverQualiData dqd;
                TeamData td;
                TeamSeasonData ts;

                parseDriverData(results.at(i).toObject(), dd, dsd);
                parseTeamData(results.at(i).toObject(), td, ts);
                parseQualiData(results.at(i).toObject(), dqd);

                dqd.season = ed.getSeason();
                dqd.round = ed.getRound();
                dqd.driverID = dd.getDriverID();

                if (!dsd.getConstructorId().isEmpty())
                    dqd.constructorID = dsd.getConstructorId().first();

                dsd.setSeason(ed.getSeason());
                dsd.setNumber(dqd.number);


                ed.addQualiData(dqd);

                drivers.append(dd);
                teams.append(td);
                driversSeasonData.append(dsd);
            }
            ed.setQualiResultsObtained(true);
        }
    }
}

void JsonDataParser::parseLapTimes(const QByteArray &data, QVector<LapData> &ld, int &season, int &round, QString &driverId)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject racesTable = data.value("RaceTable").toObject();
        QJsonArray races = racesTable.value("Races").toArray();
        QJsonArray laps = races.at(0).toObject().value("Laps").toArray();

        season = racesTable.value("season").toString().toInt();
        round = racesTable.value("round").toString().toInt();
        driverId = racesTable.value("driverId").toString();

        bool ok = false;
        int size = data.value("total").toString().toInt(&ok);

        if (ok)
        {
            for (int i = 0; i < size; ++i)
            {
                LapData lapData;
                parseLapData(laps.at(i).toObject(), lapData);

                ld.append(lapData);
            }
        }
    }
}

void JsonDataParser::parsePitStops(const QByteArray &data, QVector<PitStopData> &pd, int &season, int &round, QString &driverId)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject racesTable = data.value("RaceTable").toObject();
        QJsonArray races = racesTable.value("Races").toArray();
        QJsonArray pitStops = races.at(0).toObject().value("PitStops").toArray();

        season = racesTable.value("season").toString().toInt();
        round = racesTable.value("round").toString().toInt();
        driverId = racesTable.value("driverId").toString();

        bool ok = false;
        int size = data.value("total").toString().toInt(&ok);

        if (ok)
        {
            for (int i = 0; i < size; ++i)
            {
                PitStopData pitData;
                parsePitData(pitStops.at(i).toObject(), pitData);

                pd.append(pitData);
            }
        }
    }
}

void JsonDataParser::parseDriverResults(const QByteArray &data, QVector<EventData> &events, QVector<CircuitData> &circuits, QVector<TeamData> &teams, QString &driverId)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject racesTable = data.value("RaceTable").toObject();
        QJsonArray races = racesTable.value("Races").toArray();

        driverId = racesTable.value("driverId").toString();


        for (int i = 0; i < races.size(); ++i)
        {
            EventData ed;
            CircuitData cd;

            parseEventData(races.at(i).toObject(), ed, cd);

            for (int j = 0; j < races.at(i).toObject().value("Results").toArray().size(); ++j)
            {
                DriverRaceData dr;
                TeamSeasonData ts;
                TeamData td;

                parseTeamData(races.at(i).toObject().value("Results").toArray().at(j).toObject(), td, ts);
                parseRaceData(races.at(i).toObject().value("Results").toArray().at(j).toObject(), dr);

                bool found = false;
                for (int k = 0; k < teams.size(); ++k)
                {
                    if (teams[k].getConstructorID() == td.getConstructorID())
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    teams.append(td);

                dr.driverID = driverId;
                dr.round = ed.getRound();
                dr.season = ed.getSeason();

                ed.addRaceData(dr);
            }

            events.append(ed);
            circuits.append(cd);
        }
    }
}

void JsonDataParser::parseConstructorResults(const QByteArray &data, QVector<EventData> &events, QVector<CircuitData> &circuits, QVector<DriverData> &driverData, QString &constructorId)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject data = root.value("MRData").toObject();

        QJsonObject racesTable = data.value("RaceTable").toObject();
        QJsonArray races = racesTable.value("Races").toArray();

        constructorId = racesTable.value("constructorId").toString();


        for (int i = 0; i < races.size(); ++i)
        {
            EventData ed;
            CircuitData cd;

            parseEventData(races.at(i).toObject(), ed, cd);

            for (int j = 0; j < races.at(i).toObject().value("Results").toArray().size(); ++j)
            {
                DriverRaceData dr;
                DriverSeasonData ds;
                DriverData dd;

                parseDriverData(races.at(i).toObject().value("Results").toArray().at(j).toObject(), dd, ds);

                bool found = false;
                for (int k = 0; k < driverData.size(); ++k)
                {
                    if (driverData[k].getDriverID() == dd.getDriverID())
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                    driverData.append(dd);

                parseRaceData(races.at(i).toObject().value("Results").toArray().at(j).toObject(), dr);

                dr.driverID = dd.getDriverID();
                dr.round = ed.getRound();
                dr.season = ed.getSeason();

                ed.addRaceData(dr);
            }

            events.append(ed);
            circuits.append(cd);
        }
    }
}

void JsonDataParser::parseWeatherForecast(const QByteArray &data, WeatherForecastData &weatherData)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);

    if (doc.isObject())
    {
        QJsonObject root = doc.object();

        QJsonObject city = root.value("city").toObject();

        weatherData.setCity(city.value("name").toString());
        weatherData.setCityId(QString::number(city.value("id").toDouble(), 'f', 0));
        weatherData.setCountryCode(city.value("country").toString());
        weatherData.setLat(city.value("coord").toObject().value("lat").toDouble());
        weatherData.setLon(city.value("coord").toObject().value("lon").toDouble());

        int size = root.value("cnt").toDouble();

        QJsonArray list = root.value("list").toArray();

        for (int i = 0; i < size; ++i)
        {
            WeatherData wd;
            parseWeatherData(list.at(i).toObject(), wd);

            weatherData.addDailyForecast(wd);
        }

        weatherData.setForecastDate(QDate::currentDate());
    }
}


void JsonDataParser::parseEventData(const QJsonObject &object, EventData &ed, CircuitData &cd)
{
    ed.setSeason(object.value("season").toString().toInt());
    ed.setRound(object.value("round").toString().toInt());
    ed.setInfoURL(object.value("url").toString());
    ed.setRaceName(object.value("raceName").toString());

    QString date = object.value("date").toString();
    QString time = object.value("time").toString();

    ed.setRaceDate(QDateTime(QDate::fromString(date, "yyyy-MM-dd"), QTime::fromString(time, "HH:mm:ssZ"), Qt::UTC));

    QJsonObject circuit = object.value("Circuit").toObject();
    parseCircuitData(circuit, cd);

    ed.setCircuitID(cd.getCircuitID());
}

void JsonDataParser::parseCircuitData(const QJsonObject &object, CircuitData &cd)
{
    cd.setCircuitID(object.value("circuitId").toString());
    cd.setInfoURL(object.value("url").toString());
    cd.setName(object.value("circuitName").toString());

    QJsonObject location = object.value("Location").toObject();

    CircuitLocation loc;
    loc.country = location.value("country").toString();
    loc.locality = location.value("locality").toString();
    loc.loc_lat = location.value("lat").toString().toDouble();
    loc.loc_long = location.value("long").toString().toDouble();

    cd.setLocation(loc);
}

void JsonDataParser::parseDriverData(const QJsonObject &object, DriverData &dd, DriverSeasonData &ds, bool parsingStandings)
{
    if (parsingStandings)
    {
        ds.setPosition(object.value("position").toString("0").toInt());
        ds.setPositionText(object.value("positionText").toString());
    }

    ds.setPoints(object.value("points").toString("0").toDouble());
    ds.setWins(object.value("wins").toString("0").toInt());

    QJsonObject driver = object.value("Driver").toObject();

    dd.setDriverID(driver.value("driverId").toString());
    ds.setDriverId(dd.getDriverID());

    dd.setBiographyURL(driver.value("url").toString());
    dd.setDriverName(driver.value("givenName").toString() + " " + driver.value("familyName").toString());
    dd.setBirthDate(QDate::fromString(driver.value("dateOfBirth").toString(), "yyyy-MM-dd"));
    dd.setNationality(driver.value("nationality").toString());

    QJsonArray constructors = object.value("Constructors").toArray();

    for (int i = 0; i < constructors.size(); ++i)
        ds.addConstructorId(constructors.at(i).toObject().value("constructorId").toString());

    if (constructors.isEmpty())
    {
        ds.addConstructorId(object.value("Constructor").toObject().value("constructorId").toString());
    }
}

void JsonDataParser::parseTeamData(const QJsonObject &object, TeamData &td, TeamSeasonData &ts, bool parsingStandings)
{
    if (parsingStandings)
    {
        ts.setPosition(object.value("position").toString("0").toInt());
        ts.setPositionText(object.value("positionText").toString());
    }

    ts.setPoints(object.value("points").toString("0").toDouble());
    ts.setWins(object.value("wins").toString("0").toInt());

    QJsonObject constructor = object.value("Constructor").toObject();

    td.setConstructorID(constructor.value("constructorId").toString());
    ts.setConstructorId(td.getConstructorID());
    td.setInfoURL(constructor.value("url").toString());
    td.setTeamName(constructor.value("name").toString());
    td.setNationality(constructor.value("nationality").toString());
}

void JsonDataParser::parseRaceData(const QJsonObject &object, DriverRaceData &drd)
{
    drd.number = object.value("number").toString().toInt();
    drd.position = object.value("position").toString().toInt();    
    drd.positionText = object.value("positionText").toString();
    drd.points = object.value("points").toString().toDouble();
    drd.grid = object.value("grid").toString().toInt();
    drd.laps = object.value("laps").toString().toInt();
    drd.status = object.value("status").toString();
    drd.raceTime = object.value("Time").toObject().value("time").toString();
    drd.raceTimeMillis = object.value("Time").toObject().value("millis").toString().toInt();
    drd.fastestLap = LapTime(object.value("FastestLap").toObject().value("Time").toObject().value("time").toString());
    drd.fastestLapNumber = object.value("FastestLap").toObject().value("lap").toString().toInt();
    drd.avgSpeed = object.value("FastestLap").toObject().value("AverageSpeed").toObject().value("speed").toString();
    drd.avgSpeedUnits = object.value("FastestLap").toObject().value("AverageSpeed").toObject().value("units").toString();

    QJsonObject constructor = object.value("Constructor").toObject();
    drd.constructorID = constructor.value("constructorId").toString();
}

void JsonDataParser::parseQualiData(const QJsonObject &object, DriverQualiData &dqd)
{
    dqd.number = object.value("number").toString().toInt();
    dqd.position = object.value("position").toString().toInt();
    dqd.time[DriverQualiData::Q1] = LapTime(object.value("Q1").toString());
    dqd.time[DriverQualiData::Q2] = LapTime(object.value("Q2").toString());
    dqd.time[DriverQualiData::Q3] = LapTime(object.value("Q3").toString());
}

void JsonDataParser::parseLapData(const QJsonObject &object, LapData &ld)
{
    ld.setLapNumber(object.value("number").toString().toInt());
    ld.setDriverID(object.value("Timings").toArray().at(0).toObject().value("driverId").toString());
    ld.setLapTime(LapTime(object.value("Timings").toArray().at(0).toObject().value("time").toString()));
    ld.setPosition(object.value("Timings").toArray().at(0).toObject().value("position").toString().toInt());
}

void JsonDataParser::parsePitData(const QJsonObject &object, PitStopData &pd)
{
    pd.driverID = object.value("driverId").toString();
    pd.duration = object.value("duration").toString();
    pd.lap = object.value("lap").toString().toInt();
    pd.raceTime = QTime::fromString(object.value("time").toString(), "hh:mm:ss");
}

void JsonDataParser::parseWeatherData(const QJsonObject &object, WeatherData &wd)
{
    wd.date = QDateTime::fromTime_t(object.value("dt").toDouble()).date();
    wd.dayTemp = object.value("temp").toObject().value("day").toDouble();
    wd.nightTemp = object.value("temp").toObject().value("night").toDouble();
    wd.minTemp = object.value("temp").toObject().value("min").toDouble();
    wd.maxTemp = object.value("temp").toObject().value("max").toDouble();
    wd.pressure = object.value("pressure").toDouble();
    wd.humidity = object.value("humidity").toDouble();
    wd.weather = object.value("weather").toArray().at(0).toObject().value("description").toString();
    wd.icon = object.value("weather").toArray().at(0).toObject().value("icon").toString();
    wd.windSpeed = object.value("speed").toDouble();
    wd.windDirection = object.value("deg").toDouble();
}
