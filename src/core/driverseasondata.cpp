/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2012-2013  Mariusz Pilarek (pieczaro@gmail.com)             *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "driverseasondata.h"

DriverSeasonData::DriverSeasonData() : season(0), position(0), wins(0), points(0), number(0), seasonResultsObtained(false)
{
}
QString DriverSeasonData::getDriverId() const
{
    return driverId;
}

void DriverSeasonData::setDriverId(const QString &value)
{
    driverId = value;
}
QVector<QString> DriverSeasonData::getConstructorId() const
{
    return constructorId;
}

void DriverSeasonData::setConstructorId(const QVector<QString> &value)
{
    constructorId = value;
}

void DriverSeasonData::addConstructorId(const QString &value)
{
    if (!constructorId.contains(value))
        constructorId.append(value);
}
int DriverSeasonData::getWins() const
{
    return wins;
}

void DriverSeasonData::setWins(int value)
{
    wins = value;
}
double DriverSeasonData::getPoints() const
{
    return points;
}

void DriverSeasonData::setPoints(double value)
{
    points = value;
}


void DriverSeasonData::setSeason(int value)
{
    season = value;
}

DriverSeasonData &DriverSeasonData::operator=(const DriverSeasonData &dsd)
{
    if (this == &dsd)
        return *this;

    if (dsd.position != 0)
    {
        position = dsd.position;
        positionText = dsd.positionText;
    }

    season = dsd.season;
    driverId = dsd.driverId;
    constructorId = dsd.constructorId;

    if (dsd.number != 0)
        number = dsd.number;

    if (dsd.points > points)
        points = dsd.points;

    if (dsd.wins > wins)
        wins = dsd.wins;

    return *this;
}
int DriverSeasonData::getNumber() const
{
    return number;
}

void DriverSeasonData::setNumber(int value)
{
    number = value;
}
int DriverSeasonData::getPosition() const
{
    return position;
}

void DriverSeasonData::setPosition(int value)
{
    position = value;
}
bool DriverSeasonData::getSeasonResultsObtained() const
{
    return seasonResultsObtained;
}

void DriverSeasonData::setSeasonResultsObtained(bool value)
{
    seasonResultsObtained = value;
}
QString DriverSeasonData::getPositionText() const
{
    return positionText;
}

void DriverSeasonData::setPositionText(const QString &value)
{
    positionText = value;
}




int DriverSeasonData::getSeason() const
{
    return season;
}






