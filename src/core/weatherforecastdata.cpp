/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2014  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "weatherforecastdata.h"

WeatherForecastData::WeatherForecastData()
{
}
QString WeatherForecastData::getCity() const
{
    return city;
}

void WeatherForecastData::setCity(const QString &value)
{
    city = value;
}
QString WeatherForecastData::getCityId() const
{
    return cityId;
}

void WeatherForecastData::setCityId(const QString &value)
{
    cityId = value;
}
QString WeatherForecastData::getCountryCode() const
{
    return countryCode;
}

void WeatherForecastData::setCountryCode(const QString &value)
{
    countryCode = value;
}
double WeatherForecastData::getLat() const
{
    return lat;
}

void WeatherForecastData::setLat(double value)
{
    lat = value;
}
double WeatherForecastData::getLon() const
{
    return lon;
}

void WeatherForecastData::setLon(double value)
{
    lon = value;
}
QList<WeatherData> WeatherForecastData::getDailyForecast() const
{
    return dailyForecast;
}

void WeatherForecastData::setDailyForecast(const QList<WeatherData> &value)
{
    dailyForecast = value;
}

void WeatherForecastData::addDailyForecast(const WeatherData &data)
{
    dailyForecast.append(data);
}
QDate WeatherForecastData::getForecastDate() const
{
    return forecastDate;
}

void WeatherForecastData::setForecastDate(const QDate &value)
{
    forecastDate = value;
}
int WeatherForecastData::getSeason() const
{
    return season;
}

void WeatherForecastData::setSeason(int value)
{
    season = value;
}
int WeatherForecastData::getRound() const
{
    return round;
}

void WeatherForecastData::setRound(int value)
{
    round = value;
}









