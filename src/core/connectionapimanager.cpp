/*******************************************************************************
 *                                                                             *
 *   Copyright (C) 2013  Mariusz Pilarek (pieczaro@gmail.com)                  *
 *                                                                             *
 *   This program is free software: you can redistribute it and/or modify      *
 *   it under the terms of the GNU General Public License as published by      *
 *   the Free Software Foundation, either version 3 of the License, or         *
 *   (at your option) any later version.                                       *
 *                                                                             *
 *   This program is distributed in the hope that it will be useful,           *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 *   GNU General Public License for more details.                              *
 *                                                                             *
 *   You should have received a copy of the GNU General Public License         *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                             *
 *******************************************************************************/

#include "connectionapimanager.h"

ConnectionAPIManager::ConnectionAPIManager(QObject *parent) : QObject(parent)
{
    serverURL = "http://ergast.com/api";
    nextRaceURL = serverURL + "/f1/current/next.json";
    driverStandingsURL = serverURL + "/f1/%1/driverStandings.json?limit=200";
    driverCurrentStandingsURL = serverURL + "/f1/current/driverStandings.json?limit=200";
    teamStandingsURL = serverURL + "/f1/%1/constructorStandings.json?limit=200";
    teamCurrentStandingsURL = serverURL + "/f1/current/constructorStandings.json?limit=200";
    raceScheduleURL = serverURL + "/f1/%1.json";
    lastRaceResultsURL = serverURL + "/f1/current/last/results.json";
    lastQualiResultsURL = serverURL + "/f1/current/last/qualifying.json";
    raceResultsURL = serverURL + "/f1/%1/%2/results.json?limit=200";
    qualiResultsURL = serverURL + "/f1/%1/%2/qualifying.json?limit=200";
    lapTimesURL = serverURL + "/f1/%1/%2/drivers/%3/laps.json?limit=200";
    pitStopsURL = serverURL + "/f1/%1/%2/drivers/%3/pitstops.json";
    driverResultsURL = serverURL + "/f1/%1/drivers/%2/results.json?limit=200";
    constructorResultsURL = serverURL + "/f1/%1/constructors/%2/results.json?limit=200";

    openWeatherApiURL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=%1&lon=%2&mode=json&units=metric&cnt=7";

    dataParser = new JsonDataParser();
}

ConnectionAPIManager::~ConnectionAPIManager()
{
    delete dataParser;
}

void ConnectionAPIManager::obtainNextRaceData()
{
    req = QNetworkRequest(nextRaceURL);
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect(rep, SIGNAL(finished()), this, SLOT(finishedNextRaceData()));
}

void ConnectionAPIManager::obtainDriversStandings(int year)
{
    req = QNetworkRequest(driverStandingsURL.arg(year));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedDriverStandings()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainDriversCurrentStandings()
{
    req = QNetworkRequest(driverCurrentStandingsURL);
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedDriverStandings()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainConstructorStandings(int year)
{
    req = QNetworkRequest(teamStandingsURL.arg(year));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedConstructorStandings()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainConstructorCurrentStandings()
{
    req = QNetworkRequest(teamCurrentStandingsURL);
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedConstructorStandings()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainRaceSchedule(int year)
{
    req = QNetworkRequest(raceScheduleURL.arg(year));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedRaceSchedule()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainLatestRaceResults()
{
    req = QNetworkRequest(lastRaceResultsURL);
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedRaceResults()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainLatestQualiResults()
{
    req = QNetworkRequest(lastQualiResultsURL);
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedQualiResults()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainRaceResults(int season, int round)
{
    req = QNetworkRequest(raceResultsURL.arg(season).arg(round));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedRaceResults()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainQualiResults(int season, int round)
{
    req = QNetworkRequest(qualiResultsURL.arg(season).arg(round));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedQualiResults()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainLapTimes(QString driverId, int season, int round)
{
    req = QNetworkRequest(lapTimesURL.arg(season).arg(round).arg(driverId));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedLapTimes()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainPitStops(QString driverId, int season, int round)
{
    req = QNetworkRequest(pitStopsURL.arg(season).arg(round).arg(driverId));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedPitStops()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainDriverResults(QString driverId, int season)
{
    req = QNetworkRequest(driverResultsURL.arg(season).arg(driverId));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedDriverResults()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainConstructorResults(QString constructorId, int season)
{
    req = QNetworkRequest(constructorResultsURL.arg(season).arg(constructorId));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedConstructorResults()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::obtainWeatherForecast(double lat, double lon)
{
    req = QNetworkRequest(openWeatherApiURL.arg(lat).arg(lon));
    req.setRawHeader("User-Agent","GP-Results");

    QNetworkReply *rep = manager.get(req);
    connect (rep, SIGNAL(finished()), this, SLOT(finishedWeatherForecast()));
    connect (rep, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(connectionError(QNetworkReply::NetworkError)));
}

void ConnectionAPIManager::finishedNextRaceData()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    QByteArray array = reply->readAll();

    EventData ed;
    CircuitData cd;

    dataParser->parseNextRaceData(array, ed, cd);

    emit nextRaceDataObtained(ed, cd);
}

void ConnectionAPIManager::finishedDriverStandings()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    QByteArray array = reply->readAll();

    QVector<DriverData> drivers;
    QVector<TeamData> teams;
    QVector<DriverSeasonData> driverSeasonData;

    dataParser->parseDriverStandings(array, drivers, teams, driverSeasonData);

    emit driversStandingsObtained(drivers, driverSeasonData, teams);
}

void ConnectionAPIManager::finishedConstructorStandings()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    QByteArray array = reply->readAll();

    QVector<TeamData> teams;
    QVector<TeamSeasonData> teamSeasonData;

    dataParser->parseConstructorStandings(array, teams, teamSeasonData);

    emit constructorStandingsObtained(teams, teamSeasonData);
}

void ConnectionAPIManager::finishedRaceSchedule()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    QByteArray array = reply->readAll();

    QVector<EventData> events;
    QVector<CircuitData> circuits;

    dataParser->parseRaceSchedule(array, events, circuits);

    emit raceScheduleObtained(events, circuits);
}

void ConnectionAPIManager::finishedRaceResults()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    QByteArray array = reply->readAll();

    EventData events;
    CircuitData circuits;

    QVector<DriverData> drivers;
    QVector<DriverSeasonData> driverSeasonData;
    QVector<TeamData> teams;

    dataParser->parseRaceResults(array, events, circuits, drivers, driverSeasonData, teams);

    emit raceResultsObtained(events, circuits, drivers, driverSeasonData, teams);
}

void ConnectionAPIManager::finishedQualiResults()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());
    QByteArray array = reply->readAll();

    EventData events;
    CircuitData circuits;

    QVector<DriverData> drivers;
    QVector<DriverSeasonData> driverSeasonData;
    QVector<TeamData> teams;

    dataParser->parseQualiResults(array, events, circuits, drivers, driverSeasonData, teams);

    emit qualiResultsObtained(events, circuits, drivers, driverSeasonData, teams);
}

void ConnectionAPIManager::finishedLapTimes()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());

    QByteArray array = reply->readAll();

    int season, round;
    QString driverId;
    QVector<LapData> lapData;

    dataParser->parseLapTimes(array, lapData, season, round, driverId);

    emit lapTimesObtained(lapData, season, round, driverId);
}

void ConnectionAPIManager::finishedPitStops()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());

    QByteArray array = reply->readAll();

    int season, round;
    QString driverId;
    QVector<PitStopData> pitData;

    dataParser->parsePitStops(array, pitData, season, round, driverId);

    emit pitStopsObtained(pitData, season, round, driverId);
}

void ConnectionAPIManager::finishedDriverResults()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());

    QByteArray array = reply->readAll();

    QVector<EventData> events;
    QVector<CircuitData> circuits;
    QVector<TeamData> teams;
    QString driverId;

    dataParser->parseDriverResults(array, events, circuits, teams, driverId);

    emit driverResultsObtained(events, circuits, teams, driverId);
}

void ConnectionAPIManager::finishedConstructorResults()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());

    QByteArray array = reply->readAll();

    QVector<EventData> events;
    QVector<CircuitData> circuits;
    QVector<DriverData> drivers;
    QString constructorId;

    dataParser->parseConstructorResults(array, events, circuits, drivers, constructorId);

    emit constructorResultsObtained(events, circuits, drivers, constructorId);
}

void ConnectionAPIManager::finishedWeatherForecast()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(QObject::sender());

    QByteArray array = reply->readAll();

    WeatherForecastData wd;
    dataParser->parseWeatherForecast(array, wd);

    emit weatherForecastObtained(wd);
}
