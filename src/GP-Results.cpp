/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QtQuick>
#include <sailfishapp.h>


#include "src/items/nextraceitem.h"
#include "src/items/eventinfoitem.h"
#include "src/items/circuitinfoitem.h"
#include "src/items/driverraceinfoitem.h"
#include "src/items/driverseasoninfoitem.h"
#include "src/items/constructorseasoninfoitem.h"
#include "src/items/seasoninfoitem.h"

#include "src/core/datamanager.h"
#include "src/models/raceschedulemodel.h"
#include "src/models/raceresultsmodel.h"
#include "src/models/qualiresultsmodel.h"
#include "src/models/laptimesmodel.h"
#include "src/models/driverstandingsmodel.h"
#include "src/models/constructorstandingsmodel.h"
#include "src/models/driverresultsmodel.h"
#include "src/models/constructorresultsmodel.h"
#include "src/models/constructorraceresultsmodel.h"
#include "src/models/weatherforecastmodel.h"


int main(int argc, char *argv[])
{

    qmlRegisterType<NextRaceItem>("harbour.gpresults.GPComponents", 1, 0, "NextRaceItem");
    qmlRegisterType<EventInfoItem>("harbour.gpresults.GPComponents", 1, 0, "EventInfoItem");
    qmlRegisterType<CircuitInfoItem>("harbour.gpresults.GPComponents", 1, 0, "CircuitInfoItem");
    qmlRegisterType<DriverRaceInfoItem>("harbour.gpresults.GPComponents", 1, 0, "DriverRaceInfoItem");
    qmlRegisterType<DriverSeasonInfoItem>("harbour.gpresults.GPComponents", 1, 0, "DriverSeasonInfoItem");
    qmlRegisterType<ConstructorSeasonInfoItem>("harbour.gpresults.GPComponents", 1, 0, "ConstructorSeasonInfoItem");
    qmlRegisterType<ConstructorRaceResultsModel>("harbour.gpresults.GPComponents", 1, 0, "ConstructorRaceResultsModel");
    qmlRegisterType<SeasonInfoItem>("harbour.gpresults.GPComponents", 1, 0, "SeasonInfoItem");

    QGuiApplication *app = SailfishApp::application(argc, argv);
    QQuickView *view = SailfishApp::createView();

    RaceScheduleModel rsModel;
    RaceResultsModel rrModel;
    QualiResultsModel qrModel;
    LapTimesModel ltModel;
    DriverStandingsModel dsModel;
    ConstructorStandingsModel csModel;
    DriverResultsModel drModel;
    ConstructorResultsModel crModel;
    WeatherForecastModel wfModel;

    view->rootContext()->setContextProperty("dataManager", &DataManager::getInstance());
    view->rootContext()->setContextProperty("raceScheduleModel", &rsModel);
    view->rootContext()->setContextProperty("raceResultsModel", &rrModel);
    view->rootContext()->setContextProperty("qualiResultsModel", &qrModel);
    view->rootContext()->setContextProperty("lapTimesModel", &ltModel);
    view->rootContext()->setContextProperty("driverStandingsModel", &dsModel);
    view->rootContext()->setContextProperty("constructorStandingsModel", &csModel);
    view->rootContext()->setContextProperty("driverSeasonResultsModel", &drModel);
    view->rootContext()->setContextProperty("constructorSeasonResultsModel", &crModel);
    view->rootContext()->setContextProperty("weatherForecastModel", &wfModel);


    view->setSource(SailfishApp::pathTo("qml/harbour-gpresults.qml"));
    view->show();

    return app->exec();
}

